// JavaScript Document
$(document).ready(function () {
    //轮播
    var clientWidth;
    if (/Android|webOS|iPhone|iPod|BlackBerry/i.test(navigator.userAgent)) {
        clientWidth = document.documentElement.clientWidth;
    } else {
        $(".page").css({"width": "540px", "margin": "0 auto", "overflow": "visible"});
        $("body").css("font-size", "16px")
        $(".content").css("overflow", "visible")
        clientWidth = $(".page").width();
    }
    $("#focus ul").css("marginLeft", "-" + clientWidth + "px");

    function autoRoll() {
        $("#focus ul").animate({marginLeft: "-" + 2 * clientWidth + "px"}, 500, "linear", function () {
            $("#focus ul").css({marginLeft: "-" + clientWidth + "px"});
            $("#focus li:first").remove().clone().appendTo("#focus ul");
        })
    }

    var timer = setInterval(function () {
        autoRoll();
    }, 3000);

    function swipe(k) {
        timer && clearInterval(timer);
        if (k == "left") {
            $("#focus ul").animate({marginLeft: "-" + 2 * clientWidth + "px"}, 500, "linear", function () {
                $("#focus ul").css({marginLeft: "-" + clientWidth + "px"});
                $($("#focus li:first").remove().get(0).cloneNode(true)).appendTo("#focus ul");
                timer = setInterval(function () {
                    autoRoll();
                }, 3000)
            })
        }
        else {
            $("#focus ul").animate({marginLeft: 0}, 500, "linear", function () {
                $("#focus ul").css({marginLeft: "-" + clientWidth + "px"});
                $($("#focus li:last").remove().get(0).cloneNode(true)).prependTo("#focus ul");
                timer = setInterval(function () {
                    autoRoll();
                }, 3000)
            })
        }
    }

    $("#focus ul").swipeLeft(function () {
        swipe("left");
    })
    $("#focus ul").swipeRight(function () {
        swipe();
    })

    //登录
    $(".login_botton").on("click", function () {
        var postData = {};
        postData.username = $("input[name='username']").val();
        postData.password = $("input[name='password']").val();
        $.ajax({
            url: '/login',
            type: 'post',
            data: postData,
            dataType: 'json',
            success: function (data) {
                if (data.status == 1) {
                    window.location.href = data.url;
                } else {
                    $.alert(data.info)
                }
            }
        });
    })

    //注册
    $("#register").on("click", function () {
        var postData = {};
        postData.mobile = $("input[name='mobile']").val() || "";
        postData.email = $("input[name='email']").val() || "";
        postData.import_referer = $("input[name='import_referer']").val() || "";
        postData.password = $("input[name='password']").val() || "";
        postData.confirm_password = $("input[name='confirm_password']").val() || "";
        $.ajax({
            url: '/Index/register',
            type: 'post',
            data: postData,
            dataType: 'json',
            success: function (data) {
                if (data.status == 1) {
                    window.location.href = data.url;
                } else {
                    $.alert(data.info)
                }
            }
        });
    });

    //开户
    $("#submit").on("click", function () {
        var postData = {};
        postData.real_name = $("input[name='real_name']").val() || "";
        postData.id_no = $("input[name='id_no']").val() || "";
        postData.mobile = $("input[name='mobile']").val() || "";
        postData.email = $("input[name='email']").val() || "";
        postData.bank_type = $("select[name='bank_type']").val() || "";
        postData.bank_no = $("input[name='bank_no']").val() || "";
        postData.address = $("#address").val() + "," + $("input[name='address']").val() || "";
        var location = $("input[name='location']").val().split(",");
        postData.province = location[0];
        postData.city = location[1];
        postData.area = location[2];
        if ($("input[name='agreement_1']").is(":checked") && $("input[name='agreement_2']").is(":checked")) {
            $.ajax({
                url: '/User/open',
                type: 'post',
                data: postData,
                dataType: 'json',
                success: function (data) {
                    if (data.status == 1) {
                        window.location.href = data.url;
                    } else {
                        $.alert(data.info)
                    }
                }
            });
        } else {
            $.alert("请勾选协议")
        }
    });
    //退出登录
    $("#logOut").on("click", function () {
        $.confirm('确定要退出登录吗？', function () {
            window.location.href = "/login/logout.html"
        });
    })
    //弹出协议
    $(".alert").on("click", function (e) {
        if ($(e.target).hasClass("show1")) {
            $(".alert1").css({"z-index": 9999999, "transform": "scale(1)"});
        }
        else {
            $(".alert2").css({"z-index": 9999999, "transform": "scale(1)"});
        }
        $(".cover").addClass("show");
    })
    //关闭协议
    $(".close_aggrement, .cover").on("click", function () {
        $(".cover").removeClass("show");
        $(".agreement").css({"z-index": -1, "transform": "scale(.1)"});
    })
    //修改密码
    $('#modify').on("click", function () {
        var postData = {};
        if ($("input[name='old_password']").length) {
            postData.old_password = $("input[name='old_password']").val();
        }
        postData.password = $("input[name='password']").val();
        postData.confirm_password = $("input[name='confirm_password']").val();
        $.ajax({
            url: '/user/pwd',
            type: 'post',
            data: postData,
            dataType: 'json',
            success: function (data) {
                if (data.status == 1) {
                    window.location.href = data.url;
                } else {
                    $.alert(data.info)
                }
            }
        });
    })

    //调用相机和相册
    var ua = navigator.userAgent.toLowerCase();
    //判断是否是苹果手机，是则是true
    var isIos = (ua.indexOf('iphone') != -1) || (ua.indexOf('ipad') != -1);
    if (isIos) {
        $(".file_input").removeAttr("capture");
    }
    ;
    $(".file_input").on("change", function (e) {
        var files = e.target.files;
        var idList = "";
        if (files.length) {
            $.showPreloader("图片上传中")
            setTimeout(function () {
                for (var i = 0; i < files.length; i++) {
                    var file = files[i];
                    if (file.type.indexOf("images/") > -1) {
                        continue;
                    }
                    var formData = new FormData();
                    formData.append("kcs_uploader_file", file);
                    $.ajax({
                        url: '/index/upload', /*这是处理文件上传的servlet*/
                        type: 'POST',
                        data: formData,
                        async: false,
                        processData: false,
                        contentType: false,
                        success: function (returndata) {
                            returndata = JSON.parse(returndata);
                            if (returndata.status && returndata.status == 1) {
                                if (idList) {
                                    idList += ("," + returndata.data.id);
                                }
                                else {
                                    idList += returndata.data.id;
                                }
                            }
                        }
                    })
                }
                $.hidePreloader();
                $.toast("上传成功");
                $("input[name='business_licence']").val(idList);
            })
        }
    })

    //申请代理
    //开户
    $("#agent_submit").on("click", function () {
        var postData = {};
        postData.company = $("input[name='company']").val() || "";
        postData.location = $("input[name='location']").val() || "";
        postData.business_licence = $("input[name='business_licence']").val() || "";
        if ($("input[name='agreement_3']").is(":checked")) {
            $.ajax({
                url: '/agent',
                type: 'post',
                data: postData,
                dataType: 'json',
                success: function (data) {
                    if (data.status == 1) {
                        window.location.href = data.url;
                    } else {
                        $.alert(data.info)
                    }
                }
            });
        } else {
            $.alert("请勾选协议")
        }
    });

    //格式化时间
    function dateToString(date, format) {
        if (!date) return "";
        format = format ? format : "y-m-d";
        switch (format) {
            case "y-m":
                return date.getFullYear() + "-" + datePad(date.getMonth() + 1, 2);
            case "y-m-d":
                return date.getFullYear() + "-" + datePad(date.getMonth() + 1, 2) + "-" + datePad(date.getDate(), 2);
            case "h-m-s":
                return datePad(date.getHours(), 2) + ":" + datePad(date.getMinutes(), 2) + ":" + datePad(date.getSeconds(), 2);
            case "y-m-d-h-m-s":
                return date.getFullYear() + "-" + datePad(date.getMonth() + 1, 2) + "-" + datePad(date.getDate(), 2) + " " + datePad(date.getHours(), 2) + ":" + datePad(date.getMinutes(), 2) + ":" + datePad(date.getSeconds(), 2);
        }
    }

    function datePad(num, n) {
        if ((num + "").length >= n)
            return num; //一位数
        return arguments.callee("0" + num, n); //两位数
    }

    //初始化时间组件
    if ($("#timePicker").length) {
        $("#timePicker").calendar();
        $("#applyGoods").on("click", function () {
            var postData = {};
            postData.take_money = $("input[name=applyMoney]").val() || "";
            postData.trade_money = $("input[name=money]").val() || "";
            postData.trade_date = $("input[name=time]").val() || "";
            $.ajax({
                url: '/coupon',
                type: 'post',
                data: postData,
                dataType: 'json',
                success: function (data) {
                    if (data.status == 1) {
                        window.location.href = data.url;
                    } else {
                        $.alert(data.info)
                    }
                }
            });
        })
    }

    //我的推荐加速关系 分页
    $('[action-type="page_next"]').click(function () {
        var page_total = $(this).attr('page-total');
        var page_num = $(this).data('page_num') || 1;
        $(this).data('page_num', ++page_num);

        if (page_num >= page_total) {
            $(this).remove();
        }

        $.ajax({
            url: '',
            type:'post',
            data: {p: page_num},
            success: function (html) {
                $('#referer_list').append(html);
            }
        });
    });
});



