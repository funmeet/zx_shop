<?php
// +-------------------------------------------------------------------
// |
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Service\Spec;

use Think\Log;

class Spec extends \Service\Common\BaseModel
{
    protected $name = 'spec';
    protected $is_vip_t = 1;
    protected $is_vip_f = 0;


    //获取商品详情
    public function getDetail($goods_id, $uid = 0)
    {
        $result = $this->find($goods_id);
       
        if (!$result) {
            throw new \Exception('没有该商品');
        }
        return $this->format($result, $uid);
    }
    

    //获取每组分类商品列表
    public function getGroupList($page_size, $uid = 0, $is_vip = 0)
    {
        $page_size = $page_size ?: 5;
        $goods_type = M('poster')->where(['category' => 'goods_category'])->order('level asc,id asc')->select();
        $result = [];
        foreach ($goods_type as $row) {
            $row['pic'] AND $row['pic'] = get_cover($row['pic'], 'path');
            $list = $this->where(['type' => $row['id'], 'status' => 1,'is_display'=>1, 'is_vip' => $is_vip])
                ->page(1, $row['num'] ?: 2)
                ->order('id desc')
                ->select();
            if($list){
                foreach ($list as $k=>$v){
                    $list[$k]['poster'] = get_cover($v['poster'], 'path');
                }
            }

            $result[] = array_merge($row, [
                'list' => $this->format($list, $uid)
            ]);
        }
        return $result;
    }

    
    public function getIndexList($uid = 0, $is_vip = 0)
    {
    		$list = $this->where(['status' => 1,'is_display'=>1, 'is_vip' => $is_vip])->order('sort desc')->select();
    		foreach ($list as $k=>$v){
    			$list[$k]['poster'] = get_cover($v['poster'], 'path');
    		}
    	return $this->format($list, $uid);
    }

    //搜索
    public function search($keyword)
    {
        $list = $this->where([
            'title' => ['like', "%{$keyword}%"],
            'intro' => ['like', "%{$keyword}%"],
            '_logic' => 'OR'
        ])->select();
        return $this->format($list);
    }

    //格式化
    public function format($list, $uid = 0)
    {
        if (!$list) {
            return $list;
        }

        $is_one = count($list) == count($list, 1);
        $list = $is_one ? [$list] : $list;

        foreach ($list as &$row) {
            //复购价格
            $row['is_rebuy'] = 0;
            if ($uid) {
                $is_buy = M('user')->where(['uid' => $uid])->getField('vip_level') >= 1;
                $is_buy AND $row['contribution'] = $row['rebuy_contribution'];
                $is_buy AND $row['is_rebuy'] = 1;
            }

            $row['list_cover'] = get_cover($row['list_cover'], 'path');
            $picture_ids = array_filter(explode(',', $row['picture_ids']));
            $row['picture_urls'] = [];
            foreach ($picture_ids as $id) {
                $row['picture_urls'] [] = get_cover($id, 'path');
            }
        }
        return $is_one ? $list[0] : $list;
    }
}