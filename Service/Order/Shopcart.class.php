<?php
// +-------------------------------------------------------------------
// |
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Service\Order;

class Shopcart extends \Service\Common\BaseModel
{
    protected $name = 'shopcart';

    //加入
    public function addCart($data, $uid)
    {
        $goods_id = $data['goods_id'];
        $goods = M('goods')->find($goods_id);
        if (!$goods) {
            throw new \Exception("无效的商品");
        }
        if($goods['is_vip'] == 1){
            if($data['buy_num'] != 1){
                throw new \Exception("报单商品下单一次只能购买一件");
            }
        }

        $spec_value_arr = explode('-', $data['spec_value']);
        asort($spec_value_arr);
        $spec_value_s = join('-', $spec_value_arr);

        $cart = $this->where([
            'uid' => $uid,
            'goods_id' => $goods_id,
            'spec_list' => $spec_value_s,
        ])->find();
        if ($cart) {
           /* $spec_list = [
                [
                    'id' => 3,
                    'name' => '颜色',
                    'v' => '黑色',
                ],
                [
                    'id' => 4,
                    'name' => '尺码',
                    'v' => 'S',
                ],
            ];*/
            $this->save([
                'id' => $cart['id'],
                'buy_num' => ['exp', 'buy_num+1'],
                'spec_list' => $data['spec_value'],
            ]);
            return $cart['id'];
        }

        $cart_id = $this->add([
            'uid' => $uid,
            'goods_id' => $goods_id,
            'buy_num' => $data['buy_num'],
            'create_time' => date('Y-m-d H:i:S'),
            'spec_list' => $data['spec_value'],
        ]);
        return $cart_id;
    }

    //修改购买数量
    public function changeNum($data, $uid)
    {
        $id = $data['id'];
        $buy_num = $data['buy_num'];
        $spec_list = $data['spec_value'];
        $cart = $this->where(['uid' => $uid, 'id' => $id])->find();
        if ($cart) {

            $this->save([
                'id' => $cart['id'],
                'buy_num' => $buy_num,
                'spec_list' => $spec_list?$spec_list:$cart['spec_list'],
            ]);
        }
    }

    //移除
    public function remove($ids, $uid)
    {
        $ids = is_array($ids) ? $ids : [];
        foreach ($ids as $id) {
            if (is_numeric($id)) {
                $this->where([
                    'uid' => $uid,
                    'id' => $id
                ])->delete();
            }
        }
    }

    //下单成功清空购物车
    public function emptyCart($uid)
    {
        $this->where(['uid' => $uid])->delete();
    }

    //我的购物车


    public function lists($uid)
    {
        $list = $this->where(['uid' => $uid])->select();
        $result = [];
        foreach ($list as $row) {

            $goods = OE('goods')->getDetail($row['goods_id'], $uid);
            if($goods['is_vip'] == 1){
                $this->where(['id' => $row['id']])->delete();
            }else{
                $goods['buy_num'] = $row['buy_num'];
                $goods['goods_id'] = $row['goods_id'];
                $goods['spec_label'] = json_decode($row['spec_list'], true);
                $goods['id'] = $row['id'];
                $result[] = $goods;
            }

        }
        return $result;
    }
}