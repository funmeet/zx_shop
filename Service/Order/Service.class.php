<?php
// +-------------------------------------------------------------------
// |
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Service\Order;

use Service\Common\BaseService;

class Service extends BaseService
{
    public static function formatCallback($order)
    {
        $arr=M('order_goods')->where(['order_id' => $order['order.id']])->select();
        $str='';
        foreach($arr as $k =>$v){
            $spec_arr = json_decode($v['spec_list'], true);
            $sku_name = array_column($spec_arr, 'sku_name');
            $str.= $v['title']."&nbsp;&nbsp;&nbsp;数量:{$v['buy_num']} &nbsp;&nbsp;&nbsp; 规格:".join(',', $sku_name)." <br />";
        }
        return ['order.order_goods' => $str];
    }
    
    public static function formatExpressAddress($json_str)
    {
        $data = json_decode($json_str, JSON_UNESCAPED_UNICODE);
        $html="收货人：{$data['name']}<br />";
        $html.="联系方式：{$data['mobile']}<br />";
        $html.="地址：{$data['area_label']} {$data['address']}<br />";
        return $html;
    }

    public function emptyCart($uid)
    {
        return Shopcart::getInstance()->emptyCart($uid);
    }

    public function cartList($goods_ids, $uid)
    {
        return Shopcart::getInstance()->lists($goods_ids, $uid);
    }

    public function removeGoods($goods_ids, $uid)
    {
        return Shopcart::getInstance()->remove($goods_ids, $uid);
    }

    public function changeNum($data, $uid)
    {
        return Shopcart::getInstance()->changeNum($data, $uid);
    }

    public function addCart($data, $uid)
    {
        return Shopcart::getInstance()->addCart($data, $uid);
    }

    public function payCommissionCallback($settle)
    {
        return Order::getInstance()->payCommissionCallback($settle);
    }

    public function received($order_no, $uid)
    {
        return Order::getInstance()->received($order_no, $uid);
    }

    public function express($order_id, $express_company, $express_no)
    {
        return Order::getInstance()->express($order_id, $express_company, $express_no);
    }

    public function info($id)
    {
        return Order::getInstance()->info($id);
    }

    public function setSettle($id)
    {
        return Settle::getInstance()->setSettle($id);
    }

    public function addSettle($order)
    {
        return Settle::getInstance()->addSettle($order);
    }

    public function settleAll()
    {
        return Settle::getInstance()->settleAll();
    }

    public function submitInvite($data, $uid, $login_info)
    {
        return Order::getInstance()->submitInvite($data, $uid, $login_info);
    }

    public function submit($data, $uid)
    {
        return Order::getInstance()->submit($data, $uid);
    }

    public function remove($order_no, $uid)
    {
        return Order::getInstance()->remove($order_no, $uid);
    }

    public function reback($order_no, $uid)
    {
        return Order::getInstance()->reback($order_no, $uid);
    }

    public function pay($data, $uid)
    {
        return Order::getInstance()->pay($data, $uid);
    }

    public static function payCallback($payment)
    {
        return Order::getInstance()->payCallback($payment);
    }

    public function lists($order_status, $uid, $page_num, $page_size)
    {
        return Order::getInstance()->lists($order_status, $uid, $page_num, $page_size);
    }

    public function getDetailByOrderNO($order_no, $uid = 0)
    {
        return Order::getInstance()->getDetailByOrderNO($order_no, $uid);
    }

    public function inviteMeList($uid, $friend_status, $page_num, $page_size)
    {
        return Order::getInstance()->inviteMeList($uid, $friend_status, $page_num, $page_size);
    }

    public function meInviteList($uid, $friend_status, $page_num, $page_size)
    {
        return Order::getInstance()->meInviteList($uid, $friend_status, $page_num, $page_size);
    }

    public function getStatusNum($uid)
    {
        return Order::getInstance()->getStatusNum($uid);
    }

    public function inviteReject($order_no, $reason, $uid)
    {
        return Order::getInstance()->inviteReject($order_no, $reason, $uid);
    }

    public function rebackGoods($order_no, $uid)
    {
        return Order::getInstance()->rebackGoods($order_no, $uid);
    }

    public function orderExpress($order_no, $uid)
    {
        return Order::getInstance()->orderExpress($order_no, $uid);
    }

    public function sss()
    {
        return Order::getInstance()->sss();
    }


}