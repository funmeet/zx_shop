<?php
// +-------------------------------------------------------------------
// |
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Service\Data;

class Coupon extends User
{
    protected $name = 'transfer';

    public function getCouponList($start_time = '', $end_time = '', $company_id = '')
    {
        $date_arr = $this->getDates($start_time, $end_time);

        $list = [];
        foreach ($date_arr as $date) {
            list($import_goods_quota_new, $import_goods_quota_total, $goods_quota_new, $goods_quota_total) = $this->getGoodsQuota($date, $company_id, $start_time);
            list($count_new_release, $count_total_release) = $this->getRelease($date, $company_id, $start_time);
            list($count_new_transfer, $count_total_transfer) = $this->getTransfer($date, $company_id, $start_time);
            list($count_new_ticket, $count_total_ticket) = $this->getTicket($date, $company_id, $start_time);
            $list[] = [
                'date' => $date,
                'import_goods_quota_new' => $import_goods_quota_new,
                'import_goods_quota_total' => $import_goods_quota_total,
                'goods_quota_new' => $goods_quota_new,
                'goods_quota_total' => $goods_quota_total,
                'count_new_release' => $count_new_release,
                'count_total_release' => $count_total_release,
                'count_new_transfer' => number_format(max($count_new_transfer - $count_new_release, 0), 3, '.', ''),
                'count_total_transfer' => number_format(max($count_total_transfer - $count_total_release, 0), 3, '.', ''),
                'count_new_ticket' => number_format(max($count_new_release - $count_new_ticket, 0), 3, '.', ''),
                'count_total_ticket' => number_format(max($count_total_release - $count_total_ticket, 0), 3, '.', '')
            ];
        }

        return $list;
    }

    public function getTransfer($date, $company_id = '', $start_time = '')
    {
        $where = $company_id ? ['b.import_company' => $company_id] : [];
        $count_new_transfer = $this
            ->table('__TRANSFER__ a')
            ->join('__USER__ b on b.uid=a.uid')
            ->where(array_merge([
                "a.create_time" => [
                    ['egt', $date],
                    ['elt', "$date 23:59:59"],
                ]
            ], $where))
            ->sum('a.goods_quota');
        $count_total_transfer = $this
            ->table('__TRANSFER__ a')
            ->join('__USER__ b on b.uid=a.uid')
            ->where(array_merge([
                'a.create_time' => array_merge([
                    ['elt', "$date 23:59:59"],
                ], $start_time ? [
                    ['egt', $start_time]
                ] : [])
            ], $where))
            ->sum('a.goods_quota');

        return [
            number_format($count_new_transfer, 3, '.', ''),
            number_format($count_total_transfer, 3, '.', ''),
        ];
    }

    public function getRelease($date, $company_id = '', $start_time = '')
    {
        $where = $company_id ? ['b.import_company' => $company_id] : [];
        $count_new_release = $this
            ->table('__RELEASE__ a')
            ->join('__USER__ b on b.uid=a.uid')
            ->where(array_merge([
                "a.release_time" => [
                    ['egt', $date],
                    ['elt', "$date 23:59:59"],
                ]
            ], $where))
            ->sum('a.release_quota');
        $count_total_release = $this
            ->table('__RELEASE__ a')
            ->join('__USER__ b on b.uid=a.uid')
            ->where(array_merge([
                'a.release_time' => array_merge([
                    ['elt', "$date 23:59:59"],
                ], $start_time ? [
                    ['egt', $start_time]
                ] : [])
            ], $where))
            ->sum('a.release_quota');

        return [
            number_format($count_new_release, 3, '.', ''),
            number_format($count_total_release, 3, '.', ''),
        ];
    }

    public function getTicket($date, $company_id = '', $start_time = '')
    {
        $where = $company_id ? ['b.import_company' => $company_id] : [];
        $count_new_ticket = $this
            ->table('__TICKET__ a')
            ->join('__USER__ b on b.uid=a.uid')
            ->where(array_merge([
                "a.create_time" => [
                    ['egt', $date],
                    ['elt', "$date 23:59:59"],
                ]
            ], $where))
            ->sum('a.use_quota');
        $count_total_ticket = $this
            ->table('__TICKET__ a')
            ->join('__USER__ b on b.uid=a.uid')
            ->where(array_merge([
                'a.create_time' => array_merge([
                    ['elt', "$date 23:59:59"],
                ], $start_time ? [
                    ['egt', $start_time]
                ] : [])
            ], $where))
            ->sum('a.use_quota');

        return [
            number_format($count_new_ticket, 3, '.', ''),
            number_format($count_total_ticket, 3, '.', ''),
        ];
    }

    public function getGoodsQuota($date, $company_id = '', $start_time = '')
    {
        $where = $company_id ? ['b.import_company' => $company_id] : [];
        $count_new = $this
            ->field('sum(a.import_goods_quota) import_goods_quota_new,sum(a.goods_quota) goods_quota_new')
            ->table('__TRANSFER__ a')
            ->join('__USER__ b on b.uid=a.uid')
            ->where(array_merge([
                "a.create_time" => [
                    ['egt', $date],
                    ['elt', "$date 23:59:59"],
                ]
            ], $where))
            ->find();
        $count_total = $this
            ->field('sum(a.import_goods_quota) import_goods_quota_total,sum(a.goods_quota) goods_quota_total')
            ->table('__TRANSFER__ a')
            ->join('__USER__ b on b.uid=a.uid')
            ->where(array_merge([
                'a.create_time' => array_merge([
                    ['elt', "$date 23:59:59"],
                ], $start_time ? [
                    ['egt', $start_time]
                ] : [])
            ], $where))
            ->find();

        return [
            number_format($count_new['import_goods_quota_new'], 3, '.', ''),
            number_format($count_total['import_goods_quota_total'], 3, '.', ''),
            number_format($count_new['goods_quota_new'], 3, '.', ''),
            number_format($count_total['goods_quota_total'], 3, '.', ''),
        ];
    }
}