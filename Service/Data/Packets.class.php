<?php
// +-------------------------------------------------------------------
// |
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Service\Data;

use Kcdns\Admin\Controller\PublicController;
class Packets extends User
{
    protected $name = 'red_packets';
    
    public function getPacketsList($start_time = '', $end_time = '')
    {
    	$date_arr = $this->getDates($start_time, $end_time);
    	$list = [];
    	foreach ($date_arr as $date) {
    		list($total_count,$yl_count,$wl_count) = $this->getPacketsNum($date);
    		list($total_red_packets_quota,$yl_red_packets_quota,$wl_red_packets_quota) = $this->getPacketsQuota($date);
    		
    		$list[] = [
    		'date' => $date,
    		//'total_count'=>$total_count,
    		'yl_count'=>$yl_count,
    		//'wl_count' => $wl_count,
    		'total_red_packets_quota' => $total_red_packets_quota,
    		'yl_red_packets_quota' => $yl_red_packets_quota,
    		'wl_red_packets_quota' => $wl_red_packets_quota,
    		];
    	}
    	
    	return $list;
    }
    
    public function getPacketsQuota($date){
    	$packets_total_rate = C('packets_total_rate',null,0.02);//红包总比率
    	$time = date('Y-m-d',strtotime($date."-1 day"));
    	$day_order_quota = O('packets')->getOrderQuota($time , $time);//昨日订单总额
    	$total_red_packets_quota = number_format(
    			$day_order_quota * $packets_total_rate,
    			2,
    			'.',
    			''
    	);//今日产生红包总额
    	$yl_red_packets_quota = O('packets')->getTotalPacketsQuota('',$date , $date,0);//今日发出红包总额
    	$wl_red_packets_quota = $total_red_packets_quota - $yl_red_packets_quota;
    	return [
    	$total_red_packets_quota,
    	$yl_red_packets_quota,
    	number_format($wl_red_packets_quota,2,'.',''),
    	
    	];
    }
    
    Public function getPacketsNum($date){
    	$where = [];
    	$where['b.vip_level'] = ['in',C('packets_level',null,'1,2,3')];
    	$total_count = M('ucenter_member')->alias('a')
    	                  ->join('__USER__ b on b.uid = a.id')
    	                  ->where($where)->count();

    	$yl_count = $this->where([
    			                    "type" => 0,
					    			"create_time" => [
    			                                 ['egt', $date],
									    	     ['elt', "$date 23:59:59"],
										    	]
					    					])->count();
    $wl_count = $total_count-$yl_count;
    	//echo $this->getLastSql();exit;
    	
    	return [$total_count,$yl_count,$wl_count];
    }
    
    public function explodePackets($list = [])
    {
    	$head = [
    	"日期",
    	//"当天应领取总人数",
    	"当天已领取人数",
    	//"当天未领取人数",
    	"当天产生红包总金额",
    	"当天已领取红包金额",
    	"当天未领取红包金额",
    	];

    	$data = [];
    	foreach ($list as $row) {
    		$data[] = [
    		$row['date'],
    		//$row['total_count'],
    		$row['yl_count'],
    		//$row['wl_count'],
    		$row['total_red_packets_quota'],
    		$row['yl_red_packets_quota'],
    		$row['wl_red_packets_quota'],
    		];
    	}
    	
    	array_unshift($data, $head);
    	$file = O('util')->writeXls($data);
    	$filename = pathinfo($file, PATHINFO_BASENAME);
    	
    	header("Last-Modified: " . gmdate("l d F Y H:i:s") . " GMT");
    	header("Content-Type: application/force-download");
    	header("Content-Type: application/octet-stream");
    	header("Content-Type: application/download");
    	header("Content-Disposition: attachment;filename=" . $filename);
    	header("Content-Transfer-Encoding: binary");
    	
    	readfile($file);
    	unlink($file);
    }
    
  
}