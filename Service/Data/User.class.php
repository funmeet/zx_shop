<?php
// +-------------------------------------------------------------------
// |
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Service\Data;

class User extends \Service\Common\BaseModel
{
    protected $name = 'user';

    public function getClientList($start_time = '', $end_time = '', $company_id = '')
    {
        $date_arr = $this->getDates($start_time, $end_time);

        $list = [];
        foreach ($date_arr as $date) {
            list($count_new_register, $count_total_register,) = $this->getRegister($date, $company_id, $start_time);
            list($count_new_open, $count_total_open,) = $this->getOpen($date, $company_id, $start_time);
            list($count_new_apply, $count_total_apply,) = $this->getApply($date, $company_id, $start_time);
            list($count_new_trade, $count_total_trade,) = $this->getTrade($date, $company_id, $start_time);
            $list[] = [
                'date' => $date,
                'count_new_register' => $count_new_register,
                'count_total_register' => $count_total_register,
                'count_new_open' => $count_new_open,
                'count_total_open' => $count_total_open,
                'count_new_apply' => $count_new_apply,
                'count_total_apply' => $count_total_apply,
                'count_new_trade' => $count_new_trade,
                'count_total_trade' => $count_total_trade,
            ];
        }

        return $list;
    }

    public function getTrade($date, $company_id = '', $start_time = '')
    {
        $where = $company_id ? ['b.import_company' => $company_id] : [];
        $count_new_register = $this
            ->table('__TRANSFER__ a')
            ->join('__USER__ b on b.uid=a.uid')
            ->where(array_merge([
                "a.create_time" => [
                    ['egt', $date],
                    ['elt', "$date 23:59:59"],
                ]
            ], $where))
            ->count();
        $count_total_register = $this
            ->table('__TRANSFER__ a')
            ->join('__USER__ b on b.uid=a.uid')
            ->where(array_merge([
                'a.create_time' => array_merge([
                    ['elt', "$date 23:59:59"],
                ], $start_time ? [
                    ['egt', $start_time]
                ] : [])
            ], $where))
            ->count();

        return [
            $count_new_register,
            $count_total_register,
        ];
    }

    public function getApply($date, $company_id = '', $start_time = '')
    {
        return $this->getUser($date, 'apply_time', $company_id, $start_time);
    }

    public function getOpen($date, $company_id = '', $start_time = '')
    {
        return $this->getUser($date, 'open_time', $company_id, $start_time);
    }

    public function getUser($date, $field, $company_id = '', $start_time = '')
    {
        $where = $company_id ? ['import_company' => $company_id] : [];
        $count_new_register = $this
            ->where(array_merge([
                "$field" => [
                    ['egt', $date],
                    ['elt', "$date 23:59:59"],
                ]
            ], $where))
            ->count();
        $count_total_register = $this
            ->where(array_merge([
                "$field" => array_merge([
                    ['elt', "$date 23:59:59"],
                ], $start_time ? [
                    ['egt', $start_time]
                ] : [])
            ], $where))
            ->count();

        return [
            $count_new_register,
            $count_total_register,
        ];
    }

    public function getRegister($date, $company_id = '', $start_time = '')
    {
        $where = $company_id ? ['import_company' => $company_id] : [];
        $count_new_register = $this
            ->where(array_merge([
                'create_time' => [
                    ['egt', $date],
                    ['elt', "$date 23:59:59"],
                ]
            ], $where))
            ->count();
        $count_total_register = $this
            ->where(array_merge([
                'create_time' => array_merge([
                    ['elt', "$date 23:59:59"],
                ], $start_time ? [
                    ['egt', $start_time]
                ] : [])
            ], $where))
            ->count();

        return [
            $count_new_register,
            $count_total_register,
        ];
    }

    public function getDates($start_time, $end_time)
    {
        if (!$start_time) {
            $today = date('Y-m-d');
            $start_time = date('Y-m-d', strtotime("$today -6 day"));
        }

        if (!$end_time) {
            $end_time = date('Y-m-d');
        }

        $date_arr = array_map(function ($time) {
            return date('Y-m-d', $time);
        }, range(strtotime($start_time), strtotime($end_time), 24 * 3600));

        rsort($date_arr);
        return array_slice($date_arr, 0, 30);
    }
}