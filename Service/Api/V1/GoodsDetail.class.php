<?php
// +-------------------------------------------------------------------
// |
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Service\Api\V1;

use Think\Model;

class GoodsDetail
{
    public $login = true; //是否需要登录才能访问该接口
    public $title = "商品详情OK";
    public $group = '商品';
    public $desc = "";

    public $input = [
        'goods_id' => 'required;int;label=商品id;',
    ];

    public $output = [
        'id' => 'label=商品id;',
        'title' => 'label=标题;',
        'isValid' => 'label=是否完善信息：true是false否;',
        'isRecommend' => 'label=是否有推荐人：true是false否;',
        'intro' => 'label=描述;',
        'system_store' => [
            'supply_no' => 'label=供货商编号;',
            'name' => 'label=供货商名字;',
            'image' => 'label=供货商logo;',
            'contact' => 'label=供货商联系人;',
            'phone' => 'label=供货商联系电话;',
            '_detailed_address' => 'label=供货商地址;',
        ],
        'sell_price' => 'label=零售价;',
        'vip_price' => 'label=会员价;',
        'give_point' => 'label=赠送积分;',
        'sell_num' => 'label=销量;',
        'stock' => 'label=库存;',
        'postage' => 'label=邮费;',
        'detail' => 'label=详情;',
        'create_time' => 'label=创建时间;',
        'update_time' => 'label=更新时间;',
        'list_cover' => 'label=列表封面;comment=单图',
        'picture_urls' => 'label=详情封面;comment=多图',
        'is_vip' => 'label=是否报单商品：1是 2不是',
        'is_agent' => 'label=是否代理商商品：1是 2不是',
        'is_rush' => 'label=是否抢购商品：1是2否',
        'is_hot' => 'label=是否未热品： 1是2否',
        'is_refer' => 'label=是否主推品： 1是2否',
        'unit' => 'label=单位',
        'view_num' => 'label=评价数量',
        'good_view_rate' => 'label=评价数量',
        'goods_info' => 'label=商品信息',
        'min_num' => 'label=起订量',
        'agentTotalNum' => 'label=代理商订购数量限制',
        'spec' => [
            [
                'pid' => 'label=父规格id',
                'parent_name' => 'label=规格名称',
                'list' => [
                    [
                        'sku_id' => 'label=id',
                        'sku_name' => 'label=规格值',
                    ]
                ]
            ]
        ],
        'stock_list' => [
            [
                'id' => 'label=id',
                'goods_id' => 'label=商品id',
                'sku_ids' => 'label=id',
                'stock' => 'label=库存量',
                'price' => 'label=价格',
                'vip_price' => 'label=vip价格',
            ]
        ],
        'attr' => 'label=处理的规格格式',
        'productValue' => 'label=处理的库存格式',
    ];

    public function run($param, $uid, $login_info)
    {

        $result = OE('goods')->getDetail($param['goods_id'], $uid);
        $result['detail'] = str_replace('/Uploads/Editor', 'http://zxshop.jiuyuanmall.cn/Uploads/Editor', $result['detail']);
        $result['goods_info'] = $result;
        $result['system_store'] = (array)M('supply')->where(['id' => $result['supply_no']])->find();
        if($result['system_store']){
            $result['system_store']['name'] = $result['system_store']['supply_name'];
            $result['system_store']['_detailed_address'] = $result['system_store']['address'];
            $result['system_store']['image'] = $result['system_store']['logo'];
            $result['system_store']['phone'] = $result['system_store']['mobile'];
            $result['system_store']['latitude'] = 1;
            $result['system_store']['longitude'] = 1;
        }

        $goods_sku = M('goods_sku')->where(['goods_id' => $param['goods_id']])->select();
        $parent_id_arr = [];
        $spec_list = [];
        $sku_content = [];


        foreach ($goods_sku as &$item) {
            $sku_content_arr = json_decode($item['sku_content'], true);
            foreach($sku_content_arr as $k => $v){
                $parent = M('sku')->where(['id' => $v['pid']])->find();
                if (isset($parent['id'])) {
                    if(!in_array($v['pid'], $parent_id_arr)) $parent_id_arr[] = $parent['id'];
                    $sku = M('sku')->where(['id' => $v['sku_id']])->find();
                    if (isset($sku['id'])) {
                        $sku_id_tem = array_column($sku_content[$sku['pid']], 'sku_id');
                        if(!in_array($sku['id'], $sku_id_tem)){
                            $sku_content[$sku['pid']][] = [
                                'sku_id' => $sku['id'],
                                'sku_name' => $sku['name'],
                            ];
                        }

                    }
                }
            }

        }


       foreach($sku_content as $k1 => $v1){
           $parent = M('sku')->where(['id' => $k1])->find();
           $tmp = [
               'pid' => $parent['id'],
               'parent_name' => $parent['name'],
           ];
           foreach($sku_content[$k1] as $k2 => $v2){
               $tmp['list'][] = $v2;
           }
           $spec_list[] = $tmp;
        }


        $result['spec'] = $spec_list;

        //评价
        $review = M('goods_review')->where(['goods_id' => $param['goods_id']])->count();
        $good_review = M('goods_review')->where(['goods_id' => $param['goods_id'], 'satisfaction' => 5])
            ->count();
        $result['review_total'] = $review?$review:0;
        if(!$review){
            $result['good_review_rate'];
        }else{
            $result['good_review_rate'] = number_format($good_review/$review, 2);
        }



        //库存
        $stock_list =  (array)M('goods_sku')->where(['goods_id' => $param['goods_id']])->select();
        foreach($stock_list as &$item){
            if($login_info['vip_level'] == 0){
                $item['price'] = $item['sell_price'];
            }elseif($login_info['vip_level'] == 4){
                $item['price'] = $item['agent_price'];
            }else{
                $item['price'] = $item['vip_price'];
            }
        }
        $result['stock_list'] = $stock_list;

        $productValue = [];
        foreach($stock_list as $k => $v){
            $sku_ids_tmp = explode('-', $v['sku_ids']);
            $sku_ids_tmp_name = [];
            $unique = [];
            foreach($sku_ids_tmp as $k1 => $v1){
                $s = M('sku')->where(['id' => $v1])->find();
                if($s){
                    $sku_ids_tmp_name[] = $s['name'];
                    $unique[] = [
                        'price' => 0,
                        'sku_id' => $v1,
                        'sku_name' => $s['name'],
                    ];
                }

            }
            if(count($sku_ids_tmp_name)>0){

                if($login_info['vip_level'] == 0){
                    $price = $v['sell_price'];
                }elseif($login_info['vip_level'] == 4){
                    $price = $v['agent_price'];
                }else{
                    $price = $v['vip_price'];
                }

                $productValue[join(',', $sku_ids_tmp_name)] = [
                    'product_id' => $v['id'],
                    'stock' => $v['stock'],
                    'price' => $price,
                    'vip_price' => $v['vip_price'],
                    'unique' => json_encode($unique),

                ];
            }
        }
        $result['productValue'] = $productValue;

        //规格格式调整
        $productAttr = [];
        foreach($spec_list as $k => $v){
            $tmp=[];
            $tmp['product_id'] = $v['pid'];
            $tmp['attr_name'] = $v['parent_name'];
            $tmp['attr_values'] = array_column($v['list'], 'sku_name');
            $tmp['attr_value'] = $v['list'];
            $productAttr[] = $tmp;
        }
        $result['attr']['productAttr'] = $productAttr;


        //是否有推荐人
        if(!$login_info['id_no']){
            $result['isValid'] = false;
        }else{
            $result['isValid'] = true;
        }

        if (!$login_info['referer_id']) {
            $result['isRecommend'] = false;
        } else {
            $result['isRecommend'] = true;
        }

        //g购买限制
        $agentTotalNum = 0;
        $vip_list = get_list_config('VIP_LEVEL', true);
        if($login_info['vip_level'] == 1 || $login_info['vip_level'] == 2){
            $agentTotalNum=$vip_list[4]['buyagentorder1'];
        }

        if($login_info['vip_level'] == 3){
            $agentTotalNum=$vip_list[4]['buyagentorder2'];
        }
        $result['goods_info']['agentTotalNum'] = $agentTotalNum;

        return $result;
    }
}
