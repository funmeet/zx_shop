<?php
// +-------------------------------------------------------------------
// |
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Service\Api\V1;

class OrderExpress
{
    public $login = true; //是否需要登录才能访问该接口
    public $title = "订单物流信息查询";
    public $group = '订单';
    public $desc = "";

    public $input = [
        'order_no' => 'required;bigint;label=订单号',
    ];

    public $output = [
        'express_company' => 'label=快递公司;',
        'express_no' => 'label=快递单号;',
        'express_time' => 'label=发货时间;',
    ];

    public function run($param, $uid)
    {
        $order = OE('order')->orderExpress($param['order_no'], $uid);
        return $order;
    }
}
