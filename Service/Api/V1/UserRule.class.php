<?php
// +-------------------------------------------------------------------
// |
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Service\Api\V1;

class UserRule
{
    public $login = false; //是否需要登录才能访问该接口
    public $title = "会员规则接口";
    public $group = '用户';
    public $desc = "";

    public $input = [
    ];

    public $output = [
        'list' => [
            [
                'pic_url' => 'label=会员规则图片;',
                'title' => 'label=名称;',
                'content' => 'label=规则内容;',
            ]
        ]

    ];

    public function run($param, $uid)
    {
        $result = M('poster')->where(['category' =>'user_rule'])->select();
        foreach($result as &$item){
            $item['pic_url'] = get_cover($item['pic'], 'url');

        }

        return ['list' =>$result];
    }
}
