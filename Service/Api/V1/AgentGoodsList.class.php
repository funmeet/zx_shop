<?php
// +-------------------------------------------------------------------
// |
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Service\Api\V1;

class AgentGoodsList
{
    public $login = false; //是否需要登录才能访问该接口
    public $title = "001代理商商品列表";
    public $group = '商品';
    public $desc = "";

    public $input = [
        'page_num' => 'int;label=分页;comment=默认为1',
        'page_size' => 'int;label=分页条数;comment=默认为5',
    ];

    public $output = [
        'list' => [
            [
                'id' => 'label=id;',
                'title' => 'label=商品名称;',
                'list_cover' => 'label=列表封面图;',
                'sell_price' => 'label=零售价;',
                'vip_price' => 'label=会员价;',
                'give_point' => 'label=赠送的积分;',
                'sell_num' => 'label=销量;',
                'min_num' => 'label=起订量;',
            ]
        ],
        'banner' => [
            [
                'pic' => 'label=图片;',
                'wap_url' => 'label=链接地址;',
            ]
        ]
    ];

    public function run($param, $uid)
    {
        $page_num = $param['page_num'] ?: 1;
        $page_size = $param['page_size'] ?: PAGE_SIZE;
        list($list, $page_count, $page_total) = OE('goods')->AgentGoodsList($page_num, $page_size, $param);
        $banner = M('poster')->where(['category' => 'bd_banner'])->order('level desc')->select();
        foreach($banner as &$item){
            $item['pic'] = get_cover($item['pic'], 'url');
        }
        return [
            'page_num' => $page_num,
            'page_size' => $page_size,
            'page_count' => $page_count,
            'page_total' => $page_total,
            'list' => $list,
            'banner' => $banner,
        ];
    }
}
