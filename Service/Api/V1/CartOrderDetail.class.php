<?php
// +-------------------------------------------------------------------
// |
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Service\Api\V1;

class CartOrderDetail
{
    public $login = true; //是否需要登录才能访问该接口
    public $title = "通过购物车获取订单详情";
    public $group = '订单';
    public $desc = "";

    public $input = [
        'cart_ids' => 'required;string;label=购物车id;comment=多个id使用英文逗号隔开;',
    ];

    public $output = [
        'address' => [
            'id' => 'label=id',
            'name' => 'label=收件人姓名',
            'mobile' => 'label=收件人手机号',
            'area_label' => 'label=地区名称',
            'address' => 'label=详细地址',
            'is_default' => 'label=默认地址;comment=是否默认： 0否 1是',
        ],
        'orderGroupInfo' => "label=购物车数据"
    ];

    public function run($param, $uid, $login_info)
    {
        $result = [];
        $address_info = (array)M('address')->where(['uid' => $uid, 'is_default' => 1])->find();
        $address_info['area_label'] = $address_info['area'];
        $result['address'] = $address_info;
        $cart_id_arr = explode(',', $param['cart_ids']);
        $cart_info = [];
        $cart_list = M('shopcart')->where("id in({$param['cart_ids']}) AND uid={$uid}")->select();
        foreach ($cart_list as $k => $v) {
            $v['spec_label'] = json_decode($v['spec_list'], true);
            $sku_name = array_column($v['spec_label'], 'sku_name');
            $sku_id_arr = array_column($v['spec_label'], 'sku_id');
            //获取库存
            $where['goods_id'] = $v['goods_id'];
            asort($sku_id_arr);
            if(count($sku_id_arr) == 1){
                $sku_id_arr[]= 0;
            }
            $where['sku_ids'] = join('-', $sku_id_arr);
            $data = M('goods_sku')->where($where)->find();
            $stock = $data['stock'] ? $data['stock'] : 0;

            if($login_info['vip_level'] == 0){
                $price = $data['sell_price'];
            }elseif($login_info['vip_level'] == 4){
                $price = $data['agent_price'];
            }else{
                $price = $data['vip_price'];
            }
            $vip_price = $data['vip_price'];

            $goods_info = M('goods')->where(['id' => $v['goods_id']])->find();
            $trueprice = $price;
            $cart_info[] = [
                'id' => $v['id'],
                'type' => 'product',
                'product_id' => $v['goods_id'],
                'cart_num' => $v['buy_num'],
                'trueprice' => $trueprice,
                'vip_trueprice' => $trueprice,
                'productInfo' => [
                    'id' => $v['goods_id'],
                    'image' => get_cover($goods_info['list_cover'], 'url'),

                    'stock' => $v['stock'],
                    'store_name' => $goods_info['title'],
                    'attrInfo' => [
                        'product_id' => $v['goods_id'],
                        'suk' => join(',', $sku_name),
                        'stock' => $stock,
                        'sales' => 0,
                        'image' => '',
                        'unique' => join('-', $sku_id_arr),
                    ],
                ],
            ];
        }
        $result['orderGroupInfo']['cart_info'] = $cart_info;
        return $result;
    }
}
