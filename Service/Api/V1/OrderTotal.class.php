<?php
// +-------------------------------------------------------------------
// |
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Service\Api\V1;

class OrderTotal
{
    public $login = true; //是否需要登录才能访问该接口
    public $title = "订单中心统计接口";
    public $group = '订单';
    public $desc = "";

    public $input = [
    ];

    public $output = [
        'order_count' => 'label=订单总数;',
        'sum_price' => 'label=总消费金额;',
        'unpaid_count' => 'label=待付款订单数量;',
        'unshipped_count' => 'label=待收货订单数量;',
        'received_count' => 'label=已收货订单数量;',
        'evaluated_count' => 'label=待评价数量;',
        'complete_count' => 'label=已完成订单数量;',
        'refund_count' => 'label=退货订单数量;',
    ];

    public function run($param, $uid)
    {
        $order_count= M('order')->where(['uid' => $uid,'order_status' => ['NEQ', 6]])->count();
        $total_money= M('order')->where(['uid' => $uid, 'order_status' => ['in', '1,2,3,5']])->sum('pay_money');
        $ret0= M('order')->where(['uid' => $uid,'order_status' => 0])->count();
        $ret1= M('order')->where(['uid' => $uid,'order_status' => 1])->count();
        $ret2= M('order')->where(['uid' => $uid,'order_status' => 2])->count();
        $ret3= M('order')->where(['uid' => $uid,'order_status' => 3])->count();
        $ret5= M('order')->where(['uid' => $uid,'order_status' => 5])->count();
        $ret6= M('order')->where(['uid' => $uid,'order_status' => 6])->count();
       $result['order_count'] = $order_count?$order_count:0;
       $result['sum_price'] = $total_money?$total_money:0.00;
       $result['unpaid_count'] = $ret0?$ret0:0;
       $result['unshipped_count'] = $ret1?$ret1:0;
       $result['received_count'] = $ret2?$ret2:0;
       $result['evaluated_count'] = $ret3?$ret3:0;
       $result['complete_count'] = $ret5?$ret5:0;
       $result['refund_count'] = $ret6?$ret6:0;
        return $result;
    }
}
