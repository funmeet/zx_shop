<?php
// +-------------------------------------------------------------------
// |
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Service\Api\V1;

class RushList
{
    public $login = false; //是否需要登录才能访问该接口
    public $title = "首页/抢购商品列表OK";
    public $group = '商品';
    public $desc = "";

    public $input = [
        'page_num' => 'int;label=分页;comment=默认为1',
        'page_size' => 'int;label=分页条数;comment=默认为5',
    ];

    public $output = [
        'list' => [
            [
                'id' => 'label=id;',
                'list_cover' => 'label=列表封面图;',
                'sell_price' => 'label=零售价;',
                'vip_price' => 'label=会员价;',
                'give_point' => 'label=赠送的积分;',
            ]
        ]
    ];

    public function run($param, $uid)
    {
        $page_num = $param['page_num'] ?: 1;
        $page_size = $param['page_size'] ?: 50;
        list($list, $page_count, $page_total) = OE('goods')->rushList($page_num, $page_size);
        return [
            'page_num' => $page_num,
            'page_size' => $page_size,
            'page_count' => $page_count,
            'page_total' => $page_total,
            'list' => $list,
        ];
    }
}
