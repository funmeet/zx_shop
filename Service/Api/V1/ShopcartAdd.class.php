<?php
// +-------------------------------------------------------------------
// |
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Service\Api\V1;

class ShopcartAdd
{
    public $login = true; //是否需要登录才能访问该接口
    public $title = "加入购物车OK";
    public $group = '购物车';
    public $desc = "";

    public $input = [
        'goods_id' => 'required;int;label=商品id;',
        'buy_num' => 'required;int;label=数量;',
        'spec_value' => 'string;label=商品规格;comment=json格式:[{"sku_id":"16","name":"S"},{"sku_id":"21","name":"白色"}];',
    ];

    public $output = [
        'cart_id' => 'label=购物车id;'
    ];

    public function run($param, $uid)
    {

        $ret = OE('order')->addCart($param, $uid);
        return ['cart_id' => $ret];
    }
}
