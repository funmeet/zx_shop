<?php
// +-------------------------------------------------------------------
// |
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Service\Api\V1;

use Think\Exception;

class BindMobile
{
    public $login = true; //是否需要登录才能访问该接口
    public $title = "绑定手机号";
    public $group = '个人中心';
    public $desc = "";

    public $input = [
        'code' => 'required;label=验证码',
        'mobile' => 'required;mobile;label=收件人手机号',
    ];

    public $output = [

    ];

    public function run($param, $uid, $login_info)
    {
        $ucenter = M('ucenter_member')->where(['mobile' =>$param['mobile']])->find();
        if(isset($ucenter['id'])) throw new Exception('该手机已绑定');
        $sms = M('sms_content')->where(['uid' => $uid, 'mobile' => $param['mobile']])->order('id desc')->find();

        if($sms['code'] == $param['code']){
            if($sms['expire_time']<time()) throw new Exception('验证码已过期');
            //绑定手机
            $flag = M('ucenter_member')->where(['id' =>$uid])->save(['mobile' => $param['mobile']]);

            M('user')->where(['uid' =>$uid])->save(['mobile' => $param['mobile']]);
            if(!$flag){
                throw new \Exception('绑定失败');
            }
        }else{
            throw new \Exception('验证码不正确');
        }
        return [];
    }
}