<?php
// +-------------------------------------------------------------------
// |
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Service\Api\V1;

class NoticeList
{
    public $login = true; //是否需要登录才能访问该接口
    public $title = "通知列表";
    public $group = '通知';
    public $desc = "";

    public $input = [
        'type' => 'required;enum=0,1,2;label=类型;comment=0客服回答1订单已发货',
    ];

    public $output = [
        'list' => [
            [
                'type' => 'label=类型;comment=0客服回答1订单已发货2系统通知',
                'type_id' => 'label=对应类型的id;comment=0客服回答为问题id 1订单已发货为订单号',
                'content' => 'label=内容;',
                'create_time' => 'label=时间;',
            ]
        ],
    ];

    public function run($param, $uid)
    {
        $list = OE('user')->noticeList($uid, $param['type']);
        return [
            'list' => $list
        ];
    }
}
