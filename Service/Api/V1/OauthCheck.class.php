<?php
// +-------------------------------------------------------------------
// |
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Service\Api\V1;

class OauthCheck
{
    public $login = false; //是否需要登录才能访问该接口
    public $title = "Oauth授权";
    public $group = '用户';
    public $desc = "微信浏览器内才需用调用";

    public $input = [
        'jump_url' => 'required;hosturl;label=授权成功后的返回地址;comment=浏览器当前的完整地址http://xxx'
    ];

    public $output = [
        'is_oauth' => 'label=是否已授权;comment=false时则应跳转到授权地址oauth_url',
        'oauth_url' => 'label=授权跳转地址;comment=/oauth?jump_url＝jump_url;',
        'api_token' => 'label=用户标识;comment=没值则表示用户未绑定，有值则表示用户已绑定，应覆盖客户端的token',
        'expire_time' => 'label=token过期时间;',
    ];

    public function run($param, $uid)
    {
        list($api_token, $expire_time) = O('user')->getTokenByOauth($uid);
        return [
            'is_oauth' => (bool)O('Kcdns\Service\User')->currentOauth(),
            'oauth_url' => '/oauth?jump_url=' . urlencode($param['jump_url']),
            'api_token' => $api_token,
            'expire_time' => $expire_time,
        ];
    }
}
