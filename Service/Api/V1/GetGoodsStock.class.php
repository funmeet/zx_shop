<?php
// +-------------------------------------------------------------------
// |
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Service\Api\V1;

use Think\Model;

class GetGoodsStock
{
    public $login = false; //是否需要登录才能访问该接口
    public $title = "获取商品库存OK";
    public $group = '商品';
    public $desc = "";

    public $input = [
        'goods_id' => 'required;int;label=商品id;',
        'sku_ids' => 'required;string;label=规格id;comment=多个使用"-"分隔;',
    ];

    public $output = [
        'stock' => 'label=库存;',
    ];

    public function run($param, $uid, $login_info)
    {
        $where['goods_id'] = $param['goods_id'];
        $sku_ids_arr = explode('-', $param['sku_ids']);
        asort($sku_ids_arr);
        $where['sku_ids'] = join('-', $sku_ids_arr);
        $data = M('goods_sku')->where($where)->find();
        if(!isset($data['id'])) throw new \Exception('未找到商品库存信息');
        $result['stock'] = $data['stock'];
        return $result;
    }
}
