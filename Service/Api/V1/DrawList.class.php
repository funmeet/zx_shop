<?php
// +-------------------------------------------------------------------
// |
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Service\Api\V1;

class DrawList
{
    public $login = true; //是否需要登录才能访问该接口
    public $title = "003提现明细";
    public $group = '财务';
    public $desc = "";

    public $input = [
        'status' => 'required;enum=0,1,2,10;label=提现状态;comment=0已提交1已发放2已驳回 10所有',
        'page_num' => 'int;label=分页;comment=默认为1',
        'page_size' => 'int;label=分页条数;comment=默认为5',
    ];

    public $output = [
        'page_num' => 'label=当前页码;',
        'page_size' => 'label=每页条数;',
        'page_count' => 'label=总条数;',
        'page_total' => 'label=总页数;',
        'total_draw_money' => 'label=已提现总额;',
        'list' => [
            [
                'draw_money' => 'label=提现金额;',
                'fee_money' => 'label=手续费;comment=',
                'pay_money' => 'label=发放金额;',
                'create_time' => 'label=创建时间;',
                'update_time' => 'label=受理时间;',
                'remark' => 'label=备注;',
                'real_name' => 'label=真实姓名;',
                'name' => 'label=支付宝收款人;',
                'bank_no' => 'label=银行卡号;',
                'bank_type' => 'label=开户行;',
            ]
        ]
    ];

    public function run($param, $uid)
    {
        $page_num = $param['page_num'] ?: 1;
        $page_size = $param['page_size'] ?: 50;
        list($list, $page_count, $page_total, $total_draw_money) = OE('user')->drawList($uid, $param['status'], $page_num, $page_size);
        return [
            'page_num' => $page_num,
            'page_size' => $page_size,
            'page_count' => $page_count,
            'page_total' => $page_total,
            'total_draw_money' => $total_draw_money,
            'list' => $list,
        ];
    }
}
