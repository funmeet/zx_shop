<?php
// +-------------------------------------------------------------------
// |
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Service\Api\V1;

class OneGoodsTypeList
{
    public $login = false; //是否需要登录才能访问该接口
    public $title = "首页/一级分类和banner图OK";
    public $group = '商品';
    public $desc = "";

    public $input = [
    ];

    public $output = [
        'list' => [
            [
                'id' => 'label=id;',
                'title' => 'label=分类名称;',
            ]
        ],
        'banner' => [
            [
                'id' => 'label=id;',
                'pic_url' => 'label=图片地址;',
                'link' => 'label=链接;',
            ]
        ]
    ];

    public function run($param, $uid)
    {
        list($list, $banner) = OE('goodstype')->oneGoodsTypeList();
        return [
            'list' => $list,
            'banner' => $banner,
        ];
    }
}
