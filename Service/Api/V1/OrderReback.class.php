<?php
// +-------------------------------------------------------------------
// |
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Service\Api\V1;

class OrderReback
{
    public $login = true; //是否需要登录才能访问该接口
    public $title = "退款申请";
    public $group = '订单';
    public $desc = "";

    public $input = [
        'order_no' => 'required;bigint;label=订单号',
    ];

    public $output = [
    ];

    public function run($param, $uid)
    {
        OE('order')->reback($param['order_no'], $uid);
        return [];
    }
}
