<?php
// +-------------------------------------------------------------------
// |
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Service\Api\V1;

class QrcodeDetail
{
    public $login = true; //是否需要登录才能访问该接口
    public $title = "我的推广码";
    public $group = '个人中心';
    public $desc = "";

    public $input = [
    ];

    public $output = [
        [
        'invite_url' => 'label=推荐链接',
        'invite_url_1' => 'label=推荐链接url',
        'invite_intro' => 'label=权益介绍',
            ]
    ];

    public function run($param, $uid, $login_info)
    {
        if($login_info['vip_level'] == 0 ){
            throw new \Exception('必须先报单升级会员，才允许推广下线');
        }
        $result['invite_url_1'] = "http://zxshop.jiuyuanmall.cn/wap/index/wxReg?inviteCode={$login_info['invite_code']}";
        $result['invite_url'] = "http://zxshop.jiuyuanmall.cn/wap/index/wxReg?inviteCode={$login_info['invite_code']}";
        $result['invite_url']  = 'http://zxwww.jiuyuanmall.cn/w1'. OE('util')->qrcode($result['invite_url'], 12);
//        $result['invite_url']  = "http://hiphotos.baidu.com/news/crop%3D0%2C0%2C899%2C489%3Bq%3D80%3B/sign=0e42e7fd057b0208188665a15fe9dee3/b17eca8065380cd7f1676db5ae44ad3459828169.jpg";
        $result['invite_intro'] = "权益介绍权益介绍权益介绍权益介绍权益介绍";
        $data = [];
        $data[] = $result;
        return $data;
    }
}