<?php
// +-------------------------------------------------------------------
// |
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Service\Api\V1;

class CommissionLog
{
    public $login = true; //是否需要登录才能访问该接口
    public $title = "佣金明细";
    public $group = '财务';
    public $desc = "";

    public $input = [
        'page_num' => 'int;label=分页;comment=默认为1',
        'page_size' => 'int;label=分页条数;comment=默认为5',
    ];

    public $output = [
        'total_commission' => 'label=总佣金;',
        'list' => [
            [
                'title' => 'label=标题;',
                'type' => 'label=佣金类型;',
                'settle_status' => 'label=结算状态;',
                'type_label' => 'label=佣金类型;',
                'settle_status_label' => 'label=结算状态备注;',
                'order_no' => 'label=订单号;',
                'money' => 'label=佣金',
                'create_time' => 'label=创建时间;',
            ]
        ]
    ];

    //{"1":"分享奖励","2":"业绩奖励","3":"复消奖励","12":"代理奖励","13":"一星红豆","14":"二星红豆","15":"三星红豆","16":"四星红豆","17":"五星红豆","18":"推荐代理分成"}
    public function run($param, $uid)
    {
        $page_num = $param['page_num'] ?: 1;
        $page_size = $param['page_size'] ?: 50;
        list($list, $total_commission) = OE('user')->commissionListAll($uid);
        return [
            'total_commission' => $total_commission,
            'list' => $list,
        ];
    }
}
