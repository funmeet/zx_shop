<?php
// +-------------------------------------------------------------------
// |
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Service\Api\V1;

class AddressDetail
{
    public $login = true; //是否需要登录才能访问该接口
    public $title = "详情";
    public $group = '地址';
    public $desc = "";

    public $input = [
        'address_id' => 'required;int;label=地址id',
    ];

    public $output = [
        'id' => 'label=id',
        'name' => 'label=收件人姓名',
        'mobile' => 'label=收件人手机号',
        'mobile_secure' => 'label=收件人手机号',
        'area' => 'label=地区id',
        'area_label' => 'label=地区名称',
        'address' => 'label=详细地址',
        'is_default' => 'label=是否为默认地址',
    ];

    public function run($param, $uid)
    {
        $result=OE('address')->getDetail($param['address_id'], $uid);
        return $result;
    }
}