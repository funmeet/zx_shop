<?php
// +-------------------------------------------------------------------
// |
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Service\Api\V1;

class ShopcartList
{
    public $login = true; //是否需要登录才能访问该接口
    public $title = "购物车清单OK";
    public $group = '购物车';
    public $desc = "";

    public $input = [
    ];

    public $output = [
        'list' => [
            [
                'id' => 'label=id;',
                'goods_id' => 'label=商品id;',
                'buy_num' => 'label=购买数量;',
                'title' => 'label=标题;',
                'intro' => 'label=描述;',
                'sell_price' => 'label=零售价;',
                'vip_price' => 'label=会员价;',
                'max_buy' => 'label=限购;',
                'is_rebuy' => 'label=是否复购;',
                'contribution' => 'label=贡献;',
                'deviation_point' => 'label=抵扣积分;',
                'give_point' => 'label=赠送积分;',
                'stock' => 'label=库存;',
                'postage' => 'label=邮费;',
                'list_cover' => 'label=列表封面;comment=单图',
                'unit' => 'label=单位',
                'spec_label' => [
                    [
                        'sku_id' => 'label=规格id;',
                        'name' => 'label=规格名称;',
                    ]
                ]
            ]
        ],
        'cartList' => 'label=处理后的数据;'
    ];

    public function run($param, $uid, $login_info)
    {
        $list = OE('order')->cartList($uid);
        $valid = [];
        foreach($list as $k => $v){
            $sku_name = array_column($v['spec_label'], 'name');
            $sku_id_arr = array_column($v['spec_label'], 'sku_id');

            //获取库存
            $where['goods_id'] = $v['goods_id'];
            asort($sku_id_arr);
            if(count($sku_id_arr) == 1){
                $sku_id_arr[]= 0;
            }
            $where['sku_ids'] = join('-', $sku_id_arr);
            $data = M('goods_sku')->where($where)->find();
            $stock = $data['stock']?$data['stock']:0;

            if($login_info['vip_level'] == 0){
                $price = $data['sell_price'];
            }elseif($login_info['vip_level'] == 4){
                $price = $data['agent_price'];
            }else{
                $price = $data['vip_price'];
            }
            $vip_price = $data['vip_price'];

            $vip_list = get_list_config('VIP_LEVEL', true);
            $agentTotalNum = 0;
            if($login_info['vip_level'] == 1 || $login_info['vip_level'] == 2){
                $agentTotalNum=$vip_list[4]['buyagentorder1'];
            }

            if($login_info['vip_level'] == 3){
                $agentTotalNum=$vip_list[4]['buyagentorder2'];
            }

            $valid[] = [
                'id' => $v['id'],
                'type' => 'product',
                'product_id' => $v['goods_id'],
                'cart_num' => $v['buy_num'],
                'truePrice' => $v['buy_num']*$price,
                'viptruePrice' => $v['buy_num']*$vip_price,
                'checked' => false,
                'productInfo' => [
                    'id' => $v['goods_id'],
                    'image' => $v['list_cover'],
                    'stock' => $v['stock'],
                    'store_name' => $v['title'],
                    'is_agent' => $v['is_agent'],
                    'agentTotalNum' => $agentTotalNum,
                    'attrInfo' => [
                        'product_id' => $v['goods_id'],
                        'suk' => join(',', $sku_name),
                        'stock' => $stock,
                        'sales' => 0,
                        'image' => '',
                        'unique' => join('-', $sku_id_arr),
                    ],
                ],
            ];
        }
        return [
            'list' => $list,
            'vip_level' => $login_info['vip_level'],
            'cartList' => [
                'valid' => $valid,
                'isAllSelect' => false,
            ],
        ];
    }
}
