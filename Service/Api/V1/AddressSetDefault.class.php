<?php
// +-------------------------------------------------------------------
// |
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Service\Api\V1;

class AddressSetDefault
{
    public $login = true; //是否需要登录才能访问该接口
    public $title = "设置默认";
    public $group = '地址';
    public $desc = "";

    public $input = [
        'id' => 'required;label=id',
    ];

    public $output = [

    ];

    public function run($param, $uid)
    {
        OE('address')->setAddress($param, $uid);
        return [];
    }
}