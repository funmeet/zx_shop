<?php
// +-------------------------------------------------------------------
// |
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Service\Api\V1;

class MoneyList
{
    public $login = true; //是否需要登录才能访问该接口
    public $title = "资金明细";
    public $group = '财务';
    public $desc = "";

    public $input = [
        'page_num' => 'int;label=分页;comment=默认为1',
        'page_size' => 'int;label=分页条数;comment=默认为5',
    ];

    public $output = [
        'page_num' => 'label=当前页码;',
        'page_size' => 'label=每页条数;',
        'page_count' => 'label=总条数;',
        'page_total' => 'label=总页数;',
        'balance' => 'label=余额;',
        'frozen_money' => 'label=冻结金额;',
        'list' => [
            [
                'type' => 'label=资金类型;',
                'type_label' => 'label=资金类型;',
                'change_money' => 'label=变更金额;',
                'old_balance' => 'label=变更前金额;',
                'new_balance' => 'label=变更后金额;',
                'create_time' => 'label=时间;',
            ]
        ]
    ];

    public function run($param, $uid, $login_info)
    {
        $page_num = $param['page_num'] ?: 1;
        $page_size = $param['page_size'] ?: 5;
        list($list, $page_count, $page_total) = OE('user')->moneyList($uid, $page_num, $page_size);
        return [
            'page_num' => $page_num,
            'page_size' => $page_size,
            'page_count' => $page_count,
            'page_total' => $page_total,
            'balance' => $login_info['balance'],
            'frozen_money' => $login_info['frozen_money'],
            'list' => $list,
        ];
    }
}
