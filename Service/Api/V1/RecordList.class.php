<?php
// +-------------------------------------------------------------------
// |
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Service\Api\V1;

class RecordList
{
    public $login = true; //是否需要登录才能访问该接口
    public $title = "足迹列表";
    public $group = '我的足迹';
    public $desc = "";

    public $input = [
    ];

    public $output = [
        [
            'day_date' => 'label=日期;',
            'list' => [
                [
                    'goods_id' => 'label=商品id;' ,
                    'title' => 'label=商品名称;' ,
                    'sell_price' => 'label=零售价;' ,
                    'vip_price' => 'label=会员价;' ,
                    'sell_num' => 'label=销量;' ,
                    'list_cover' => 'label=列表封面图;',
                ]
            ],
        ]
    ];

    public function run($param, $uid)
    {
        $list = M('goods_spoor')->where(['uid' => $uid])->order('id desc')->select();
        foreach($list as &$item){
            $goods = M('goods')->where(['id' => $item['goods_id']])->find();
            $item['title'] = $goods['title'];
            $item['sell_price'] = $goods['sell_price'];
            $item['vip_price'] = $goods['vip_price'];
            $item['sell_num'] = $goods['sell_num'];
            $item['list_cover'] = get_cover($goods['list_cover'], 'url');
        }
        $result = [];
        foreach($list as $k => $v){
            $tmp =$v;
            unset($tmp['day_date']);
            $result[$v['day_date']][] = $tmp;
        }
        $data = [];
        foreach($result as $k => $v){
            $ret1 = [];
            $ret1['day_date'] = date('m月d日', strtotime($k.' 00:00:00'));
            foreach($v as $k1 => $v1){
                $ret1['list'][] = $v1;
            }
            $data[] = $ret1;
        }
        return $data;
    }
}