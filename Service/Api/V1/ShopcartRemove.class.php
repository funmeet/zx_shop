<?php
// +-------------------------------------------------------------------
// |
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Service\Api\V1;

class ShopcartRemove
{
    public $login = true; //是否需要登录才能访问该接口
    public $title = "移除商品OK";
    public $group = '购物车';
    public $desc = "";

    public $input = [
        'ids[]' => 'required;label=购物车id;comment=多个id用数组形式,例如ids[]＝123&ids[]＝124',
    ];

    public $output = [
    ];

    public function run($param, $uid)
    {
        OE('order')->removeGoods($param['ids'], $uid);
        return;
    }
}
