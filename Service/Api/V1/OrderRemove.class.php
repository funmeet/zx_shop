<?php
// +-------------------------------------------------------------------
// |
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Service\Api\V1;

class OrderRemove
{
    public $login = true; //是否需要登录才能访问该接口
    public $title = "取消订单";
    public $group = '订单';
    public $desc = "";

    public $input = [
        'order_no' => 'required;bigint;label=订单号',
    ];

    public $output = [
    ];

    public function run($param, $uid)
    {
        OE('order')->remove($param['order_no'], $uid);
        return [];
    }
}
