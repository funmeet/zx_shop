<?php
// +-------------------------------------------------------------------
// |
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Service\Api\V1;

class GetPwdSmsCode
{
    public $login = false; //是否需要登录才能访问该接口
    public $title = "获取找回密码短信验证码";
    public $group = '用户';
    public $desc = "";

    public $input = [
        'mobile' => 'required;label=手机号;',
    ];

    public $output = [
    ];

    public function run($param, $uid)
    {
        $info = M('ucenter_member')->where(['mobile' => $param['mobile']])->find();
        if (!$info) {
            throw new \Exception("该手机号不存在");
        }

        $result = O('sms')->sendCode($param['mobile'], '', 'findPwd');
        if (!$result) {
            throw new \Exception(O('sms')->getError());
        }
        return [];
    }
}
