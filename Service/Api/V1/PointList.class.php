<?php
// +-------------------------------------------------------------------
// |
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Service\Api\V1;

class PointList
{
    public $login = true; //是否需要登录才能访问该接口
    public $title = "积分明细";
    public $group = '用户';
    public $desc = "";

    public $input = [
        'page_num' => 'int;label=分页;comment=默认为1',
        'page_size' => 'int;label=分页条数;comment=默认为5',
    ];

    public $output = [
        'page_num' => 'label=当前页码;',
        'page_size' => 'label=每页条数;',
        'page_count' => 'label=总条数;',
        'page_total' => 'label=总页数;',
        'total_point' => 'label=总积分;',
        'list' => [
            [
                'goods_title' => 'label=商品标题;',
                'type' => 'label=积分类型;comment=0支付赠送1下单抵扣2取消返还',
                'type_label' => 'label=积分类型;',
                'point' => 'label=积分;comment=大于0的是赠送的,小于0的是抵扣的',
                'create_time' => 'label=创建时间;',
            ]
        ]
    ];

    public function run($param, $uid)
    {
        $page_num = $param['page_num'] ?: 1;
        $page_size = $param['page_size'] ?: 5;
        list($list, $page_count, $page_total, $total_point) = OE('user')->pointList($uid, $page_num, $page_size);
        return [
            'page_num' => $page_num,
            'page_size' => $page_size,
            'page_count' => $page_count,
            'page_total' => $page_total,
            'total_point' => $total_point,
            'list' => $list,
        ];
    }
}
