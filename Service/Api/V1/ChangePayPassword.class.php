<?php
// +-------------------------------------------------------------------
// |
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Service\Api\V1;

class ChangePayPassword
{
    public $login = true; //是否需要登录才能访问该接口
    public $title = "修改支付密码";
    public $group = '用户';
    public $desc = "";

    public $input = [
        'old_password' => 'required;length=6,;label=旧支付密码;',
        'pay_password' => 'required;length=6,;label=支付密码;',
    ];

    public $output = [
    ];

    public function run($param, $uid)
    {
        OE('user')->changePayPassword($param, $uid);
        return [];
    }
}
