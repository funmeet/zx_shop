<?php
// +-------------------------------------------------------------------
// |
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Service\Api\V1;

use Think\Exception;

class BindOldUser
{
    public $login = true; //是否需要登录才能访问该接口
    public $title = "绑定老会员";
    public $group = '用户';
    public $desc = "";

    public $input = [
        'mobile' => 'required;label=手机号;',
        'code' => 'required;label=验证码;',
    ];

    public $output = [
        'expires_time' => 'label=过期时间;',
        'token' => 'label=token;',
    ];

    public function run($param, $uid, $login_info)
    {
        throw new Exception('暂未开放');
        $result = OE('user')->bindOldUser($param, $uid, $login_info);
        return $result;
    }
}
