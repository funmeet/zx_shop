<?php
// +-------------------------------------------------------------------
// |
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Service\Api\V1;

class ResetPassword
{
    public $login = false; //是否需要登录才能访问该接口
    public $title = "重置密码";
    public $group = '用户';
    public $desc = "";

    public $input = [
        'mobile' => 'required;label=手机号;',
        'new_password' => 'required;length=6,;label=新密码;',
        'confirm_password' => 'required;length=6,;label=确认密码;',
        'code' => 'required;label=短信验证码;',
    ];

    public $output = [
    ];

    public function run($param)
    {
        OE('user')->findPwd($param);
        return [];
    }
}
