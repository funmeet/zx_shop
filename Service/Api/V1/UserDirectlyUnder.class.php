<?php
// +-------------------------------------------------------------------
// |
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Service\Api\V1;

class UserDirectlyUnder
{
    public $login = true; //是否需要登录才能访问该接口
    public $title = "直属下级";
    public $group = '用户';
    public $desc = "";

    public $input = [
        'uid' => 'label=用户id;comment=不传等于当前用户',
    ];

    public $output = [
        'list' => [
            [
                'uid' => 'label=用户id;',
                'avatar_url' => 'label=头像地址;',
                'real_name' => 'label=真实姓名;',
                'nickname' => 'label=昵称;',
                'mobile' => 'label=手机号;',
                'vip_level'=>'label=会员级别;',
                'vip_level_label'=>'label=会员级别;',
            ]
        ]
    ];

    public function run($param, $uid)
    {
        $list = OE('user')->getUserDirectlyUnder($uid, $param['uid']);
        return ['list' => $list];
    }
}
