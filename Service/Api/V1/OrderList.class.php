<?php
// +-------------------------------------------------------------------
// |
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Service\Api\V1;

class OrderList
{
    public $login = true; //是否需要登录才能访问该接口
    public $title = "列表";
    public $group = '订单';
    public $desc = "";

    public $input = [
        'type' => 'required;int;enum=0,1,2,3,5,6;label=订单状态;comment=0已提交1已支付2已发货3已收货5已完成6退货 ',
        'page_num' => 'int;label=分页;comment=默认为1',
        'page_size' => 'int;label=分页条数;comment=默认为5',
    ];

    public $output = [
        'page_num' => 'label=当前页码;',
        'page_size' => 'label=每页条数;',
        'page_count' => 'label=总条数;',
        'page_total' => 'label=总页数;',
        'list' => [
            [
                'order_no' => 'label=订单号;',
                'order_status' => 'label=订单状态;comment=订单状态0已提交1已支付2已发货3已收货4取消5',
                'order_status_label' => 'label=订单状态描述;',
                'order_contribution' => 'label=订单总贡献;',
                'give_point' => 'label=订单赠送积分;',
                'deviation_point' => 'label=订单抵扣积分;',
                'friend_fee_money' => 'label=代付抵扣金额;comment=',
                'postage' => 'label=订单邮费;',
                'total_money' => 'label=应付金额;',
                'pay_money' => 'label=支付金额;',
                'pay_time' => 'label=支付时间;',
                'create_time' => 'label=创建时间;',
                'goods' => [
                    [
                        'title' => 'label=商品标题;',
                        'sell_price' => 'label=零售价;',
                        'vip_price' => 'label=会员价;',
                        'max_buy' => 'label=限购数量',
                        'buy_num' => 'label=购买数量;',
                        'goods_total_money' => 'label=应付金额;',
                        'goods_pay_money' => 'label=实付金额;',
                        'goods_deviation_point' => 'label=商品总抵扣积分;',
                        'goods_give_point' => 'label=商品总赠送积分;',
                        'goods_contribution' => 'label=商品总贡献值;',
                        'intro' => 'label=商品描述;',
                        'list_cover' => 'label=图片;',
                        'spec_list' => 'label=规格;',
                    ]
                ]
            ]
        ],
        'orderList' => 'label=处理的订单数据;'
    ];

    public function run($param, $uid)
    {
        $page_num = $param['page_num'] ?: 1;
        $page_size = $param['page_size'] ?: 50;
        list($list, $page_count, $page_total) = OE('order')->lists($param['type'], $uid, $page_num, $page_size);
        $orderList = [];
        foreach($list as &$item){
            $tmp['add_time'] = $item['create_time'];
            $tmp['order_id'] = $item['order_no'];
            $tmp['pay_price'] = $item['pay_money'];
            $tmp['total_num'] = $item['buy_num'];
            $tmp['total_price'] = $item['total_money'];
            $tmp['pay_postage'] = $item['postage'];
            $tmp['total_postage'] = $item['postage'];
            $tmp['status'] = $item['order_status'];
            $tmp['shipping_type'] = 1;
            $tmp['_status'] = [
                '_type' => $item['order_status'],
                '_title' => '',
                '_msg' => '',
                '_class' => '',
                '_payType' => '',
            ];
            $cartInfo = [];
            foreach($item['goods'] as $k => $v){
                $Info = [];
                $Info['product_id'] = $v['goods_id'];
                $Info['cart_num'] = $v['buy_num'];
                $Info['truePrice'] = $v['buy_num'];
                $Info['vip_truePrice'] = $v['buy_num'];
                $Info['combination_id'] = 0;
                $Info['bargain_id'] = 0;
                $Info['seckill_id'] = 0;
                $Info['productInfo'] = [
                    'id' => $v['id'],
                    'image' => $v['list_cover'],
                    'store_name' => $v['title'],
                    'price' => $v['sell_price'],
                    'vip_price' => $v['vip_price'],
                    'spec_list' => $v['spec_list'],
                ];
                $cartInfo[] =  $Info;

            }
            $tmp['cartInfo'] = $cartInfo;
            $orderList[] = $tmp;
        }
        return [
            'page_num' => $page_num,
            'page_size' => $page_size,
            'page_count' => $page_count,
            'page_total' => $page_total,
            'list' => $list,
            'orderList' => $orderList,
        ];
    }
}
