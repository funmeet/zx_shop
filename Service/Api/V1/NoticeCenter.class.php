<?php
// +-------------------------------------------------------------------
// |
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Service\Api\V1;

class NoticeCenter
{
    public $login = true; //是否需要登录才能访问该接口
    public $title = "通知中心";
    public $group = '通知';
    public $desc = "";

    public $input = [
    ];

    public $output = [
        'list' => [
            [
                'type' => 'label=类型;comment=0客服回答1订单已发货',
                'type_label' => 'label=类别标题;',
                'content' => 'label=内容;',
                'create_time' => 'label=时间;',
                'is_read' => 'label=是否已读;comment=0未读1已读',
            ]
        ]
    ];

    public function run($param, $uid)
    {
        $list = OE('user')->noticeCenter($uid);
        return [
            'list' => $list
        ];
    }
}
