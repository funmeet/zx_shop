<?php
// +-------------------------------------------------------------------
// |
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Service\Api\V1;

use Think\Exception;

class RecordSubmit
{
    public $login = true; //是否需要登录才能访问该接口
    public $title = "添加我的足迹";
    public $group = '我的足迹';
    public $desc = "";

    public $input = [
        'goods_id' => 'required;int;label=商品id',
    ];

    public $output = [

    ];

    public function run($param, $uid)
    {
        $ret = M('goods_spoor')->where(['uid' => $uid, 'goods_id' =>$param['goods_id']])->find();
        if($ret) throw new Exception('已添加该商品');
        M('goods_spoor')->add([
            'goods_id' => $param['goods_id'],
            'day_date' => date('Y-m-d'),
            'uid' => $uid,
        ]);
        return [];
    }
}