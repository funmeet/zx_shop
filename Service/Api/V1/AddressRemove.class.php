<?php
// +-------------------------------------------------------------------
// |
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Service\Api\V1;

class AddressRemove
{
    public $login = true; //是否需要登录才能访问该接口
    public $title = "删除";
    public $group = '地址';
    public $desc = "";

    public $input = [
        'address_id' => 'required;int;label=地址id',
    ];

    public $output = [

    ];

    public function run($param, $uid)
    {
        OE('address')->remove($param['address_id'], $uid);
        return [];
    }
}