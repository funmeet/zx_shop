<?php
// +-------------------------------------------------------------------
// |
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Service\Api\V1;

class GoodsTwoTypeList
{
    public $login = false; //是否需要登录才能访问该接口
    public $title = "首页/商品二级分类列表OK";
    public $group = '商品';
    public $desc = "";

    public $input = [
        'page_num' => 'int;label=分页;comment=默认为1',
        'page_size' => 'int;label=分页条数;comment=默认为5',
    ];

    public $output = [
        'one_category' => [
            [
                'id' => 'label=分类id',
                'title' => 'label=分类名称',
                'two_category' => [
                    [
                        'id' => 'label=分类id',
                        'title' => 'label=分类名称',
                        'pic' => 'label=分类图标',
                    ]
                ]
            ]
        ]
    ];

    public function run($param, $uid)
    {
        $page_num = $param['page_num'] ?: 1;
        $page_size = $param['page_size'] ?: 5;
        list($list, $page_count, $page_total) = OE('goods')->goodsTwoTypeList($page_num, $page_size, $param);
        return [
            'page_num' => $page_num,
            'page_size' => $page_size,
            'page_count' => $page_count,
            'page_total' => $page_total,
            'one_category' => $list,
        ];
    }
}
