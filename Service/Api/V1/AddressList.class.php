<?php
// +-------------------------------------------------------------------
// |
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Service\Api\V1;

class AddressList
{
    public $login = true; //是否需要登录才能访问该接口
    public $title = "列表";
    public $group = '地址';
    public $desc = "";

    public $input = [
        'keyword' => 'label=搜索关键字',
    ];

    public $output = [
        [
            'id' => 'label=id',
            'name' => 'label=收件人姓名',
            'mobile_secure' => 'label=收件人手机号',
            'area' => 'label=地区id',
            'area_label' => 'label=地区名称',
            'address' => 'label=详细地址',
            'is_default' => 'label=是否为默认地址',
            'is_remote' => 'label=是否为偏远地区',
        ]
    ];

    public function run($param, $uid)
    {
        $list = OE('address')->lists($uid, $param);
        return $list;
    }
}