<?php
// +-------------------------------------------------------------------
// |
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Service\Api\V1;

class DrawCash
{
    public $login = true; //是否需要登录才能访问该接口
    public $title = "002支付宝提现提交";
    public $group = '财务';
    public $desc = "";

    public $input = [
        'real_name' => 'required;label=真实姓名',
        'name' => 'required;label=支付宝账号',
        'draw_money' => 'required;number;min=0.01;label=提现金额;comment=',
    ];

    public $output = [
    ];

    public function run($param, $uid, $login_info)
    {
        OE('user')->drawCash($param, $uid, $login_info);
        return [];
    }
}
