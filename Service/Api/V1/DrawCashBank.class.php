<?php
// +-------------------------------------------------------------------
// |
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Service\Api\V1;

class DrawCashBank
{
    public $login = true; //是否需要登录才能访问该接口
    public $title = "020银行卡提现提交";
    public $group = '财务';
    public $desc = "";

    public $input = [
        'real_name' => 'required;label=真实姓名',
        'bank_no' => 'required;label=银行卡号',
        'bank_type' => 'required;label=开户行',
        'draw_money' => 'required;number;min=0.01;label=提现金额;comment=',
    ];

    public $output = [
    ];

    public function run($param, $uid, $login_info)
    {
        OE('user')->drawCashBank($param, $uid, $login_info);
        return [];
    }
}
