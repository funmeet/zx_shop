<?php
// +-------------------------------------------------------------------
// |
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Service\Api\V1;

use Think\Exception;

class OrderSubmitBalance
{
    public $login = false; //是否需要登录才能访问该接口
    public $title = "普通下单";
    public $group = '订单';
    public $desc = "";

    public $input = [
        'cart_ids' => 'required;string;label=购物车id;comment=多个id使用英文逗号隔开;',
        'address_id' => 'required;int;label=地址id',
        'red_packets' => 'label=使用红包金额;comment=0或不填则不使用红包',
        'remark' => 'label=备注',
        'pay_type' => 'required;int;enum=1,3,4;label=支付方式;comment=1余额支付 3微信h5支付 4微信公众号支付',
        'pay_password' => 'label=支付密码;comment=余额支付为必填',
    ];

    public $output = [
        'order_no' => 'required;bigint;label=订单号;comment=用于支付订单',
        'wxjs' => [
            'appId' => 'label=appId;comment=用于微信公众号支付',
            'timeStamp' => 'label=timeStamp;comment=用于微信公众号支付',
            'nonceStr' => 'label=nonceStr;comment=用于微信公众号支付',
            'package' => 'label=package;comment=用于微信公众号支付',
            'signType' => 'label=signType;comment=用于微信公众号支付',
            'paySign' => 'label=paySign;comment=用于微信公众号支付',
        ]
    ];

    public function run($param, $uid)
    {

//        throw new Exception('暂未开放');
        if($param['pay_type'] == 1){
            throw new Exception('余额支付暂未开放');
        }
//        $uid = $uid?$uid:10002;
//        OE('user')->chenckmMobile($param['phone'],$uid);
        $order_no = OE('order')->submit($param, $uid);
        $wxjs = [];
        $param['order_no'] = $order_no;
        if($param['pay_type'] == 4){
            $wxjs = OE('order')->pay($param, $uid);
        }
        return [
            'order_no' => $order_no,
            'wxjs' => $wxjs,
        ];
    }
}
