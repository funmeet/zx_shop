<?php
// +-------------------------------------------------------------------
// |
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Service\Api\V1;

class AddressSubmit
{
    public $login = true; //是否需要登录才能访问该接口
    public $title = "新增";
    public $group = '地址';
    public $desc = "";

    public $input = [
        'name' => 'required;label=收件人姓名',
        'mobile' => 'required;mobile;label=收件人手机号',
        'area' => 'required;label=地区;comment=例子1,36,37',
        'address' => 'required;label=详细地址',
        'is_default' => 'required;enum=0,1;label=是否为默认地址;comment=0不设为默认1为默认',
    ];

    public $output = [

    ];

    public function run($param, $uid)
    {
        OE('address')->submit($param, $uid);
        return [];
    }
}