<?php
// +-------------------------------------------------------------------
// |
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Service\Api\V1;

class OrderDetail
{
    public $login = true; //是否需要登录才能访问该接口
    public $title = "详情";
    public $group = '订单';
    public $desc = "";

    public $input = [
        'order_no' => 'required;bigint;label=订单号;',
    ];

    public $output = [
        'is_order_deal' => 'label=订单处理状态;',
        'order_no' => 'label=订单号;',
        'order_status' => 'label=订单状态;',
        'order_status_label' => 'label=订单状态描述;',
        'order_contribution' => 'label=订单总贡献;',
        'postage' => 'label=订单邮费;',
        'total_num' => 'label=订购商品总数;',
        'total_money' => 'label=应付金额;',
        'pay_money' => 'label=支付金额;',
        'pay_time' => 'label=支付时间;',
        'pay_status' => 'label=支付状态;comment=0未支付1已支付',
        'pay_type' => 'label=支付方式;',
        'pay_type_label' => 'label=支付方式;',
        'friend_name' => 'label=代付人姓名;comment=我邀请的人',
        'friend_mobile' => 'label=代付人手机号;',
        'order_owner' => 'label=订单归属人;comment=别人邀请我代付的时候',
        'order_owner_mobile' => 'label=订单归属人手机号;comment=别人邀请我代付的时候',
        'create_time' => 'label=创建时间;',
        'is_bd' => 'label=是否报单;comment=是否报单： 1是 2否',
        'is_invite' => 'label=是否代付订单;comment=0不是1是',
        'is_my_invite' => 'label=是否是我邀请的代付订单;comment=0不是1是',
        'give_point' => 'label=赠送积分;comment=',
        'deviation_point' => 'label=抵扣积分;comment=',
        'friend_fee_money' => 'label=代付抵扣金额;comment=',
        'express_company' => 'label=快递公司;comment=',
        'express_no' => 'label=快递单号;comment=',
        'express_time' => 'label=发货时间;comment=',
        'use_red_packets' => 'label=使用红包金额;comment=',
        'goods' => [
            [
                'goods_id' => 'label=商品id;',
                'title' => 'label=商品标题;',
                'sell_price' => 'label=零售价;',
                'vip_price' => 'label=会员价;',
                'is_rebuy' => 'label=是否复购;comment=0不是 1是',
                'max_buy' => 'label=限购;',
                'buy_num' => 'label=购买数量;',
                'goods_total_money' => 'label=应付金额;',
                'goods_pay_money' => 'label=实付金额;',
                'goods_deviation_point' => 'label=商品总抵扣积分;',
                'goods_give_point' => 'label=商品总赠送积分;',
                'goods_contribution' => 'label=商品总贡献值;',
                'deviation_point' => 'label=商品抵扣积分;',
                'give_point' => 'label=商品赠送积分;',
                'contribution' => 'label=商品贡献值;',
                'intro' => 'label=商品描述;',
                'list_cover' => 'label=图片;',
                'spec_list' => 'label=规格;',
            ]
        ],
        'address' => [
            'id' => 'label=id',
            'name' => 'label=收件人姓名',
            'mobile_secure' => 'label=收件人手机号',
            'area_label' => 'label=地区名称',
            'address' => 'label=详细地址',
        ]
    ];

    public function run($param, $uid)
    {
        $result = OE('order')->getDetailByOrderNO($param['order_no'], $uid);
        $order = M('order')->where(['order_no' =>$param['order_no']])->find();
        $result['total_num'] = $order['buy_num'];
        return $result;
    }
}
