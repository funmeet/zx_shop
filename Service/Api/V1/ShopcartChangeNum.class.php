<?php
// +-------------------------------------------------------------------
// |
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Service\Api\V1;

class ShopcartChangeNum
{
    public $login = true; //是否需要登录才能访问该接口
    public $title = "修改商品OK";
    public $group = '购物车';
    public $desc = "";

    public $input = [
        'id' => 'required;int;label=购物车id;',
        'buy_num' => 'required;min=1;int;label=购买数量;',
        'spec_value' => 'string;label=商品规格;comment=json格式:[{"sku_id":"16","name":"S"},{"sku_id":"21","name":"白色"}];',
    ];

    public $output = [
    ];

    public function run($param, $uid)
    {
        OE('order')->changeNum($param, $uid);
        return;
    }
}
