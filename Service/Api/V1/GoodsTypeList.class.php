<?php
// +-------------------------------------------------------------------
// |
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Service\Api\V1;

class GoodsTypeList
{
    public $login = false; //是否需要登录才能访问该接口
    public $title = "首页/专区商品列表OK";
    public $group = '商品';
    public $desc = "";

    public $input = [
        'page_num' => 'int;label=分页;comment=默认为1',
        'page_size' => 'int;label=分页条数;comment=默认为5',
        'type' => 'label=专区;comment=专区：rush新品 refer推荐 hot爆品',
    ];

    public $output = [
        'list' => [
            [
                'id' => 'label=id;',
                'title' => 'label=商品名称;',
                'list_cover' => 'label=列表封面图;',
                'sell_price' => 'label=零售价;',
                'vip_price' => 'label=会员价;',
                'give_point' => 'label=赠送的积分;',
                'sell_num' => 'label=销量;',
            ]
        ],
        'banner' => [
            [
                'pic' => 'label=图片;',
                'wap_url' => 'label=链接地址;',
            ]
        ]
    ];

    public function run($param, $uid)
    {
        $page_num = $param['page_num'] ?: 1;
        $page_size = $param['page_size'] ?: 5;
        list($list, $page_count, $page_total) = OE('goods')->goodsTypeList($page_num, $page_size, $param);
        $banner = M('poster')->where(['category' => 'goods_type_list_banner'])->order('level desc')->select();
        foreach($banner as &$item){
            $item['pic'] = get_cover($item['pic'], 'url');
        }
        return [
            'page_num' => $page_num,
            'page_size' => $page_size,
            'page_count' => $page_count,
            'page_total' => $page_total,
            'list' => $list,
            'banner' => $banner,
        ];
    }
}
