<?php
// +-------------------------------------------------------------------
// |
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Service\Api\V1;

class GoodsType
{
    public $login = false; //是否需要登录才能访问该接口
    public $title = "商品分类列表";
    public $group = '商品';
    public $desc = "";

    public $input = [
        'page_num' => 'int;label=分页;comment=默认为1',
        'page_size' => 'int;label=分页条数;comment=默认为5',
        'sid'=>'int;required;label=分类id',
        'order'=>'label=排序，综合：id，销量：sell_num，价格：vip_price',
        'mode'=>'label=排序方式，升序：asc,降序：desc',
    ];

    public $output = [
        'goods' => [
            [
                [
                    // 'str'=>'label=商品id;',
                    'id' => 'label=商品id;',
                    'title' => 'label=标题;',
                    'intro' => 'label=描述;',
                    'sell_price' => 'label=零售价;',
                    'vip_price' => 'label=会员价;',
                    'deviation_point' => 'label=抵扣积分;',
                    'give_point' => 'label=赠送积分;',
                    'sell_num' => 'label=销量;',
                    'stock' => 'label=库存;','',
                    'list_cover' => 'label=列表封面;comment=单图'
            ]

        
    ]
]];

    public function run($param, $uid)
    {
        $page_num = $param['page_num'] ?: 1;
        $page_size = $param['page_size'] ?: 5;
        $goods = O('goods')->getGroupList2($page_num, $page_size, $uid, $param['order'],$param['mode'], $param['sid']);
        return [
            'goods' => $goods,
           
        ];
    }
}
