<?php
// +-------------------------------------------------------------------
// |
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Service\Api\V1;

class GwqList
{
    public $login = true; //是否需要登录才能访问该接口
    public $title = "购物券明细";
    public $group = '财务';
    public $desc = "";

    public $input = [
        'page_num' => 'int;label=分页;comment=默认为1',
        'page_size' => 'int;label=分页条数;comment=默认为5',
    ];

    public $output = [
        'page_num' => 'label=当前页码;',
        'page_size' => 'label=每页条数;',
        'page_count' => 'label=总条数;',
        'page_total' => 'label=总页数;',
        'total_gwq' => 'label=累计购物券;',
        'today_gwq' => 'label=今日购物券;',
        'use_gwq' => 'label=可用购物券;',
        'list' => [
            [
                'type' => 'label=资金类型;',
                'type_label' => 'label=资金类型;',
                'change_gwq' => 'label=变更金额;',
                'old_gwq' => 'label=变更前金额;',
                'new_gwq' => 'label=变更后金额;',
                'create_time' => 'label=时间;',
            ]
        ]
    ];

    public function run($param, $uid, $login_info)
    {
        $page_num = $param['page_num'] ?: 1;
        $page_size = $param['page_size'] ?: 50;
        list($list, $page_count, $page_total) = OE('user')->gwqList($uid, $page_num, $page_size);
        $today_gwq = M('gwq_log')->where([
            'uid' => $uid,
            'type' => 1,
            'create_time' => [['GT', date('Y-m-d 00:00:00', strtotime("-1days"))],['LT', date('Y-m-d 23:59:59', strtotime("-1days"))]]
        ])->sum('change_gwq');
        return [
            'page_num' => $page_num,
            'page_size' => $page_size,
            'page_count' => $page_count,
            'page_total' => $page_total,
            'total_gwq' => $login_info['total_gwq'],
            'use_gwq' => $login_info['use_gwq'],
            'list' => $list,
        ];
    }
}
