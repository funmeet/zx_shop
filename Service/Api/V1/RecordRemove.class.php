<?php
// +-------------------------------------------------------------------
// |
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Service\Api\V1;

use Think\Exception;

class RecordRemove
{
    public $login = true; //是否需要登录才能访问该接口
    public $title = "清空我的足迹";
    public $group = '我的足迹';
    public $desc = "";

    public $input = [
    ];

    public $output = [

    ];

    public function run($param, $uid)
    {
        $ret = M('goods_spoor')->where(['uid' => $uid])->delete();
        return [];
    }
}