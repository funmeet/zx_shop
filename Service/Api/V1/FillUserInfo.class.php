<?php
// +-------------------------------------------------------------------
// |
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Service\Api\V1;

class FillUserInfo
{
    public $login = true; //是否需要登录才能访问该接口
    public $title = "完善信息";
    public $group = '用户';
    public $desc = "";

    public $input = [
//        'nickname' => 'label=昵称;',
        'sex' => 'required;enum=0,1;label=性别;comment=0女1男',
//        'born' => 'date;label=出生日期;',
//        'avatar_url' => 'label=头像地址;',
        'real_name' => 'required;label=真实姓名;',
        'id_no' => 'required;idcard;label=身份证号;',
//        'mobile' => 'mobile;label=手机号;',
//        'area' => 'area;label=所在地区;',
//        'address' => 'label=详细地址;',
//        'email' => 'email;label=邮箱;',
//        'bank_type' => 'banktype;label=开户行;',
//        'bank_no' => 'label=银行卡号;',
    ];

    public $output = [
    ];

    public function run($param, $uid)
    {
        OE('user')->fillUserInfo($param, $uid);
        return [];
    }
}
