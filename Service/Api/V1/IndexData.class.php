<?php
// +-------------------------------------------------------------------
// |
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Service\Api\V1;

class IndexData
{
    public $login = false; //是否需要登录才能访问该接口
    public $title = "首页/数据获取OK";
    public $group = '商品';
    public $desc = "";

    public $input = [
    ];

    public $output = [
        'one_goods_type' => [
            [
                'id' => 'label=id;',
                'title' => 'label=商品分类名称;',
                'pic' => 'label=图片;',
            ]
        ],
        'banner' => [
            [
                'pic' => 'label=图片;',
                'wap_url' => 'label=链接地址;',
            ]
        ],
        'roll' => [
            [
                'id' => 'label=id;',
                'content' => 'label=公告内容;',
            ]
        ],
        'info' => [
            'hot_goods_list' => [
                [
                    'id' => 'label=id;',
                    'title' => 'label=商品名称;',
                    'list_cover' => 'label=列表封面图;',
                    'sell_price' => 'label=零售价;',
                    'vip_price' => 'label=会员价;',
                    'sell_num' => 'label=销售量;',
                    'give_point' => 'label=赠送的积分;',
                ]
            ],
            'refer_goods_list' => [
                [
                    'id' => 'label=id;',
                    'title' => 'label=商品名称;',
                    'list_cover' => 'label=列表封面图;',
                    'sell_price' => 'label=零售价;',
                    'vip_price' => 'label=会员价;',
                    'sell_num' => 'label=销售量;',
                    'give_point' => 'label=赠送的积分;',
                ]
            ],
            'rush_goods_list' => [
                [
                    'id' => 'label=id;',
                    'title' => 'label=商品名称;',
                    'list_cover' => 'label=列表封面图;',
                    'sell_price' => 'label=零售价;',
                    'sell_num' => 'label=销售量;',
                    'vip_price' => 'label=会员价;',
                    'give_point' => 'label=赠送的积分;',
                ]
            ],


        ],
        'goods_type_list' => [
            [
                'id' => 'label=id;',
                'title' => 'label=分类名称;',
                'pic' => 'label=分类图标;',
            ]
        ],
        'menu_list' => [
            [
                'id' => 'label=id;',
                'wap_url' => 'label=链接;',
                'title' => 'label=分类名称;',
                'pic' => 'label=分类图标;',
            ]
        ],
    ];

    public function run($param, $uid)
    {
        $data['banner'] = M('poster')->field('pic, link as wap_url')->where(['category' => 'mall_home_banner'])->order('level desc')->select();
        $data['roll'] =M('notice')->field('id, content')->limit(5)->select();
        $data['info']['hot_goods_list'] = M('goods')
        ->field('id, title, sell_price, vip_price, sell_num, list_cover')
            ->where(['is_hot' => 1,'is_vip' =>2])
            ->limit(5)
            ->select();

        $data['info']['rush_goods_list'] = (array)M('goods')
            ->field('id, title, sell_price, vip_price, sell_num, list_cover')
            ->where(['is_rush' => 1,'is_vip' =>2])
            ->limit(5)
            ->select();
        $data['info']['refer_goods_list'] = (array)M('goods')
            ->field('id, title, sell_price, vip_price, sell_num, list_cover')
            ->where(['is_refer' => 1,'is_vip' =>2])
            ->limit(5)
            ->select();
        foreach($data['info']['hot_goods_list'] as &$item){
            $item['list_cover'] = get_cover($item['list_cover'], 'url');
        }
        foreach($data['info']['rush_goods_list'] as &$item){
            $item['list_cover'] = get_cover($item['list_cover'], 'url');
        }
        foreach($data['info']['refer_goods_list'] as &$item){
            $item['list_cover'] = get_cover($item['list_cover'], 'url');
        }
        foreach($data['banner'] as &$item){
            $item['pic'] = get_cover($item['pic'], 'url');
        }

        $where = [
            'pid' => ['gt', 0],
        ];
        $data['goods_type_list'] = (array)M('goodstype')->where($where)->order('sort desc')->limit(4)->select();
        foreach($data['goods_type_list'] as &$item){
            $item['pic'] = get_cover($item['pic'], 'url');
        }

       /* $menu_list = [
            [
                'id' => 'id',
                'title' => '报单专区',
                'pic' => '/Uploads/Picture/2020-01-12/0e7d5ab215d37d0a56a29c76a7590548.png',
                'wap_url' => '/declaration_goods/',
            ],
            [
                'id' => 'id',
                'title' => '主推专区',
                'pic' => '/Uploads/Picture/2020-01-12/0e7d5ab215d37d0a56a29c76a7590548.png',
                'wap_url' => '/hot_new_goods/refer',
            ]
            , [
                'id' => 'id',
                'title' => '热销专区',
                'pic' => '/Uploads/Picture/2020-01-12/0e7d5ab215d37d0a56a29c76a7590548.png',
                'wap_url' => '/hot_new_goods/hot',
            ],
        ];*/

        $menu_list = M('indexmenu')->where(['status' => 1])->select();
        foreach($menu_list as &$item){
            $item['pic'] = get_cover($item['pic'], 'url');
        }


        if(count($data['goods_type_list'])>4){
            $leng = 4;
        }else{
            $leng = count($data['goods_type_list']);
        }
        foreach(array_slice($data['goods_type_list'], $leng) as $item){
            $menu_list[] = [
                'id' => $item['id'],
                'title' => $item['title'],
                'pic' => $item['pic'],
                'wap_url' => "/goods_list?id={$item['id']}&title={$item['title']}",
            ];
        }


        $data['menu_list'] = $menu_list;
        return $data;

    }
}
