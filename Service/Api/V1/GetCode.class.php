<?php
// +-------------------------------------------------------------------
// |
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Service\Api\V1;

class GetCode
{
    public $login = true; //是否需要登录才能访问该接口
    public $title = "获取短信验证码";
    public $group = '个人中心';
    public $desc = "";

    public $input = [
        'mobile' => 'required;mobile;label=收件人手机号',
    ];

    public $output = [
    ];

    public function run($param, $uid)
    {
        $key = "sms_{$uid}";
        $cache_submit = S($key);
        if($cache_submit){
            throw new \Exception('请勿重复提交');
        }else{
            S($key, 1, ['expire' => 60]);
        }

        $code = rand(100000,999999);
        $post_data = array();
        $post_data['userid'] = 6651;
        $post_data['account'] = 'user71123';
        $post_data['password'] = '12345678a';
        $post_data['content'] = "【拼自己】您的验证码为：{$code}"; //短信内容
        $post_data['mobile'] = $param['mobile'];
        $post_data['sendtime'] = date('Y-m-d H:i:s'); //时定时发送，输入格式YYYY-MM-DD HH:mm:ss的日期值
        $url='http://sms10692.com/sms.aspx?action=send';
        $o='';
        foreach ($post_data as $k=>$v)
        {
            $o.="$k=".urlencode($v).'&';
        }
        $post_data=substr($o,0,-1);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_URL,$url);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); //如果需要将结果直接返回到变量里，那加上这句。
        $result = curl_exec($ch);
        //短信码插入表中
        $add = [
            'uid' => $uid,
            'code' => $code,
            'mobile' => $param['mobile'],
            'content' => $post_data['content'],
            'expire_time' => time()+3*60,
        ];
        M('sms_content')->add($add);

        return [];
    }
}