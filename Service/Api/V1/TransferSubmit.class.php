<?php
// +-------------------------------------------------------------------
// |
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Service\Api\V1;

class TransferSubmit
{
    public $login = true; //是否需要登录才能访问该接口
    public $title = "余额转账";
    public $group = '财务';
    public $desc = "";

    public $input = [
        'nickname' => 'required;label=对方姓名;',
        'mobile' => 'required;mobile;label=对方手机;',
        'money' => 'required;number;label=转账金额;',
        'pay_password' => 'required;label=支付密码;',
    ];

    public $output = [
    ];

    public function run($param, $uid, $login_info)
    {
        OE('user')->transferSubmit($param,$login_info);
        return [];
    }
}
