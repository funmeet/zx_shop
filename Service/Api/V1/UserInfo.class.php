<?php
// +-------------------------------------------------------------------
// |
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Service\Api\V1;

class UserInfo
{
    public $login = true; //是否需要登录才能访问该接口
    public $title = "用户信息";
    public $group = '用户';
    public $desc = "";

    public $input = [
    ];

    public $output = [
        'uid' => 'label=用户id;',
        'total_contribution' => 'label=贡献值;',
        'avatar_url' => 'label=头像地址;',
        'username' => 'label=用户名;',
        'real_name' => 'label=真实姓名;',
        'nickname' => 'label=昵称;',
        'sex' => 'label=性别;',
        'sex_label' => 'label=性别;',
        'born' => 'label=出生日期;',
        'id_no' => 'label=身份证号;',
        'mobile' => 'label=手机号;',
        'area' => 'label=所在地区;',
        'area_label' => 'label=所在地区;',
        'address' => 'label=详细地址;',
        'email' => 'label=邮箱;',
        'bank_type' => 'label=开户行;',
        'bank_type_label' => 'label=开户行;',
        'bank_no' => 'label=银行卡号;',
        'balance' => 'label=余额;',
        'brokerage_price' => 'label=余额;',
        'frozen_money' => 'label=冻结金额;',
        'total_point' => 'label=总积分;',
        'pay_password' => 'label=是否有支付密码;comment=[0没有,1有]',
        'total_unread' => 'label=未读通知数量;',
        'invite_code' => 'label=邀请码;',
        'red_packets' => 'label=可用红包;',
        'invite_qrcode' => 'label=邀请二维码;',
        'vip_level' => 'label=会员级别;',
        'level_text' => 'label=会员级别;',
        'agent_level' => 'label=代理级别;',
        'agent_level_text' => 'label=代理级别;',
        'qrcode_url' =>'label=二维码背景图url;',
        'refer_nickname' =>'label=推荐人昵称;',
        'refer_mobile' =>'label=推荐人手机号;',
        'is_speed_open' =>'label=同步状态;',
        'yuji_money' =>'label=预计收益;',
        'total_money' =>'label=总佣金;',
        'use_gwq' =>'label=可用购物券;',
        'couponCount' =>'label=可用购物券;',
        'user_msg' =>'label=推广提示;',
        'is_qy' =>'label=是否迁移;comment=1是 2否',
        'qrcode' => [
                [
                    'id' =>'label=图片id;',
                    'title' =>'label=标题;',
                   // 'pic' =>'label=图片id;',
                    //'qrcode_url' =>'label=图片连接地址;',
                    //'status' =>'label=是否显示;',
                ]
           ]
    ];

    public function run($param, $uid)
    {
        $result = OE('user')->getDetail($uid);
        if($result['referer_id']){
            $refer_data = OE('user')->getDetail($result['referer_id']);
            $result['refer_nickname'] = $refer_data['nickname'];
            $result['refer_mobile'] = $refer_data['mobile'];
        }

        $ret = M('team_log')->field('sum(num) as num_total')
            ->where(['uid' => $uid])->find();
        if($ret['num_total']>300){
            $result['yuji_money'] = $ret['num_total']?number_format(($ret['num_total']-300)*6, 2):0.00;
        }else{
            $result['yuji_money'] = 0;
        }

        $ret = M('commission_log')->field('sum(money) as money_total')
            ->where(['uid' => $uid])->find();
        $result['total_money'] = $ret['money_total']?$ret['money_total']:0.00;


        $result['pay_password'] = $result['pay_password'] ? 1 : 0;
        $result['qrcode_url'] = OE('user')->getUserQrcodeUrl($uid);
        $result['qrcode'] = OE('user')->getQrcode();
        $result['user_msg'] = "亲，拼单成为商城会员，才能拥有自己的专属推广二维码哟";
        $result['couponCount'] = $result['use_gwq'];
        $result['brokerage_price'] = $result['balance'];
        return $result;
    }
}
