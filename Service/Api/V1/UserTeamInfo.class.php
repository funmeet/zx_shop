<?php
// +-------------------------------------------------------------------
// |
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Service\Api\V1;

class UserTeamInfo
{
    public $login = true; //是否需要登录才能访问该接口
    public $title = "业绩统计";
    public $group = '用户';
    public $desc = "";

    public $input = [

    ];

    public $output = [
        'total_contribution' => 'label=累计积分;',
        'total_month_contribution' => 'label=当月积分;',
        'total_today_contribution' => 'label=当日积分;',
    ];

    public function run($param, $uid, $login_info)
    {
        list($total_contribution, $total_month_contribution, $total_today_contribution) = OE('user')->getUserTeamInfo($login_info);
        return compact('total_contribution', 'total_month_contribution', 'total_today_contribution');
    }
}
