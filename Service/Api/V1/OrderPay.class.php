<?php
// +-------------------------------------------------------------------
// |
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Service\Api\V1;

use Think\Log;

class OrderPay
{
    public $login = true; //是否需要登录才能访问该接口
    public $title = "支付";
    public $group = '订单';
    public $desc = "";

    public $input = [
        'order_no' => 'required;label=订单号',
        'pay_type' => 'required;int;enum=1,3,4;label=支付方式;comment=1余额支付 3微信h5支付 4微信公众号支付',
        'pay_password' => 'label=支付密码;comment=余额支付为必填',
    ];

    public $output = [
        'wxh5' => [
            'pay_url' => 'label=支付同步跳转地址;comment=用于微信h5支付'
        ],
        'wxjs' => [
            'appId' => 'label=appId;comment=用于微信公众号支付',
            'timeStamp' => 'label=timeStamp;comment=用于微信公众号支付',
            'nonceStr' => 'label=nonceStr;comment=用于微信公众号支付',
            'package' => 'label=package;comment=用于微信公众号支付',
            'signType' => 'label=signType;comment=用于微信公众号支付',
            'paySign' => 'label=paySign;comment=用于微信公众号支付',
        ]
    ];

    public function run($param, $uid)
    {
        $result = OE('order')->pay($param, $uid);
//        Log::w_log("param:".json_encode($param)."----uid:{$uid}----result: ".json_encode($result));
        switch ($param['pay_type']) {
            case 1:
                return [];
            case 3:
                return [
                    'wxh5' => [
                        'pay_url' => $result
                    ],
                ];
            case 4:
                return [
                    'wxjs' => $result
                ];
        }
    }
}
