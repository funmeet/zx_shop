<?php
// +-------------------------------------------------------------------
// |
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Service\Api\V1;

class Login
{
    public $login = false; //是否需要登录才能访问该接口
    public $title = "登录ok";
    public $group = '用户';
    public $desc = "";

    public $input = [
        'username' => 'required;label=用户名;comment=支持账号、邮箱、手机号登录',
        'password' => 'required;length=6,;label=密码;',
    ];

    public $output = [
        'api_token' => 'label=接口token;comment=用户登录状态标识,通过header参数API-TOKEN=api_token提交;',
        'expire_time' => 'label=接口token过期时间;',
    ];

    public function run($param, $uid)
    {
//        var_dump($param)
       /* $option_data_str = file_get_contents("php://input");
        $option_data = json_decode($option_data_str,true);
        return $option_data;*/
        list($api_token,$expire_time) = OE('user')->login($param['username'],$param['password']);
        return [
            'api_token' => $api_token,
            'expire_time' => $expire_time,
        ];
    }
}
