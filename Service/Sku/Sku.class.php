<?php
// +-------------------------------------------------------------------
// |
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Service\Sku;

class Sku extends \Service\Common\BaseModel
{
    protected $name = 'sku';



    
    public function formatCallback($data)
    {

		$detail = $this->getData($data['sku.id']);
		return [
            'sku.pid_name' => $detail['pid_name'],//父类名称
		];
    }

	private function getData($id){
		$data = $this->where(['id' => $id])->find();
		if($data['pid']){
			$pid_data = $this->where(['id' => $data['pid']])->find();
			$data['pid_name'] = $pid_data['name'];
		}else{
			$data['pid_name'] = '';
		}
		return $data;
	}

	public function oneGoodsTypeList()
	{
		$where = [
			'pid' => 0
		];
		$list = (array)$this->where($where)->order('sort desc')->select();
		$count = $this->where($where)->count();
		$banner = (array)M('poster')->field('id,link,pic')->where(['category' => 'mall_home_banner'])->order('level desc')->select();
		foreach($banner as &$item){
			$item['pic_url'] = get_cover($item['pic'], 'url');
		}
		return [
			$list,
			$banner
		];
	}
    

    
}