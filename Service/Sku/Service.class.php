<?php
// +-------------------------------------------------------------------
// |
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Service\Sku;

use Service\Common\BaseService;

class Service extends BaseService
{
    public function oneGoodsTypeList()
    {
        return Goodstype::getInstance()->oneGoodsTypeList();
    }
}