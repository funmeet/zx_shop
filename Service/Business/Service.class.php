<?php
// +-------------------------------------------------------------------
// |
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Service\Business;

use Service\Common\BaseService;

class Service extends BaseService
{
	public function checkData($data,$status = 'add',$id = '')
	{
		return Collection::getInstance()->checkData($data,$status,$id);
	}
	
	public function checkNo($no)
	{
		return Collection::getInstance()->checkNo($no);
	}
	
	public function isBusiness($uid,$value)
	{
		return Collection::getInstance()->isBusiness($uid,$value);
	}
	
	public function application($data, $uid)
	{
		return Application::getInstance()->application($data, $uid);
	}
	
	public function applicationData($uid)
	{
		return Application::getInstance()->applicationData($uid);
	}
    
}