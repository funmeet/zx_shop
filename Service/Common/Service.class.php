<?php
// +-------------------------------------------------------------------
// |
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Service\Common;

class Service extends BaseService
{
	public static function getBankText($type){
		$list = self::getBankList();
		return $list[$type];
	}

    public function getVipLevel(){
        $vip_array =get_list_config('VIP_LEVEL', false);
        return json_encode($vip_array);
    }
	
    //银行列表
    public function getBankList()
    {
        return [
            '008' => '中国银行',
            '002' => '中国工商银行',
            '005' => '中国建设银行',
            '004' => '中国农业银行',
            '031' => '宁波银行',
            '030' => '邮政储蓄银行',
            '029' => '重庆商业银行',
            '028' => '重庆三峡银行',
            '027' => '昆仑银行',
            '023' => '哈尔滨银行',
            '021' => '青岛银行',
            '020' => '济宁银行',
            '019' => '光大银行',
            '017' => '浙商银行',
            '016' => '四川农信',
            '015' => '上海银行',
            '014' => '平安银行',
            '013' => '华夏银行',
            '012' => '中信银行',
            '011' => '民生银行',
            '010' => '兴业银行',
            '007' => '广发银行',
            '006' => '交通银行',
            '003' => '招商银行',
            '001' => '浦发银行',
        ];
    }

    //验证地区
    public function checkArea($area)
    {
        list($province, $city, $area,) = explode(',', $area);
        $arr = [
            ['省份', $province, 0],
            ['城市', $city, $province],
            ['地区', $area, $city],
        ];
        foreach ($arr as $key => $row) {
            list($label, $area_id, $area_parent_id) = $row;

            if (!$area_id) {
                throw new \Exception("无效的{$label}");
            }

            $result = M('area')->where([
                'area_parent_id' => $area_parent_id,
                'area_id' => $area_id,
                'area_deep' => $key + 1
            ])->find();
            if (!$result) {
                throw new \Exception("该{$label}不存在");
            }
        }
        return true;
    }

    //获取地区名称
    public static function getAreaLabel($ids_str)
    {
        $ids = array_filter(explode(',', trim($ids_str)));
        $arr = [];
        foreach ($ids as $id) {
            $arr[] = M('area')->where(['area_id' => $id])->getField('area_name');
        }
        return join(',', $arr);
    }

    //获取地区词典
    public function getAreaDict()
    {
        $are = 'area_true';
        if ($list = S($are)) {
            return $list;
        }

        $area = M('area');
        $field = 'area_id,area_name,area_parent_id';
        $where['area_parent_id'] = 0;
        $arr = $area->field($field)->where($where)->select();
        $list = [];

        foreach ($arr as $k => $v) {
            $where1['area_parent_id'] = $v['area_id'];
            $list1 = [];
            $arr1 = $area->field($field)->where($where1)->select();

            foreach ($arr1 as $key => $val) {
                $where2['area_parent_id'] = $val['area_id'];
                $list2 = [];
                $arr2 = (array)$area->field($field)->where($where2)->select();

                foreach ($arr2 as $key1 => $val1) {

                    $list2[$key1]['id'] = $val1['area_id'];
                    $list2[$key1]['name'] = $val1['area_name'];
                }
                $list1[$key]['id'] = $val['area_id'];
                $list1[$key]['name'] = $val['area_name'];
                $list1[$key]['child'] = $list2;
            }
            $list[$k]['id'] = $v['area_id'];
            $list[$k]['name'] = $v['area_name'];
            $list[$k]['child'] = $list1;

        }
        S($are, $list);

        return $list;

    }

    public function getReferMobile($uid){
        $data = M('ucenter_member')->where(['id'=>$uid])->find();
        return $data['mobile'];
    }

    public function getGoodsTypeOneList(){
        $list = M('goods_type')->where(['pid'=>0])->select();
        $data = [];
        foreach($list as $k => $v){
            $data[$v['id']] = $v['title'];
        }
        return $data;
    }
}