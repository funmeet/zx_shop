<?php
// +-------------------------------------------------------------------
// |
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Service\Common;

class BaseModel extends \Think\Model
{
    public static $instance = [];

    public static function getInstance()
    {
        $class = \get_called_class();
        if (!self::$instance[$class]) {
            self::$instance[$class] = new $class();
        }
        return self::$instance[$class];
    }

    public function _format($data, $callback)
    {
        if (!$data) {
            return $data;
        }

        $is_one = count($data) == count($data, 1);
        $list = $is_one ? [$data] : $data;

        foreach ($list as &$row) {
            call_user_func_array($callback, [$row]);
        }
        return $is_one ? $list[0] : $list;
    }
}