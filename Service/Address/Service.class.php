<?php
// +-------------------------------------------------------------------
// |
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Service\Address;

use Service\Common\BaseService;

class Service extends BaseService
{
    public function submit($data, $uid)
    {
        return Address::getInstance()->submit($data, $uid);
    }

    public function edit($data, $uid)
    {
        return Address::getInstance()->edit($data, $uid);
    }


    public function setAddress($data, $uid)
    {
        return Address::getInstance()->setAddress($data, $uid);
    }

    public function remove($address_id, $uid)
    {
        return Address::getInstance()->remove($address_id, $uid);
    }

    public function getDetail($address_id, $uid)
    {
        return Address::getInstance()->getDetail($address_id, $uid);
    }

    public function lists($uid, $param)
    {
        return Address::getInstance()->lists($uid, $param);
    }
}