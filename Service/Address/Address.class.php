<?php
// +-------------------------------------------------------------------
// |
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Service\Address;

use Think\Exception;
use Think\Model;

class Address extends \Service\Common\BaseModel
{
    protected $name = 'address';

    //新增
    public function submit($data, $uid)
    {
        $max = $this->where(['uid' => $uid])->count();
        if ($max > 10) {
            throw new \Exception("最多只能填10个地址");
        }

        $data['uid'] = $uid;
        $data['create_time'] = date('Y-m-d H:i:s');

        if ($data['is_default']) {
            $this->where([
                'uid' => $uid
            ])->save([
                'is_default' => 0
            ]);
        }

        $this->add($data);
        return true;
    }

    //新增
    public function edit($data, $uid)
    {
        $address_id = $data['address_id'];
        unset($data['address_id']);
        $this->getDetail($address_id, $uid);

        $data['id'] = $address_id;
        $data['uid'] = $uid;
        $data['update_time'] = date('Y-m-d H:i:s');

        if ($data['is_default']) {
            $this->where([
                'uid' => $uid
            ])->save([
                'is_default' => 0
            ]);
        }
        $this->save($data);
        return true;
    }

    public function setAddress($data, $uid)
    {
        $address_id = $data['id'];
        $this->getDetail($address_id, $uid);


        $this->where([
            'uid' => $uid
        ])->save([
            'is_default' => 0
        ]);
        $this->where(['id' => $address_id])->save(['is_default' => 1]);
        return true;
    }

    //删除地址
    public function remove($address_id, $uid = 0)
    {
        $this->getDetail($address_id, $uid);
        $this->delete($address_id);
        return true;
    }

    //地址详情
    public function getDetail($address_id, $uid = 0)
    {
        $where = [
            'id' => $address_id
        ];
        if ($uid) {
            $where['uid'] = $uid;
        }
        $address = $this->where($where)->find();
        if (!$address) {
            throw new \Exception("无效的地址id");
        }
        return $this->format($address);
    }

    //地址详情
    public function lists($uid, $param)
    {
        $field = O('user')->getLoginType($param['keyword'], 2);
        if($field == 'mobile'){
            $sql = "SELECT * FROM `k_address` WHERE uid={$uid} AND mobile='{$param['keyword']}'
ORDER BY is_default desc,id desc;";
        }else{
            $sql = "SELECT * FROM `k_address` WHERE uid={$uid} AND
(name LIKE '%{$param['keyword']}%' OR address LIKE '%{$param['keyword']}%')
ORDER BY is_default desc,id desc;";
        }
        $model = new Model();
        $list = $model->query($sql);
        foreach($list as &$item){
            if($this->isRemoteArea($item['area'])){
                $item['is_remote'] = 1;
            }else{
                $item['is_remote'] = 0;
            }
        }
        return $this->format($list);
    }

    public function isRemoteArea($area){
        $remote_area = array(5, 26, 31);
        $area_arr = explode(',', $area);
        if(in_array($area_arr[0], $remote_area)){
            return true;
        }else{
            return false;
        }
    }

    public function zwDeal($string){
        $string_arr = explode(',', $string);
        $model = new Model();
        $sql = "select * from k_area WHERE area_deep=1 AND  area_name LIKE  '%{$string_arr[0]}%'";
        $province = $model->query($sql);
        if(!$province) throw new Exception('省份不存在');
        $sql = "select * from k_area WHERE area_deep=2 AND  area_name LIKE  '%{$string_arr[1]}%'";
        $city = $model->query($sql);
        if(!$city) throw new Exception('城市不存在');
        $sql = "select * from k_area WHERE area_deep=3 AND  area_name LIKE  '%{$string_arr[2]}%'";
        $qu = $model->query($sql);
        if(!$qu) throw new Exception('区不存在');
        return "{$province[0]['id']},{$city[0]['id']},{$qu[0]['id']}";
    }

    //格式化
    public function format($list)
    {
        if (!$list) {
            return $list;
        }
        
        $is_one = count($list) == count($list, 1);
        $list = $is_one ? [$list] : $list;

        foreach ($list as &$row) {
            $row['area_label'] = $row['area'];
            $row['mobile_secure'] = preg_replace('/^(\d{3})(\d{4})(\d{4})$/', '$1****$3', $row['mobile']);
        }
        return $is_one ? $list[0] : $list;
    }

}