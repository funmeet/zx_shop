<?php
// +-------------------------------------------------------------------
// |
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Service\Goodstype;

class Goodstype extends \Service\Common\BaseModel
{
    protected $name = 'goodstype';



    
    public function formatCallback($data)
    {

		$detail = $this->getData($data['goodstype.id']);
		return [
            'goodstype.pid_title' => $detail['pid_title'],//父类名称
			'goodstype.pic_html' => '<img style="width: 40px;" src="' . $detail['pic_url'] . '" >',//推荐人昵称
		];
    }

	private function getData($id){
		$data = $this->where(['id' => $id])->find();
		if($data['pid']){
			$pid_data = $this->where(['id' => $data['pid']])->find();
			$data['pid_title'] = $pid_data['title'];
		}else{
			$data['pid_title'] = '';
		}

		$picture = M('picture')->where(['id' => $data['pic']])->find();
		$data['pic_url'] =  $picture['url'];
		return $data;
	}

	public function oneGoodsTypeList()
	{
		$where = [
			'pid' => 0
		];
		$list = (array)$this->where($where)->order('sort desc')->select();
		$count = $this->where($where)->count();
		$banner = (array)M('poster')->field('id,link,pic')->where(['category' => 'mall_home_banner'])->order('level desc')->select();
		foreach($banner as &$item){
			$item['pic_url'] = get_cover($item['pic'], 'url');
		}
		return [
			$list,
			$banner
		];
	}
    

    
}