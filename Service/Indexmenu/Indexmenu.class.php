<?php
// +-------------------------------------------------------------------
// |
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Service\Indexmenu;

class Indexmenu extends \Service\Common\BaseModel
{
    protected $name = 'indexmenu';



    
    public function formatCallback($data)
    {

		$url_img = getcover($data['indexmenu.pic']);
		$img = "<img src='{$url_img}' style='width: 60px;'>";
		return [
			'indexmenu.pic_img' => $img
		];
    }

    

    
}