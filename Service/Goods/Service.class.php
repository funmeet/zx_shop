<?php
// +-------------------------------------------------------------------
// |
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Service\Goods;

use Service\Common\BaseService;

class Service extends BaseService
{
	
	public function getTypeList()
	{
		return Goods::getInstance()->getTypeList();
	}
	
    public function getDetail($goods_id, $uid = 0)
    {
        return Goods::getInstance()->getDetail($goods_id, $uid);
    }
    
    public function getIndexList($uid = 0, $is_vip = 0)
    {
    	return Goods::getInstance()->getIndexList($uid, $is_vip);
    }

    public function getGroupList($page_size, $uid = 0, $is_vip = 0)
    {
        return Goods::getInstance()->getGroupList($page_size, $uid, $is_vip);
    }
    
    public function getGroupList2($page_num, $page_size, $uid = 0,$order = 'id',$mode = 'desc', $type)
    {
    	return Goods::getInstance()->getGroupList2($page_num, $page_size, $uid ,$order,$mode, $type);
    }

    public function getGroupList3($page_size, $uid = 0,$poster_id,$order = 'id',$mode = 'desc')
    {
        return Goods::getInstance()->getGroupList3($page_size, $uid,$poster_id,$order,$mode);
    }

    public static function getSelectCategory($type = 0)
    {
        if (ACTION_NAME == 'index') {
            return M('goodstype')->where(['id' => $type])->getField('title');
        } else {
            return M('goodstype')->where(['pid' => ['NEQ', 0]])->order('sort asc,id asc')->getField('id,title', true);
        }
    }

    public static function getParentCategory($type = 0)
    {
        if (ACTION_NAME == 'index') {
            return M('goodstype')->where(['id' => $type])->getField('title');
        } else {
            return M('goodstype')->where(['pid' => ['EQ', 0]])->order('sort asc,id asc')->getField('id,title', true);
        }
    }

    public static function getSkuSelectList($type = 0)
    {
        if (ACTION_NAME == 'index') {
            return M('sku')->where(['id' => $type])->getField('name');
        } else {
            return M('sku')->where(['pid' => ['EQ', 0]])->order('id asc')->getField('id,name', true);
        }
    }

    public static function getSupplySelectList($type = 0)
    {
        if (ACTION_NAME == 'index') {
            return M('supply')->where(['id' => $type])->getField('supply_name');
        } else {
            return M('supply')->order('id asc')->getField('id,supply_name', true);
        }
    }

    public function search($keyword)
    {
        return Goods::getInstance()->search($keyword);
    }

    public function rushList($page_num = 1, $page_size = 5)
    {
        return Goods::getInstance()->rushList($page_num, $page_size);
    }

    public function goodsHotList($page_num = 1, $page_size = 5)
    {
        return Goods::getInstance()->goodsHotList($page_num, $page_size);
    }

    public function goodsReferList($page_num = 1, $page_size = 5)
    {
        return Goods::getInstance()->goodsReferList($page_num, $page_size);
    }

    public function goodsTypeList($page_num = 1, $page_size = 5, $param)
    {
        return Goods::getInstance()->goodsTypeList($page_num, $page_size, $param);
    }

    public function goodsTwoTypeList($page_num = 1, $page_size = 5, $param)
    {
        return Goods::getInstance()->goodsTwoTypeList($page_num, $page_size, $param);
    }

    public function BdGoodsList($page_num = 1, $page_size = 5, $param)
    {
        return Goods::getInstance()->BdGoodsList($page_num, $page_size, $param);
    }

    public function AgentGoodsList($page_num = 1, $page_size = 5, $param)
    {
        return Goods::getInstance()->AgentGoodsList($page_num, $page_size, $param);
    }

    public function setGoodsSku($input, $goodsid,$type)
    {

        return Goods::getInstance()->setGoodsSku($input, $goodsid,$type);
    }

    public function goodsDel($ids)
    {
        return Goods::getInstance()->goodsDel($ids);
    }
}