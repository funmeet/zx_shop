<?php
// +-------------------------------------------------------------------
// |
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Service\User;

class Remit extends \Service\Common\BaseModel
{
    protected $name = 'remit';

    const STATUS_SUBMIT = 0;//已提交
    const STATUS_SUCCESS = 1;//审核成功
    const STATUS_FAIL = 2;//审核失败

    //汇款审核
    public function check($data)
    {
        $remit = $this->find($data['id']);
        if (!$remit) {
            throw new \Exception("无效的汇款记录");
        }

        //状态没发生变化
        if ($remit['status'] == $data['status']) {
            return true;
        }

        if ($remit['status'] == self::STATUS_SUCCESS) {
            throw new \Exception("已通过的记录不可再审核");
        }

        try {
            $this->startTrans();

            //更新汇款状态
            $update = $this->save([
                'id' => $data['id'],
                'update_time' => date('Y-m-d H:i:s'),
                'status' => $data['status'],
                'status_des' => $data['status_des'],
                'receive_money' => $data['receive_money'],
            ]);
            if ($update === false) {
                throw new \Exception("汇款状态更新失败");
            }

            if ($data['status'] == self::STATUS_SUCCESS) {
                //余额更新
                if ($data['receive_money'] <= 0) {
                    throw new \Exception("请输入入款金额");
                }
                $remit['receive_money']=$data['receive_money'];
                OE('user')->remit($remit['to_uid'], $data['receive_money'], $remit);
            }

            $this->commit();
        } catch (\Exception $e) {
            $this->rollback();
            throw new \Exception($e->getMessage());
        }

        return true;
    }

    //汇款提交
    public function submit($data, $uid)
    {
        $user = M('ucenter_member')->where(['mobile' => $data['recharge_mobile']])->find();
        if (!$user) {
            throw new \Exception("无效的手机号");
        }
        $to_uid = $user['id'];

        $this->add(array_merge($data, [
            'uid' => $uid,
            'to_uid' => $to_uid,
            'create_time' => date('Y-m-d H:i:s')
        ]));

        return true;
    }

    //列表
    public function lists($uid, $status, $page_num, $page_size)
    {
        $where = [
            'uid' => $uid,
            'status' => $status
        ];
        $list = (array)$this->where($where)->order('id desc')->page($page_num, $page_size)->select();
        foreach ($list as $key => &$row) {
            $real_name = M('user')->where(['uid' => $row['to_uid']])->getField('real_name');
            $row['recharge_mobile'] = $row['recharge_mobile'] . ($real_name ? "($real_name)" : '');
        }
        $count = $this->where($where)->count();
        return [
            $list,
            $page_count = $count,
            $page_total = ceil($count / $page_size)
        ];
    }
}