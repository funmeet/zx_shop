<?php
// +-------------------------------------------------------------------
// |
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Service\User;

use Service\Common\BaseService;

class Service extends BaseService
{
	public function chenckmMobile($phone,$uid)
	{
		return User::getInstance()->chenckmMobile($phone,$uid);
	}
	
	public function getQrcode()
	{
		return Qrcode::getInstance()->getQrcode();
	}
	
	public function getUserQrcode($uid,$id)
	{
		return Qrcode::getInstance()->getUserQrcode($uid,$id);
	}
	
	public function getUserQrcodeUrl($uid)
	{
		return Qrcode::getInstance()->getUserQrcodeUrl($uid);
	}
	
    public function useRedInOrder($uid, $order_no, $money)
    {
        return RedPacketsLog::getInstance()->useRedInOrder($uid, $order_no, $money);
    }

    public function backRedByCancel($uid, $order_no, $money)
    {
        return RedPacketsLog::getInstance()->backRedByCancel($uid, $order_no, $money);
    }

    public function payCommissionCallback($order)
    {
        return CommissionLog::getInstance()->payCommissionCallback($order);
    }

    public function newOrderCommission($order,$uid)
    {
        return CommissionLog::getInstance()->newOrderCommission($order,$uid);
    }

    public function oneceFanQing($order,$uid)
    {
        return CommissionLog::getInstance()->oneceFanQing($order,$uid);
    }

    public function upLevelCommission($order,$level,$uid)
    {
        return CommissionLog::getInstance()->upLevelCommission($order,$level,$uid);
    }



    public function genMonthBounds()
    {
        return CommissionLog::getInstance()->genMonthBounds();
    }

    public function genYearBounds()
    {
        return CommissionLog::getInstance()->genYearBounds();
    }

    public function getUserTeamInfo($login_info)
    {
        return TeamLog::getInstance()->getUserTeamInfo($login_info);
    }

    public function getUserDirectlyUnder($login_uid, $uid)
    {
        return User::getInstance()->getUserDirectlyUnder($login_uid, $uid);
    }

    public function logout($uid)
    {
        return User::getInstance()->logout($uid);
    }

    public function getTokenByOauth($uid)
    {
        return User::getInstance()->getTokenByOauth($uid);
    }

    public function receiveInvitePay($order)
    {
        return Notice::getInstance()->receiveInvitePay($order);
    }

    public function addDrawMoney($draw)
    {
        return Notice::getInstance()->addDrawMoney($draw);
    }

    public function addRemit($remit)
    {
        return Notice::getInstance()->addRemit($remit);
    }

    public function addReceiveTransfer($transfer)
    {
        return Notice::getInstance()->addReceiveTransfer($transfer);
    }

    public function invitePay($order)
    {
        return Notice::getInstance()->invitePay($order);
    }

    public function addOrderExpress($order)
    {
        return Notice::getInstance()->addOrderExpress($order);
    }

    public function addCustomerService($question)
    {
        return Notice::getInstance()->addCustomerService($question);
    }

    public function noticeList($uid, $type)
    {
        return Notice::getInstance()->noticeList($uid, $type);
    }

    public function noticeCenter($uid)
    {
        return Notice::getInstance()->noticeCenter($uid);
    }

    public function questionEvaluate($data, $uid)
    {
        return Question::getInstance()->questionEvaluate($data, $uid);
    }

    public function questionAnswer($question_id, $uid, $content, $is_admin = 0)
    {
        return Question::getInstance()->answer($question_id, $uid, $content, $is_admin);
    }

    public function questionDetail($question_id, $uid, $login_info)
    {
        return Question::getInstance()->detail($question_id, $uid, $login_info);
    }

    public function questionList($uid, $page_num, $page_size)
    {
        return Question::getInstance()->lists($uid, $page_num, $page_size);
    }

    public function submitQuestion($data, $uid)
    {
        return Question::getInstance()->submit($data, $uid);
    }

    public function submitRemit($data, $uid)
    {
        return Remit::getInstance()->submit($data, $uid);
    }

    public function RemitList($uid, $order_status, $page_num, $page_size)
    {
        return Remit::getInstance()->lists($uid, $order_status, $page_num, $page_size);
    }

    public function checkRemit($data)
    {
        return Remit::getInstance()->check($data);
    }

    public function addTeamContribution($uid, $order)
    {
        return TeamLog::getInstance()->addTeamContribution($uid, $order);
    }

    public function upgradeVipLevel($uid,$orderid)
    {
        return User::getInstance()->upgradeVipLevel($uid,$orderid);
    }

    public function upgradeAllVipLevel()
    {
        return User::getInstance()->upgradeAllVipLevel();
    }

    public function upgradeManageLevel($uid)
    {
        return User::getInstance()->upgradeManageLevel($uid);
    }

    public function upgradeLeaderLevel($uid)
    {
        return User::getInstance()->upgradeLeaderLevel($uid);
    }

    public function getUnderRelation($uid, $current_level = 0)
    {
        return User::getInstance()->getUnderRelation($uid, $current_level);
    }
    
    public function editMobile($uid, $mobile)
    {
    	return User::getInstance()->editMobile($uid, $mobile);
    }

    public function getTopRefererRelationTree($uid)
    {
        return User::getInstance()->getTopRefererRelationTree($uid);
    }

    public function transferList($uid, $page_num = 1, $page_size = 5)
    {
        return BalanceTransfer::getInstance()->transferList($uid, $page_num, $page_size);
    }

    public function transferSubmit($data, $login_info)
    {
        return BalanceTransfer::getInstance()->transferSubmit($data, $login_info);
    }

    public function zpacketsList($uid, $page_num = 1, $page_size = 5)
    {
        return Zpackets::getInstance()->zpacketsList($uid, $page_num, $page_size);
    }

    public function zpacketsSubmit($data, $login_info)
    {
        return Zpackets::getInstance()->zpacketsSubmit($data, $login_info);
    }

    public function checkDrawMoney($draw_cash_id, $status, $remark)
    {
        return DrawCash::getInstance()->checkDrawMoney($draw_cash_id, $status, $remark);
    }

    public function moneyList($uid, $page_num = 1, $page_size = 5)
    {
        return MoneyLog::getInstance()->moneyList($uid, $page_num, $page_size);
    }

    public function transfer($uid, $to_uid, $money, $transfer_id)
    {
        return MoneyLog::getInstance()->transfer($uid, $to_uid, $money, $transfer_id);
    }

    public function drawMoneySuccess($uid, $money, $draw_cash_id)
    {
        return MoneyLog::getInstance()->drawMoneySuccess($uid, $money, $draw_cash_id);
    }

    public function unfreezeDrawMoney($uid, $money, $draw_cash_id)
    {
        return MoneyLog::getInstance()->unfreezeDrawMoney($uid, $money, $draw_cash_id);
    }

    public function drawMoney($uid, $money, $draw_cash_id)
    {
        return MoneyLog::getInstance()->drawMoney($uid, $money, $draw_cash_id);
    }

    public function drawList($uid, $status, $page_num = 1, $page_size = 5)
    {
        return DrawCash::getInstance()->drawList($uid, $status, $page_num, $page_size);
    }

    public function drawCash($data, $uid, $login_info)
    {
        return DrawCash::getInstance()->apply($data, $uid, $login_info);
    }

    public function commissionSaleDetail($uid, $data, $page_num = 1, $page_size = 5)
    {
        return CommissionLog::getInstance()->commissionSaleDetail($uid, $data, $page_num, $page_size);
    }

    public function commissionBonusList($uid, $level, $page_num = 1, $page_size = 5)
    {
        return CommissionLog::getInstance()->commissionBonusList($uid, $level, $page_num, $page_size);
    }

    public function commissionSaleList($uid, $level, $page_num = 1, $page_size = 5)
    {
        return CommissionLog::getInstance()->commissionSaleList($uid, $level, $page_num, $page_size);
    }

    public function commissionList($uid, $type, $page_num = 1, $page_size = 5)
    {
        return CommissionLog::getInstance()->commissionList($uid, $type, $page_num, $page_size);
    }

    public function commissionListAll($uid)
    {
        return CommissionLog::getInstance()->commissionListAll($uid);
    }

    public function commissionListAllNew($param, $uid)
    {
        return CommissionLog::getInstance()->commissionListAllNew($param, $uid);
    }

    public function addSaleCommission($uid, $money, $percent, $order, $level)
    {
        return CommissionLog::getInstance()->addSaleCommission($uid, $money, $percent, $order, $level);
    }



    public function commissionShare($uid, $money, $order_id)
    {
        return MoneyLog::getInstance()->commissionShare($uid, $money, $order_id);
    }


    public function commissionRebuy($uid, $money, $order_id)
    {
        return MoneyLog::getInstance()->commissionRebuy($uid, $money, $order_id);
    }

    public function commissionYeji($uid, $money, $order_id)
    {
        return MoneyLog::getInstance()->commissionYeji($uid, $money, $order_id);
    }



    public function commissionAgent($uid, $money, $order_id)
    {
        return MoneyLog::getInstance()->commissionAgent($uid, $money, $order_id);
    }

    public function commissionAgent2($uid, $money, $order_id)
    {
        return MoneyLog::getInstance()->commissionAgent2($uid, $money, $order_id);
    }




    public function addManageCommission($uid, $money, $percent, $order)
    {
        return CommissionLog::getInstance()->addManageCommission($uid, $money, $percent, $order);
    }

    public function addPayCommission($uid, $money, $percent, $order)
    {
        return CommissionLog::getInstance()->addPayCommission($uid, $money, $percent, $order);
    }

    public function addBuyCommission($uid, $money, $percent, $order)
    {
        return CommissionLog::getInstance()->addBuyCommission($uid, $money, $percent, $order);
    }

    public function addBuyServiceCommission($uid, $money, $percent, $order)
    {
        return CommissionLog::getInstance()->addBuyServiceCommission($uid, $money, $percent, $order);
    }

    public function addBoundsCommission($uid, $money, $percent, $order, $type)
    {
        return CommissionLog::getInstance()->addBoundsCommission($uid, $money, $percent, $order, $type);
    }
    
    public function addAgentBoundsCommission($uid, $agentFee, $percent,$settle_id)
    {
    	return CommissionLog::getInstance()->addAgentBoundsCommission($uid, $agentFee, $percent,$settle_id);
    }

    public function pointList($uid, $page_num = 1, $page_size = 5)
    {
        return PointLog::getInstance()->pointList($uid, $page_num, $page_size);
    }

    public function addGivePoint($uid, $order_id, $goods_title, $point)
    {
        return PointLog::getInstance()->addGivePoint($uid, $order_id, $goods_title, $point);
    }

    public function addDeviationPoint($uid, $order_id, $goods_title, $point)
    {
        return PointLog::getInstance()->addDeviationPoint($uid, $order_id, $goods_title, $point);
    }

    public function addCancelPoint($uid, $order_id, $goods_title, $point)
    {
        return PointLog::getInstance()->addCancelPoint($uid, $order_id, $goods_title, $point);
    }

    public function getRefererTree($uid, $count = 0, $manage_level = 0, &$referer_tree = [])
    {
        return User::getInstance()->getRefererTree($uid, $count, $manage_level, $referer_tree);
    }

    public function commissionSale($uid, $money, $order_id, $level)
    {
        return MoneyLog::getInstance()->commissionSale($uid, $money, $order_id, $level);
    }

    public function commissionUp($uid, $money,$gwk, $order_id,$level,$zorj)
    {
        return MoneyLog::getInstance()->commissionUp($uid, $money,$gwk, $order_id,$level,$zorj);
    }

    public function commissionBuy($uid, $money, $order_id)
    {
        return MoneyLog::getInstance()->commissionBuy($uid, $money, $order_id);
    }

    public function commissiondayfanli($uid, $money, $order_id)
    {
        return MoneyLog::getInstance()->commissiondayfanli($uid, $money, $order_id);
    }

    public function commissionManage($uid, $money, $order_id)
    {
        return MoneyLog::getInstance()->commissionManage($uid, $money, $order_id);
    }
    
    public function settleAgent($uid, $agentFee,$percent,$settle_id)
    {
    	return MoneyLog::getInstance()->settleAgent($uid, $agentFee,$percent,$settle_id);
    }
    
    public function settleAgentBounds($uid, $money)
    {
    	return MoneyLog::getInstance()->settleAgentBounds($uid, $money);
    }

    public function decodeToken($token)
    {
        return User::getInstance()->decodeToken($token);
    }

    public function register($data)
    {
        return User::getInstance()->register($data);
    }

    public function login($username, $password)
    {
        return User::getInstance()->login($username, $password);
    }

    public function getDetail($uid)
    {
        return User::getInstance()->getDetail($uid);
    }

    public function recharge($uid, $money)
    {
        return MoneyLog::getInstance()->recharge($uid, $money);
    }

    public function dock($uid, $money)
    {
        return MoneyLog::getInstance()->dock($uid, $money);
    }

    public function mallMonthBonus($uid, $money)
    {
        return MoneyLog::getInstance()->mallMonthBonus($uid, $money);
    }

    public function mallYearBonus($uid, $money)
    {
        return MoneyLog::getInstance()->mallYearBonus($uid, $money);
    }

    public function remit($uid, $money, $remit)
    {
        return MoneyLog::getInstance()->remit($uid, $money, $remit);
    }

    public function pay($uid, $money, $order)
    {
        return MoneyLog::getInstance()->pay($uid, $money, $order);
    }
    
    public function reset($uid, $data = array())
    {
    	return User::getInstance()->reset($uid, $data);
    }

    public function resetPayPwd($uid, $password = '')
    {
        return User::getInstance()->resetPayPwd($uid, $password);
    }

    public function resetPwd($uid, $password = '')
    {
        return User::getInstance()->resetPwd($uid, $password);
    }

    public function fillUserInfo($data, $uid)
    {
        return User::getInstance()->fillUserInfo($data, $uid);
    }

    public function checkPayPassword($uid, $pay_password)
    {
        return User::getInstance()->checkPayPassword($uid, $pay_password);
    }

    public function findPwd($data)
    {
        return User::getInstance()->findPwd($data);
    }

    public function changePassword($data, $uid)
    {
        return User::getInstance()->changePassword($data, $uid);
    }

    public function changePayPassword($data, $uid)
    {
        return User::getInstance()->changePayPassword($data, $uid);
    }

    public function finance($uid)
    {
        return User::getInstance()->finance($uid);
    }

    public function getSynLoginUrl($uid,$go)
    {
        return User::getInstance()->getSynLoginUrl($uid,$go);
    }

    public function getUserInfo($uid, $field = 'uid')
    {
        return User::getInstance()->getUserInfo($uid, $field);
    }

    public function getSubUserList($uid, $level = 1, $page_num = 1, $page_size = 5)
    {
        return User::getInstance()->getSubUserList($uid, $level, $page_num, $page_size);
    }

    public function increaseSelfBuy($order)
    {
        return User::getInstance()->increaseSelfBuy($order);
    }

    public function register2($data)
    {
        return User::getInstance()->register2($data);
    }

    public function wxLogin($openid)
    {
        return User::getInstance()->wxLogin($openid);
    }

    public function user_import($xls_file)
    {
        return User::getInstance()->user_import($xls_file);
    }
    public function bindWx($data, $uid)
    {
        return User::getInstance()->bindWx($data, $uid);
    }
    public function daoUser(){
        return User::getInstance()->daoUser();
    }
    public function shellDaoUser()
    {
        return User::getInstance()->shellDaoUser();
    }

    public function teamSettle()
    {
        return CommissionLog::getInstance()->teamSettle();
    }

    public function shellTeamSettle()
    {
        return CommissionLog::getInstance()->shellTeamSettle();
    }

    public function fanli()
    {
        return CommissionLog::getInstance()->fanli();
    }

    public function shellfanli()
    {
        return CommissionLog::getInstance()->shellfanli();
    }

    public function genApiToken($uid){
        return User::getInstance()->genApiToken($uid);
    }

    public function refund($uid, $money, $order_id)
    {
        return MoneyLog::getInstance()->refund($uid, $money, $order_id);
    }

    public function gwqList($uid, $type, $page_num = 1, $page_size = 5)
    {
        return GwqLog::getInstance()->gwqList($uid, $type, $page_num, $page_size);
    }

    public function drawCashBank($data, $uid, $login_info)
    {
        return DrawCash::getInstance()->drawCashBank($data, $uid, $login_info);
    }

    //绑定老会员
   /* public function bindOldUser($data, $uid, $login_info)
    {
        return User::getInstance()->bindOldUser($data, $uid, $login_info);
    }*/
}