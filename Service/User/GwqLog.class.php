<?php
// +-------------------------------------------------------------------
// |
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Service\User;

class GwqLog extends \Service\Common\BaseModel
{
    protected $name = 'gwq_log';

    CONST TYPE_REWARD = 1;//奖励
    CONST TYPE_CONSUME = 2;//消费
    CONST TYPE_IN = 3;//转入
    CONST STATUS_OUT = 4;//转出

    const TYPE_LABEL = [
        self::TYPE_REWARD => '奖励',
        self::TYPE_CONSUME => '消费',
        self::TYPE_IN => '转入',
        self::STATUS_OUT => '转出',
    ];


    //问题列表
    public function gwqList($uid, $page_num, $page_size)
    {
        $where = [
            'uid' => $uid,
            'type' =>  ['NEQ', 5]
        ];
        $list = (array)$this->where($where)->order('create_time desc,id desc')->page($page_num, $page_size)->select();
        $count = $this->where($where)->count();
        return [
            $this->format($list),
            $page_count = $count,
            $page_total = ceil($count / $page_size),
        ];
    }


    //格式化
    public function format($list)
    {
        if (!$list) {
            return $list;
        }

        $is_one = count($list) == count($list, 1);
        $list = $is_one ? [$list] : $list;

        foreach ($list as &$row) {
            $row['type_label'] = self::TYPE_LABEL[$row['type']];
        }
        return $is_one ? $list[0] : $list;
    }


}