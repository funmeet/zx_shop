<?php
// +-------------------------------------------------------------------
// |
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Service\User;

class Question extends \Service\Common\BaseModel
{
    protected $name = 'question';

    CONST STATUS_SUBMIT = 0;//已提交
    CONST STATUS_RESOLVED = 1;//已解决
    CONST STATUS_UNRESOLVED = 2;//未解决
    CONST STATUS_CLOSED = 3;//已关闭

    const STATUS_LABEL = [
        self::STATUS_SUBMIT => '已提交',
        self::STATUS_RESOLVED => '已解决',
        self::STATUS_UNRESOLVED => '未解决',
        self::STATUS_CLOSED => '已关闭',
    ];

    //问题评价
    public function questionEvaluate($data, $uid)
    {
        $question_id = $data['question_id'];
        $status = $data['status'];

        $question = $this->where([
            'id' => $question_id,
            'uid' => $uid,
        ])->find();
        if (!$question) {
            throw new \Exception("无效的问题");
        }

        if ($question['status'] != self::STATUS_SUBMIT) {
            throw new \Exception("该问题已关闭");
        }

        $this->save([
            'id' => $question_id,
            'status' => $status,
        ]);
    }

    //问题提交
    public function submit($data, $uid)
    {
        $question_type = get_list_config('QUESTION_TYPE');
        if (!isset($question_type[$data['type']])) {
            throw new \Exception("无效的分类");
        }

        $this->add([
            'uid' => $uid,
            'title' => $data['title'],
            'type' => $data['type'],
            'create_time' => date('Y-m-d H:i:s'),
            'is_read' => 1,
        ]);
    }

    //问题列表
    public function lists($uid, $page_num, $page_size)
    {
        $where = [
            'uid' => $uid,
        ];
        $list = (array)$this->where($where)->order('update_time desc,id desc')->page($page_num, $page_size)->select();
        $count = $this->where($where)->count();
        return [
            $this->format($list),
            $page_count = $count,
            $page_total = ceil($count / $page_size),
        ];
    }

    //问题详情
    public function detail($question_id, $uid, $login_info)
    {
        $question = $this->where([
            'id' => $question_id,
            'uid' => $uid,
        ])->find();
        if (!$question) {
            throw new \Exception("无效的问题");
        }

        $this->save([
            'id' => $question_id,
            'is_read' => 1,
        ]);

        $list = (array)M('question_answer')
            ->where([
                'uid' => $uid,
                'question_id' => $question_id
            ])
            ->order('id asc')
            ->limit(0, 50)
            ->select();
        foreach ($list as &$row) {
            $row['is_questioner'] = $row['uid'] == $row['answer_uid'];
            if ($row['is_questioner']) {
                $row['avatar_url'] = $login_info['avatar_url'];
            } else {
                $row['avatar_url'] = '/H5/images/index_icons_17.png';
            }
        }

        return [
            'question' => $this->format($question),
            'list' => $list
        ];
    }

    //格式化
    public function format($list)
    {
        if (!$list) {
            return $list;
        }

        $is_one = count($list) == count($list, 1);
        $list = $is_one ? [$list] : $list;

        $question_type = get_list_config('QUESTION_TYPE');
        foreach ($list as &$row) {
            $row['type_label'] = $question_type[$row['type']];
            $row['status_label'] = self::STATUS_LABEL[$row['status']];
        }
        return $is_one ? $list[0] : $list;
    }

    //回答问题
    public function answer($question_id, $uid, $content, $is_admin = 0)
    {
        $question = $this->where([
            'id' => $question_id,
        ])->find();
        if (!$question) {
            throw new \Exception("无效的问题");
        }

        if (!($question['uid'] == $uid || $is_admin)) {
            throw new \Exception("非法操作");
        }

        if ($question['status'] != self::STATUS_SUBMIT) {
            throw new \Exception("该问题已结束,不可操作");
        }

        M('question_answer')->add([
            'question_id' => $question_id,
            'answer_uid' => $uid,
            'content' => $content,
            'uid' => $question['uid'],
            'create_time' => date('Y-m-d H:i:s'),
        ]);

        $this->save(array_merge(
            [
                'id' => $question_id,
                'update_time' => date('Y-m-d H:i:s'),
            ],
            $is_admin ? ['is_read' => 0] : []
        ));

        //发送通知
        if ($is_admin) {
            OE('user')->addCustomerService($question);
        }
    }
}