<?php
// +-------------------------------------------------------------------
// | 
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------

// 项目环境配置
return [
    'APP_DEBUG' => true,//是否开启调试模式
    'API_DOC' => true,//是否允许api在线调试

    /**
     * ---------------------------------------------------------
     * MySQL 配置
     * ---------------------------------------------------------
     */
     'DB_HOST' => '127.0.0.1',

    'DB_NAME' => 'zx_shop',

    'DB_USER' => 'zx_shop',

    'DB_PWD' => 'zx_shop.123321.',

    'DB_PORT' => '3306',   

 /* 'DB_HOST' => '127.0.0.1',

'DB_NAME' => 'qch',

'DB_USER' => 'qch',

'DB_PWD' => 'qch.123',

'DB_PORT' => '3306',  */

    //redis
    'REDIS_HOST' => '0.0.0.0',
    'REDIS_PORT' => '6379',
    'REDIS_PASSWORD' => '123456',
    'REDIS_DATABASE' => 0,

    //ldap
    //'LDAP_HOST' => '0.0.0.0',
    //'LDAP_PORT' => '389',
    //'LDAP_RDN' => '',
    //'LDAP_PASSWORD' => '',

    /**
     * ---------------------------------------------------------
     * 安全配置
     * ---------------------------------------------------------
     */

    // 数据加密 key
    'DATA_ENCRYPT_KEY' => 'ec6780aa101f0395c293a14514d451c4d',

    //以下配置 上线得设置成false
    'DEFAULT_MODULE' => 'Wap',
    'OPEN_WX_AUTH' => false,//是否必须微信授权
    'IS_ENTERPRISE_WX' => false,//是否是企业号
    'WECAHT_JSSDK_DEBUG' => false,//是否开启微信jssdk调试
    'OPEN_ONLINE_DOC' => false,//是否开启在线文档
    'LOGIN_OPEN_DEBUG' => false,//是否开启登录调试模式
    'WX_JS_CONFIG_OPEN_DEBUG' => false,//是否开启微信js配置调试模式
    'XCX_CODE_OPEN_DEBUG' => false,//小程序code获取openid是否开启调试模式
    'LOGIN_DEBUG' => false,//模拟登陆模式

    //微信JS配置测试key  WX_JS_CONFIG_OPEN_DEBUG=true时才生效  用于微信授权是模拟  微信上传是用微信测试号（黄明的测试号）
    'WX_JS_CONFIG_APP_KEY' => '',
    'WX_JS_CONFIG_APP_SECRET' => '',

    //微信小程序配置
    'XCX_APP_ID' => '',
    'XCX_APP_SECRET' => '',

    // ---------------------------------------------------------
    // 计划任务配置
    // ---------------------------------------------------------
    'CRON_CLI' => true, // 使用 cli 模式运行计划任务
//    'CRON_COMMAND' => '/www/server/php/70/bin/php -c /www/server/php/70/etc/php.ini ',
    'CRON_COMMAND' => '/usr/bin/php ',

    //日志配置
    'LOG_TIME_LIMIT' => 500,

    'PAGE_SIZE' => 50

];