ALTER TABLE `k_user`
ADD `total_rebuy_commission` decimal(10,2) NOT NULL DEFAULT '0' COMMENT '总重复消费奖' AFTER `total_sale2_commission`,
ADD `total_share_commission` decimal(10,2) NOT NULL DEFAULT '0' COMMENT '总分享奖金' AFTER `total_rebuy_commission`,
ADD `total_yeji_commission` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '总业绩奖' AFTER `total_share_commission`,
ADD `total_agent_commission` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '总代理奖' AFTER `total_yeji_commission`,
ADD `total_agent2_commission` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '总推荐代理奖' AFTER `total_agent_commission`;