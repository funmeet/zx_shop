<?php
namespace Think;
/**
 * 文件上传处理
 */
class Kuploader
{
    // 配置项 : 允许上传的文件类型,不区分大小写
    protected $allowedType = array(
            'jpg',
            'png',
            'gif'
    );
    
    // 配置项 : 临时文件过期时间, 单位秒(s)
    protected $tempFileExpire = 3600;
    
    // 配置项 : 文件上传完毕后移动到此目录, 默认不移动
    protected $uploadDir = null;
    
    // 配置项 : 文件自动保存目录的 web 路径, 未配置时使用 /+$uploadDir 作为默认值
    protected $uploadPath = null;
    
    // 配置项 : 文件上传临时目录, 必填
    protected $tempDir = null;
    
    // 配置项 : 文件临时目录的 web 路径, 未配置时使用 /+$tempDir 作为默认值
    protected $tempPath = null;
    
    // 当前上传文件的文件名
    protected $filename = null;
    
    // 当前文件类型
    protected $fileType = null;
    
    // 错误通知类型
    protected static $notifyInfo = array(
            1000 => '无法获取文件名',
            1001 => '文件名不合法',
            1002 => '创建临时文件失败',
            1003 => '文件上传错误',
            1004 => '文件未上传',
            1005 => '无法读取上传临时文件',
            1006 => '无法读取输入流',
            1007 => '无法移动文件到上传目录',
            1008 => '无法移动文件到临时目录',
            1009 => '创建上传目录失败',
            1010 => '未设置临时目录',
            1011 => '创建临时目录失败'
    );

    protected $application;

    protected $webPath;

    const LOG_TAG = 'KCSUpload';

    /**
     * 文件上传处理流程
     */
    public function run ($config = array())
    {
        $this->_loadConfig($config);
        
        // 获取文件信息
        $this->_loadFileinfo();
        
        // 文件保存路径
        $filePath = $this->tempDir . DIRECTORY_SEPARATOR . $this->filename . '.part';
        
        // 清理过期文件
        // $this->_cleanUp();
        
        // 分段上传 : 当前分段序号
        $chunk = isset($_REQUEST["chunk"]) ? intval($_REQUEST["chunk"]) : 0;
        
        // 分段上传 : 分段总数
        $chunks = isset($_REQUEST["chunks"]) ? intval($_REQUEST["chunks"]) : 0;
        
        // 打开或创建临时文件
        $out = fopen("{$filePath}", $chunks ? "ab" : "wb") or $this->_notify(1002);
        
        // 从上传文件或输入中读取数据
        if (! empty($_FILES) && isset($_FILES['kcs_uploader_file']))
        {
            $_FILES["kcs_uploader_file"]["error"] and $this->_notify(1003, $_FILES["file"]["error"]);
            is_uploaded_file($_FILES["kcs_uploader_file"]["tmp_name"]) or $this->_notify(1004);
            $in = fopen($_FILES["kcs_uploader_file"]["tmp_name"], "rb") or $this->_notify(1005);
        }
        else
        {
            $in = fopen("php://input", "rb") or $this->_notify(1006);
        }
        
        // 追加到临时文件
        while ($buff = fread($in, 4096))
        {
            fwrite($out, $buff);
        }
        fclose($out);
        fclose($in);
        
        // 分段上传
        if ($chunks && $chunk < $chunks - 1)
        {
            $this->_notify(true);
        }
        
        // 上传完毕
        if (! $chunks || $chunk == $chunks - 1)
        {
            // 重命名上传文件
            $newFilename = uniqid(rand(100, 999)) . '.' . $this->fileType;
            
            // 设置了上传目录
            if ($this->uploadDir)
            {
                // 上传图片移动后的路径
                $newFile = $this->uploadDir . DIRECTORY_SEPARATOR . $newFilename;
                // 上传图片的 web 路径
                $webPath = $this->uploadPath . DIRECTORY_SEPARATOR . $newFilename;
                // 移动临时文件到指定的上传目录
                rename($filePath, $newFile) or $this->_notify(1007, $filePath . '=>' . $newFile);
            }
            // 使用临时目录
            else
            {
                // 上传图片临时路径
                $newFile = $this->tempDir . DIRECTORY_SEPARATOR . $newFilename;
                // 上传图片的 web 路径
                $webPath = $this->tempPath . DIRECTORY_SEPARATOR . $newFilename;
                // 重命名临时文件
                rename($filePath, $newFile) or $this->_notify(1008, $filePath . '=>' . $newFile);
            }
            
            // web 路径修正
            $webPath = str_replace('\\', '/', $webPath);
            
            // 发送成功通知
            $this->_notify(true, $webPath);
        }
    }

    /**
     * 移动上传文件
     */
    public function move ($webPath, $uploadDir = '', $config = array())
    {
        $this->_loadConfig($config);
        
        if ($this->uploadDir)
        {
            $oldFile = $this->uploadDir . substr($webPath, 0, strlen($this->uploadPath));
        }
        else
        {
            $oldFile = $this->tempDir . substr($webPath, 0, strlen($this->tempPath));
        }
        
        $newFile = trim($uploadDir, '/') . '/' . basename($oldFile);
        
        if (! rename($oldFile, $newFile))
        {
            //KCS::log("移动文件失败 : $oldFile -> $newFile", self::LOG_TAG);
            return false;
        }
        
        return $newFile;
    }

    /**
     * 加载配置项, 并校验
     */
    protected function _loadConfig ($config)
    {
        is_array($config) or $config = array();
        foreach ($config as $_key => $_value)
        {
            if (property_exists($this, $_key))
            {
                $this->$_key = $_value;
            }
        }
        
        // 检查临时目录
        $this->tempDir or $this->_notify(1010);
        if (! file_exists($this->tempDir))
        {
            mkdir($this->tempDir, 0777, true) or $this->_notify(1011, $this->$this->tempDir);
        }
        
        // 修正临时目录 web 路径
        $this->tempPath or $this->tempPath = '/' . $this->tempDir;
        
        // 检查上传目录
        if (! $this->uploadDir)
        {
            return;
        }
        
        // 创建上传目录
        if (! file_exists($this->uploadDir))
        {
            mkdir($this->uploadDir, 0777, true) or $this->_notify(1009, $this->uploadDir);
        }
        
        // 修正上传目录 web 路径
        $this->uploadPath or $this->uploadPath = '/' . $this->uploadDir;
    }

    /**
     * 获得上传文件的文件名和类型并校验
     */
    protected function _loadFileinfo ()
    {
        if (isset($_REQUEST["name"]))
        {
            $filename = $_REQUEST["name"];
        }
        elseif (! empty($_FILES))
        {
            $filename = @$_FILES["kcs_uploader_file"]["name"];
        }
        else
        {
            // 获取文件名失败
            $this->_notify(1000);
        }
        
        // 文件名只允许包含 数字,字母,下划线和点号
        preg_match('/^[a-zA-Z0-9_\.]+$/', $filename) or $this->_notify(1001, '文件名无效 : ' . $filename);
        
        $this->filename = $filename;
        
        $filetype = strtolower(pathinfo($this->filename, PATHINFO_EXTENSION));
        
        // 文件类型检测
        in_array($filetype, $this->allowedType) or $this->_notify(1001, '文件类型无效 : ' . $filetype);
        
        $this->fileType = $filetype;
    }

    /**
     * 清除过期的临时文件, 删除的临时文件写入清理日志
     */
    protected function _cleanUp ($targetDir)
    {
        return;
        while (($file = readdir($this->tempDir)) !== false)
        {
            $tmpfilePath = $this->tempDir . DIRECTORY_SEPARATOR . $file;
            
            // Remove temp file if it is older than the max age and is not the current file
            if (filemtime($tmpfilePath) < time() - $this->tempFileExpire)
            {
                $filesize = number_format(filesize($tmpfilePath) / (1024 * 1024), 2) . 'KB';
                @$status = unlink($tmpfilePath);
                //KCS::log("清理过期临时文件 [" . ($status ? '1' : '0') . "] : $file (" . $filesize . ")", self::LOG_TAG);
            }
        }
        closedir($dir);
    }

    /**
     * 发送通知, 错误信息保存到 KCSUpload 日志内
     */
    protected function _notify ($status = '', $logMsg = null)
    {
        $json = array(
                'jsonrpc' => '2.0',
                'id' => 'id'
        );
        
        if ($status !== true)
        {
            
            $msg = self::$notifyInfo[$status];
            
            //KCS::log($msg . ' ' . $logMsg, self::LOG_TAG);
            
            $json['error'] = array(
                    'code' => $status,
                    'message' => $msg
            );
        }
        else
        {
            $json['result'] = $logMsg;
        }
        
        exit(json_encode($json));
    }
}