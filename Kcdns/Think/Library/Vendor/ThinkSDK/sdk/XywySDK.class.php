<?php
// +----------------------------------------------------------------------
// | TOPThink [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2010 http://topthink.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: 麦当苗儿 <zuojiazi.cn@gmail.com> <http://www.zjzit.cn>
// +----------------------------------------------------------------------
// | TencentSDK.class.php 2013-02-25
// +----------------------------------------------------------------------

class XywySDK extends ThinkOauth{

	/**
	 * 获取requestCode的api接口
	 * @var string
	 */
	protected $GetRequestCodeURL = 'https://passport.xywy.com/oauthlogin/';

        /**
	 * 获取access_token的api接口
	 * @var string
	 */
	protected $GetAccessTokenURL = 'http://api.wws.xywy.com/api.php/user/userCheckMsg/index';

        /**
	 * API根路径
	 * @var string
	 */
	protected $ApiBase = 'https://api.weixin.qq.com/';


	protected $userName = '';
	protected $checkMsg = '';
	protected $oauthUid = '';

	/**
	 * 请求code
	 */
	public function getRequestCodeURL($callback){

		if(!empty($callback))
			$this->Callback = $callback;
		else
			throw new Exception('请配置回调页面地址');

		$miniType = I('minitype');
		if (!$miniType) throw new Exception('miniType is null');
		if ($miniType=='member') {
		    return 'https://passport.xywy.com/member/login.htm?ucback=' . $this->Callback . '?code=1';
		}
		$miniType .= ($miniType=='alipay') ? '/alipay_login.php' : '/login.php';

		//http://blog.dev.kcdns.net/xoauth/sina.html
		//Oauth 标准参数
		ismobile() && $params["source"] = '3g';//当非移动端时不带该参数
		$params["backurl"] = $this->Callback . '?code=1';

		//获取额外参数
		if($this->Authorize){
			parse_str($this->Authorize, $_param);
			if(is_array($_param)){
				$params = array_merge($params, $_param);
			} else {
				throw new Exception('AUTHORIZE配置不正确！');
			}
		}
		return $this->GetRequestCodeURL . $miniType . '?' . http_build_query($params);
	}


	/**
	 * 组装接口调用参数 并调用接口
	 * @param  string $api    微信API
	 * @param  string $param  调用API的额外参数
	 * @param  string $method HTTP请求方法 默认为GET
	 * @return json
	 */
	public function call($api, $param = '', $method = 'GET', $multi = false){
		/* 腾讯微信调用公共参数 */
		$params = array(
			'access_token'       => $this->Token['access_token'],
			'openid'             => $this->openid(),
			'lang'             => 'zh_CN'
		);

		$vars = $this->param($params, $param);
		$data = $this->http($this->url($api), $vars, $method, array(), $multi);
		return json_decode($data, true);
	}

	/**
	 * 解析access_token方法请求后的返回值
	 * @param string $result 获取access_token的方法的返回值
	 */
	protected function parseToken($result, $extend){
		
		//var_dump($result);exit;
		$data = json_decode($result,true);
		if($data['code']=='10000'){
			$data['access_token'] = $this->checkMsg;
			$data['openid'] = $data['data']['c_uid'];
			//$data['username'] = $this->userName;
			//$data['nickname'] = $data['data']['c_nick'];
			return $data;
		}else
			throw new Exception("获取 ACCESS_TOKEN 出错：{$data['code']}{$data['msg']}");
	}

	/**
	 * 获取当前授权应用的openid
	 * @return string
	 */
	public function openid(){
		$data = $this->Token;
		if(isset($data['openid']))
			return $data['openid'];
		else
			throw new Exception('没有获取到openid！');
	}

        /**
	 * 获取access_token
	 * @param string $code 上一步请求到的code
	 */
	public function getAccessToken($code, $extend = null){

		//$this->nickName = I('cookie.cookie_usern');
		//$this->userName = I('cookie.cookie_username');
		$this->checkMsg = urldecode(I('cookie.check_msg'));
		$this->oauthUid = I('cookie.cookie_user');
		
		$params = array(
			'act' => 'decode',
			'check_msg' => $this->checkMsg,
			//'username' => $this->userName,
			'uid' => $this->oauthUid,
			'version' => '1.1',
			'source' => 'dks',
			'os' => 'pc',
			'api' => '792',
			'pro' => $this->AppSecret
		);
			//var_dump(md5('act=decode&api=792&check_msg=RZObfwiQ0X0LnASEVyP2yxWUm6wqzUW9J%252FCqLQzSHmTah5gMittYGdtjWMPfVX6v3GaxzPEDw0U4MBjn0sdS5Aa3M%252Fnf67JEvkwCB2jnnk10m92znY2kZKyKKC0JVBAph2iesIZpWokOvIxfEzFADIiAq63mA0cEp7y8So6V2qsTJGK4f84AT5iGEtX3KChr%252FJ2nmVyKhjFh0%252FVbvSFd1VIo1D8PdPKFQR8ymun54Sens9t5Bra%252FWzWT2mSL06XRT4ltb%252BiFYVZebEQxjcuBaduM93z8WB2lorIhD7KI2n57BLlhddKluuNLSt00%252FzFDnQf8KsA8Dno%253D&os=ios&pro=xywyf32l24WmcqquqqTdhXaIkQ&source=dks&uid=112726701&version=1.1Ayj+@iixt*c@O.@o'));
			//var_dump(urlencode('RZObfwiQ0X0F4nZAaM5cU10ZDcThnWgwtagyY4nVAAvAs7ISqRAqhCz82K%252B56jkcbNnPtpBayBZeM%252FBinNGU93fNA%252FYF5Kh9Y6cjUWSNZROxik%252FCN1kUHLXoSsEzvLyfb5x3S2d%252FckQk0nd2SEsNsZoThCGH4xjUAUNxLwkY4NuJ4H36JrM9stvsyjP%252B%252BvGV4iVe%252BYReqSd5%252BqDgUGEJyOPE2qxjLRgHAX2bh7nH%252B4bY56D8L3sHRfGsSAJc32xic0F8eLH1TsYdh7k1AaDAmHUYK%252FpaqGeLz2tO5KBvaWh9goxLxW%252BWsREZAp8ybbti4v1Z6f5yUsuIfpvqX1tNSLy8xT7sjJbWaRtXeix1MFI8zehQURrcpJ1N2IZqXaac5QeHmut7d65mYkCrJb9xdg%253D%253D'));
		ksort($params);
		reset($params);

		foreach($params as $k => $v){
			$paramStr .= $k.'='.$v.'&';
		}
		$paramStr = rtrim($paramStr,'&');
		//var_dump($paramStr);
		$params['sign'] = md5($paramStr . $this->AppKey);
		//$params['check_msg'] = $this->checkMsg;
		//var_dump($params);

		$data = $this->http($this->GetAccessTokenURL, $params, 'GET');
		$this->Token = $this->parseToken($data, $extend);
		return $this->Token;
	}

}
