<?php
// +-------------------------------------------------------------------
// | 
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Kcdns\Service\Sms\drivers;

/**
 * 短信发送驱动 通用驱动
 *
 * @package Service\Util
 *         
 */
class Common extends \Service\Sms\DriverBase
{
    
    //短信发送
    public function send($mobile,$content){
        
        $params=$this->params;
        $params['content']=$content;
        $params['to']=$mobile;
        
        $response=$this->http_get($this->api_url,$params);
        
        //发送后 返回信息 通过统一方法封装返回  参数:发送状态   接口完整响应  错误码  错误描述
        return $this->parseResponse($this->http_code=='200',$response,$this->http_code=='200'?"":$this->http_code,$this->http_code=='200'?'':'短信发送失败');
    }
    
}