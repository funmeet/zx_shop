<?php
// +-------------------------------------------------------------------
// | 
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Kcdns\Service\Sms\drivers;

/**
 * 短信发送驱动 通用驱动
 *
 * @package Service\Util
 *
 */
class Aliyun extends \Kcdns\Service\Sms\DriverBase
{

    //短信发送
    public function send($mobile, $content)
    {
        $params = [];
        $params['sign'] = 500016;
        $params['skin'] = is_numeric($content) ? 900044 : 900045;
        $params['phone'] = $mobile;
        $params['param'] = $content;

        $header = [];
        $header[] = "Authorization:APPCODE " . $this->params['AppCode'];

        $response = $this->http_get($this->api_url, $params, $header);
        $result = json_decode($response, true);

        //发送后 返回信息 通过统一方法封装返回  参数:发送状态   接口完整响应  错误码  错误描述
        return $this->parseResponse(
            $result['Code'] === 'OK',
            $response,
            isset($result['Code']) ? $result['Code'] : $this->http_code,
            isset($result['Message']) ? $result['Message'] : '短信发送失败'
        );
    }

}