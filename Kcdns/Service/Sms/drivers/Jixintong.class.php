<?php
// +-------------------------------------------------------------------
// | 
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Kcdns\Service\Sms\drivers;

/**
 * 短信发送驱动 吉信通
 *
 * @package Service\Util
 *         
 */
class Jixintong extends \Service\Sms\DriverBase
{
    
    //短信发送
    public function send($mobile,$content){
        
        $params=$this->params;
        $params['time']="";
        $params['content']=iconv("UTF-8", "GB2312", $content);
        $params['to']=$mobile;
        
        //success:   "000/Send:1/Consumption:.1/Tmoney:46.6/sid:1206153201792676"
        //error  :   ""-03/Send:1/Consumption:0/Tmoney:0/sid:""
        $response=$this->http_get($this->api_url,$params);
        
        $responseArr=explode('/',$response);
        
        //发送后 返回信息 通过统一方法封装返回  参数:发送状态   接口完整响应  错误码  错误描述
        return $this->parseResponse($responseArr[0]==='000',$response,$responseArr[0],$responseArr[0]==='000'?'':'短信发送失败');
    }
    
}