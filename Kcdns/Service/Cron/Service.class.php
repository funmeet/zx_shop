<?php
// +-------------------------------------------------------------------
// | 
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Kcdns\Service\Cron;

/**
 * 计划任务
 */
class Service
{

    public function start ($cliCommand = '')
    {
        Cron::start($cliCommand);
    }

    public function stop ()
    {
        Cron::stop();
    }

    public function run ($taskId)
    {
        Cron::run($taskId);
    }

    public function status ()
    {
        return Cron::status();
    }

    public function crontime ($cron)
    {
        return CronExpression::factory($cron)->getNextRunDate()->getTimestamp();
    }
}