<?php
// +-------------------------------------------------------------------
// | 
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Kcdns\Service\Util;

class Service
{

    // 数据验证
    public function validate($data, $rules = array(), $filter = true)
    {
        return Validate::getInstance()->check($data, $rules);
    }

    // 网络请求
    public function curl($url = '', $data = '', $isPost = false, $options = [])
    {
        return Curl::run($url, $data, $isPost, $options);
    }

    /**
     * 生成二维码图片
     *
     * @param string $text
     *            文本
     * @param number $size
     *            放大倍数
     * @param number $margin
     *            边距基数
     * @param number $level
     *            容错级别 0,1,2,3(最高)
     */
    public function qrcode($text = '', $size = 10, $margin = 1, $level = 1)
    {
        $pngFile = md5($text . C('DATA_AUTH_KEY')) . rand(1000,9999) . "_{$level}_{$size}_{$margin}.png";
        $pngDir = "./Uploads/Qrcode/" . substr($pngFile, 0, 2);
        file_exists($pngDir) or mkdir($pngDir, 0777, true);
        $pngPath = $pngDir . '/' . $pngFile;

        if (!file_exists($pngPath)) {
            require_once __DIR__ . '/phpqrcode/phpqrcode.php';
            \QRcode::png($text, $pngPath, $level, $size, $margin);
        }

        return ltrim($pngPath, '.');
    }

    public function logQrcode($text, $logo = '')
    {
        $pngFile = md5($text . $logo . C('DATA_AUTH_KEY')) . ".log.png";
        $pngDir = "./Uploads/Qrcode/" . substr($pngFile, 0, 2);
        is_dir($pngDir) or mkdir($pngDir, 0777, true);
        $pngPath = $pngDir . '/' . $pngFile;

        if (is_file($pngPath)) {
            return ltrim($pngPath, '.');
        }

        if ($logo == '') {
            return false;
        }

        $png = '.' . $this->qrcode($text);
        if (!is_file($png)) {
            return false;
        }

        $imgStr = file_get_contents($logo);
        if (!$imgStr) {
            return false;
        }

        $QR = imagecreatefrompng($png);
        $logo = imagecreatefromstring($imgStr);
        $QR_width = imagesx($QR);
        $logo_width = imagesx($logo);
        $logo_height = imagesy($logo);
        $logo_qr_width = $QR_width / 5;
        $scale = $logo_width / $logo_qr_width;
        $logo_qr_height = $logo_height / $scale;
        $from_width = ($QR_width - $logo_qr_width) / 2;
        imagecopyresampled($QR, $logo, $from_width, $from_width, 0, 0, $logo_qr_width, $logo_qr_height, $logo_width, $logo_height);
        imagepng($QR, $pngPath);
        imagedestroy($QR);

        return ltrim($pngPath, '.');
    }

    /**
     * 生成随机字符串
     *
     * @param unknown $length
     *            长度
     * @param number $type
     *            1-数字;2-数字加大写字母;
     * @param string $prefix
     *            前缀
     * @param string $suffix
     *            后缀
     */
    public function rand($length, $type = 1, $prefix = '', $suffix = '')
    {
        return Random::string($length, $type, $prefix, $suffix);
    }

    /**
     * 设置 / 输出 SEO 信息
     *
     * @param unknown $key
     * @param string $value
     */
    public function seo($key = null, $value = '')
    {
        return $key ? Seo::set($key, $value) : Seo::getHtml();
    }

    /**
     * 发送邮件
     *
     * @param unknown $emailAddress
     *            收件人列表, 字符串或数组
     * @param string $subject
     *            邮件标题
     * @param string $body
     *            邮件内容
     * @param unknown $attachment
     *            附件列表
     */
    public function email($emailAddress, $subject = '', $body = '', $attachment = array())
    {
        return Email::send($emailAddress, $subject, $body, $attachment);
    }

    /**
     * 静态资源操作
     *
     * @param $assets string
     *            true : 返回 html 标签
     *            false : 清空待加载静态资源文件列表
     *            字符串 : 文件url(绝对路径), 注册为待加载静态资源文件
     *            数组 : 更新配置
     *
     * @return string
     */
    public function assets($assets = true)
    {
        return Assets::import($assets);
    }

    /**
     * post 数据签名代码
     */
    public function postToken()
    {
        return SecureEntry::postToken();
    }

    public function readXsl($file, $del_first_line = false)
    {
        return Excle::readXsl($file, $del_first_line);
    }

    public function writeXls($data, $filename)
    {
        return Excle::writeXls($data, $filename);
    }

    public function encodeJWT($token)
    {
        $jwt = JWT::encode($token, DATA_ENCRYPT_KEY);
        return $jwt;
    }

    public function decodeJWT($jwt)
    {
        $decoded = JWT::decode($jwt, DATA_ENCRYPT_KEY, array('HS256'));
        return (array)$decoded;
    }

    /**
     * 判断是否微信客户端
     */
    public function isWxClient ()
    {
        return strpos($_SERVER['HTTP_USER_AGENT'], 'MicroMessenger') !== false;
    }

    /**
     * 判断是否支付宝客户端
     */
    public function isAlipayClient ()
    {
        return strpos($_SERVER['HTTP_USER_AGENT'], 'AlipayClient') !== false;
    }
}
