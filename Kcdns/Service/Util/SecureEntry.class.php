<?php
// +-------------------------------------------------------------------
// | 
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Kcdns\Service\Util;

class SecureEntry
{

    protected static $entrys = [];

    protected static $config = [
            // 受保护入口列表
            'SECURE_ENTRY' => [],
            // 不对 url 签名的入口
            'SECURE_ENTRY_URL_IGNORE' => [],
            // 使用会话 url 签名的入口
            'SECURE_ENTRY_URL_PRIVATE' => [],
            // TODO : url 签名变量白名单, 用于兼容一些 js 自动添加的变量
            'SECURE_ENTRY_URL_EXCEPT' => [],
            // url 签名全局密钥
            'SECURE_ENTRY_URL_KEY' => '',
            // 不校验 post 签名的入口
            'SECURE_ENTRY_POST_IGNORE' => [],
            // post 签名令牌有效期
            'SECURE_ENTRY_POST_EXPIRE' => 60,
            // get / post 输入参数过滤白名单
            'SECURE_ENTRY_INPUT_IGNORE' => []
    ];

    protected static $route = '';

    public static function init ($config = [])
    {
        self::$config = array_merge(self::$config, $config ?  : []);
        self::$route = MODULE_NAME . '/' . CONTROLLER_NAME . '/' . ACTION_NAME;
        
        SecureEntry\Url::init(self::$config, self::$route);
        SecureEntry\Post::init(self::$config, self::$route);
        SecureEntry\Input::init(self::$config, self::$route);
    }

    /**
     * 安全检查
     */
    public static function check ()
    {
        // 入口不在保护范文内
        if (self::isProtected(self::$route))
        {
            SecureEntry\Url::check();
            SecureEntry\Post::check();
        }
        return true;
    }

    /**
     * url 签名
     */
    public static function signUrl ($url, $route = '')
    {
        return self::isProtected($route) ? SecureEntry\Url::sign($url, $route) : $url;
    }

    /**
     * post 动态表单令牌
     */
    public static function postToken ()
    {
        return SecureEntry\Post::token();
    }

    /**
     * 输入参数过滤
     */
    public static function input ($controller)
    {
        return self::isProtected(self::$route) ? SecureEntry\Input::check($controller) : true;
    }
    
    // 检测入口是否在保护范围内
    public static function isProtected ($route = '')
    {
        if (! $route)
        {
            return false;
        }
        
        if (! isset(self::$entrys[$route]))
        {
            self::$entrys[$route] = false;
            is_array(self::$config['SECURE_ENTRY']) or self::$config['SECURE_ENTRY'] = [];
            foreach (self::$config['SECURE_ENTRY'] as $_v)
            {
                if (preg_match("#^{$_v}$#i", $route))
                {
                    self::$entrys[$route] = true;
                    break;
                }
            }
        }
        return self::$entrys[$route];
    }
}