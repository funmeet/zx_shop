<?php
// +-------------------------------------------------------------------
// | 
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Kcdns\Service\Util;

class Seo
{

    protected static $title = array();

    protected static $keywords = array();

    protected static $description = '';

    public static function set ($key, $value = null)
    {
        switch ($key)
        {
            case 'title':
                $value and array_unshift(self::$title, $value);
                break;
            case 'keywords':
                $value and array_push(self::$keywords, $value);
                break;
            case 'description':
                $value and self::$description = $value;
                break;
            default:
                self::set('title', $key);
        }
    }

    public static function getHtml ()
    {
        $title = implode('_', self::$title);
        $keywords = implode(',', self::$keywords);
        $description = self::$description;
        return <<<EOF
        <title>{$title}</title>
        <meta name="keywords" content="{$keywords}"/>
        <meta name="description" content="{$description}"/>
EOF;
    }
}