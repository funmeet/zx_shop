<?php
// +-------------------------------------------------------------------
// | 
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Kcdns\Service\Util;

class Random
{

    /**
     * 生成随机字符串
     *
     * @param number $length            
     * @param number $type
     *            1-数字;2-数字加大写字母;
     * @param string $prefix            
     * @param string $suffix            
     */
    public static function string ($length = 8, $type = 1, $prefix = '', $suffix = '')
    {
        $randString = '';
        $length = $length - strlen($prefix . $suffix);
        for ($i = 0; $i < $length; $i ++)
        {
            $rand = mt_rand(1, 99999);
            if ($type == 1)
            {
                $randString .= $rand % 10;
            }
            elseif ($type == 2)
            {
                $rand = $rand % 36 + 48;
                $rand >= 58 and $rand <= 64 and $rand += 7;
                $randString .= chr($rand);
            }
        }
        return $prefix . $randString . $suffix;
    }
}