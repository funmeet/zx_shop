<?php
namespace Kcdns\Service\Util;

class Image
{

    protected $picDir = 'Uploads/defaultPic';

    protected $nopicPath = 'Public/blog/images/nopic.png';
    
    // 当图片文件大小超过此值时, 压缩图片, 单位 KB
    private $compressLimit = 250;

    public function getCover ($cover_id, $width, $height, $attributes, $imageTag)
    {
        $src = $this->getSrc($cover_id, $width, $height, $attributes);
        return $imageTag ? '<img width="' . $width . '" height="' . $height . '" src="' . $src . '" ' . $attributes . ' />' : $src;
    }

    public function getSrc ($cover_id, $width, $height, $attributes)
    {
        $picture = M('Picture')->where(array(
                'status' => 1
        ))->getById($cover_id);
        $file_path = ltrim($picture['path'], '/');
        
        // 只传宽，没传高，高按原图比例计算
        if ($width && ! $height)
        {
            $info = getimagesize($file_path);
            $real_w = $info[0];
            $real_h = $info[1];
            $height = floor(($width * $real_h) / $real_w);
        }
        
        $size = filesize($file_path) / 1024; // kb
        if ($size < $compressLimit)
        {
            $src = __ROOT__ . $picture['path'];
            return '<img width="' . $width . '" height="' . $height . '" src="' . $src . '" ' . $attributes . ' />';
        }
        
        $src = is_numeric($cover_id) ? get_cover($cover_id, 'path', $width, $height) : $cover_id;
        (! $src or ! file_exists(ltrim($src, '/'))) and $src = $this->_getDefaultPic($width, $height);
        
        return $src;
    }

    protected function _getDefaultPic ($width, $height)
    {
        file_exists($this->picDir) or mkdir($this->picDir, 0777);
        $filename = $this->picDir . '/default_' . $width . '_' . $height . '.png';
        file_exists($filename) or $this->_createDefaultPic($filename, $width, $height);
        return '/' . $filename;
    }

    protected function _createDefaultPic ($filename, $width, $height)
    {
        $nopicPath = $this->nopicPath;
        $img2Size = getimagesize($nopicPath);
        
        // 原水印图片宽高
        $src_w = $img2Size[0];
        $src_h = $img2Size[1];
        
        if ($width < $src_w || $height < $src_h)
        {
            $temp = $width / $height; // 要生成图片的宽高比
            $temp2 = $src_w / $src_h; // 原水印图片的宽高比
            if ($temp > $temp2)
            {
                // 横条图片
                $src_w = $height * $temp2;
                $src_h = $height;
            }
            else
            {
                // 竖条图片
                $src_h = $width / $temp2;
                $src_w = $width;
            }
            $nopicPath = $this->_createNewNopic(ceil($src_w), ceil($src_h));
        }
        
        $dst_x = $width < $src_w ? 0 : floor(($width - $src_w) / 2);
        $dst_y = $height < $src_h ? 0 : floor(($height - $src_h) / 2);
        
        $img = imagecreatetruecolor($width, $height);
        $color = imagecolorallocate($img, 245, 245, 245);
        // imagecolortransparent($img, $color);
        imagefill($img, 0, 0, $color);
        $img2 = imagecreatefrompng($nopicPath);
        imagecolortransparent($img2, $color);
        imagefill($img2, 0, 0, $color);
        
        imagecopy($img, $img2, $dst_x, $dst_y, 0, 0, $src_w, $src_h);
        
        imagepng($img, $filename);
        imagedestroy($img);
    }
    
    // 生成新的水印图片
    protected function _createNewNopic ($width, $height)
    {
        $image = new \Think\Image();
        $image->open($this->nopicPath);
        $newName = $this->picDir . '/nopic_' . $width . '_' . $height . '.png';
        $image->thumb($width, $height)->save($newName, 'png');
        return $newName;
    }
}