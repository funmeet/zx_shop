<?php
// +-------------------------------------------------------------------
// | 
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Kcdns\Service\Site;

/**
 * 获取前台广告模块数据
 *
 * @package Service\Site
 *         
 */
class Site
{

    static $instace;
    
    private $queryTokenFlag="_token";
    

    public static function getInstance ()
    {
        self::$instace or self::$instace = new self();
        return self::$instace;
    }

    
    /*
     * 获取导航
     */
    public function getChannel(){
        $model=M('channel');
        $channelList=$model->where(array('status'=>1))->order('sort asc')->select();
        foreach($channelList as $k=>$v){
            $link=$v['url']?U(str_replace('*','',$v['url'])):'javascript:void(0);';
            $target=$v['target']?"target=_blank":"";
            $channelList[$k]['url_label']="<a href='{$link}' {$target} data-patten='{$v['url']}'>{$v['title']}</a>";
            $channelList[$k]['url_patt']=$v['url'];
        }
        return $channelList;
    }
    
    /*
     * 获取LOGO
     */
    public function getLogo($width,$height){
        return o('Util')->getSrc($this->getSetting('SITE_LOGO'),$width,$height);
    }
    
    /*
     * 获取站点设置
     */
    public function getSetting($name){
        return C($name);
    }
    
    /*
     * 获取文案配置
     */
    public function getContent($name){
        $key='WEB_CONTENT_'.$name;
        return C($key);
    }
    
    /*
     * 文章别名检测
     */
    public function checkDocumentUrl($uri){
        // 文章别名处理
        if ($uri)
        {
            return  M('Document')->where(array(
                    'url' => $uri,
                    'status'=>array('egt',1),
            ))->order('id desc')->field('id,uuid')->find();
        }
        return false;
    }
    /*
     * 分类别名检测
     */
    public function checkCategoryUrl($uri){
        if(!$uri)return false;
        $cate_alias=S('cate_alias');
        if(!$cate_alias){
           //生成分类别名缓存
           $list=(array)M('category')->field('name,cate_alias')->where(array(
               'cate_alias' => array(
                   array('neq',''),
                   array('exp','is not null')
               ),
           ))->order('id asc')->select();
           $cate_alias=array();
           foreach($list as $row){
               $arr=  array_map('trim', explode("\n",$row['cate_alias']));
               $arr=  array_filter($arr);
               foreach($arr as $url){
                   $url=  strtolower($url);
                   $url=  str_ireplace(array('.html','.htm'), '', $url);
                   $cate_alias[$url]=$row['name'];
                   $cate_alias[$url.'.htm']=$row['name'];
                   $cate_alias[$url.'.html']=$row['name'];
               }
           }
           S('cate_alias',$cate_alias);
        }
        return $cate_alias[$uri];
    }
    
    /*
     * 生成站点异步请求token
     */
    public function createSiteToken(){
        
        $key="__TOKEN__";
        if(cookie($key)){
            return true;
        }
        
        $str = null;
        $strPol = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz123456789+-*/?;.!";
        $max = strlen($strPol)-1;

        for($i=0;$i<10;$i++){
            $str.=$strPol[rand(0,$max)];//rand($min,$max)生成介于min和max两个数之间的一个随机整数
        }

        $token= md5($str.time());
        
        cookie($key,$token);
        
        return true;
    }
    
    /*
     * 检测token是否合法
     */
    public function checkSiteToken(){
        if(cookie('__TOKEN__')){
            return I($this->queryTokenFlag)==cookie('__TOKEN__');
        }
        return true;
    }
    
    /*
     * 获取下载中心列表
     */
    public function getDownloadList($page=1,$pageSize=10){
        $downloadCate=M('Category')->where(array('name'=>'download'))->cache()->getField('id');
        $where=array(
            'category_id'=>$downloadCate,
            'status'=>array('egt',1)
        );
        $order="level desc,create_time desc";
        $field="title,file_id,file_sn,create_time";
        $articleList=M('Document')->join('left join __DOCUMENT_DOWNLOAD__ on __DOCUMENT__.id=__DOCUMENT_DOWNLOAD__.id')
                                    ->field($field)
                                    ->where($where)
                                    ->page($page,$pageSize)
                                    ->order($order)
                                    ->select();
        
        $count=M('Document')->where($where)->count();
        
        return array(
            'count'=>$count,
            'list'=>$articleList
        );
    }
}