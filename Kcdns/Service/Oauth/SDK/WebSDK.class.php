<?php
// +----------------------------------------------------------------------
// | Author: zj <zhoujian@kcdns.com> <http://www.kcdns.com>
// +----------------------------------------------------------------------
// | WebSDK.class.php 2016-09-25
// +----------------------------------------------------------------------
namespace Kcdns\Service\Oauth\SDK;
use Kcdns\Service\Oauth\OauthSDK;
class WebSDK extends OauthSDK{

    /**
	 * 请求code
	 */
	public function getRequestCodeURL($callback){
		$pos= (strpos($callback, '?')===false)?'?':'&';
		$callback .= ($pos . 'code='.  uniqid()) ;
        return $callback;
	}

	/**
	 * 组装接口调用参数 并调用接口
	 * @param  string $api    微信API
	 * @param  string $param  调用API的额外参数
	 * @param  string $method HTTP请求方法 默认为GET
	 * @return json
	 */
	public function call($api, $param = '', $method = 'GET', $multi = false){

	}

	/**
	 * 解析access_token方法请求后的返回值
	 * @param string $result 获取access_token的方法的返回值
	 */
	protected function parseToken($result, $extend){

	}

	/**
	 * 获取当前授权应用的openid
	 * @return string
	 */
	public function openid(){
		$data = $this->Token;
		return $data['openid'];
	}

        /**
	 * 获取access_token
	 * @param string $code 上一步请求到的code
	 */
	public function getAccessToken($code, $extend = null){
        $auth_openid=cookie('auth_openid');
        if(empty($auth_openid)){
             $auth_openid= uniqid(mt_rand(), true);
             cookie('auth_openid',$auth_openid,30*24*3600);
        }

		$this->Token = array(
                    'openid' => $auth_openid,
					'access_token' => md5($auth_openid)
                );
		return $this->Token;
	}

	public function getUserInfo()
    {
        $userInfo['type'] = 'web';
        $userInfo['name'] = uniqid();
        $userInfo['nickname'] = $userInfo['name'];
        $userInfo['head'] = '/Public/Wap/images/default_avatar.png';
        return $userInfo;    }

}
