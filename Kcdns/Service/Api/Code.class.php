<?php
// +-------------------------------------------------------------------
// | 
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Kcdns\Service\Api;

class Code
{

    public static $V1 = [
            'ok' => [
                    0,
                    ''
            ],
            // 系统错误 10000+
            'default_error' => [
                    10000,
                    '系统异常'
            ],
            'invaid_action' => [
                    10001,
                    '操作无效'
            ],
            'invalid_param' => [
                    10002,
                    '参数无效 : %s'
            ],
            'faild' => [
                    10003,
                    '操作失败'
            ],
            // 用户相关 20000+
            'invalid_sms_code' => [
                    20001,
                    '验证码错误'
            ],
            'not_login' => [
                    20004,
                    '用户未登录'
            ]
    ];

}