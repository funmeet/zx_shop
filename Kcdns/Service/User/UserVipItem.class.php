<?php
// +-------------------------------------------------------------------
// | 
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Kcdns\Service\User;

use Think\Model;

class UserVipItem extends Model
{

    static $instace;

    protected $name = 'user_vip_item';

    public static function getInstance()
    {
        return self::$instace ?: (self::$instace = new self());
    }

    // 获取会员信息
    public function userVipInfo($uid, $type = 1)
    {
        if ($type == 3) {
            return M('UserVipItem')->where([
                'uid' => $uid,
                'status' => 1
            ])->getField('type,id,uid,company_name,nickname,content,create_time');
        } else {
            return M('UserVipItem')->where([
                'uid' => $uid,
                'type' => $type,
                'status' => 1
            ])->find();
        }
    }

    // 修改会员信息
    public function userVipEdit($id, $data)
    {
        filter($data, [
            'company_name' => '',
            'nickname' => '',
            'content' => ''
        ]);

        $info = M("UserVipItem")->where(['id' => $id, 'status' => 1])->find();
        // 查询是否为企业会员
        $vipItem = M('UserVip')->where(['uid' => $info['uid'], 'type' => 2, 'status' => 1])->find();
        if (!empty($vipItem)) {
            // 是否赠送过
            $vipRoom = M("UserVipRoom")->where(['suid' => $vipItem['muid'], 'uid' => $info['uid'], 'type' => 1])->count();
            if ($vipRoom <= 0) {
                $vipName = M('UserVipItem')->where(['uid' => $vipItem['muid'], 'type' => 2])->getField('company_name');
                if ($vipName == $data['company_name']) {
                    O('User')->addVipRoomLog($info['uid'], $vipItem['muid'], 1);
                    O('Coupon')->addUserCoupon(1, $vipItem['muid'], 1, "修改个人用户信息,增加1小时");
                }
            }
        }
        $data['id'] = $id;
        return M('UserVipItem')->save($data);
    }

    // 修改会员基本信息
}