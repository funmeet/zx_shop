<?php
// +-------------------------------------------------------------------
// | 
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Kcdns\Service\User;

use Think\Model;

class UserVip extends Model
{

    static $instace;

    protected $name = 'user_vip';

    public static function getInstance()
    {
        return self::$instace ?: (self::$instace = new self());
    }

    public function companyList($uid, $muid, $company)
    {
        filter($uid, 'required;int;');
        filter($muid, 'int;');
        if (empty($muid)) {
            $muid = M('UserVip')->where(['status' => 1, 'uid' => $uid, 'type' => 2])->getField('muid');
            if (empty($muid)) {
                return [];
            }
            $company = UserVipItem::getInstance()->userVipInfo($muid, 2);
        }

        $list = M('UserVip')->where(['status' => 1, 'muid' => $muid])->select();
        if ($list) {
            $userArr = [];
            foreach ($list as $k => &$v) {
                $userArr[$v['uid']] = $v['uid'];
            }

            // 获取用户信息
            $userList = User::getInstance()->getInList($userArr);

            foreach ($list as $k => &$v) {
                $v['company_name'] = $company['company_name'];
                $v['mobile']       = $userList[$v['uid']]['mobile'] ?: '';
            }
        }

        return $list;
    }

    public function getInList($uidIn, $muid)
    {
        return $this->where([])->getField();
    }

}