<?php
// +-------------------------------------------------------------------
// | 
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Kcdns\Service\User;

use Think\Model;

class UserVipRoom extends Model
{

    static $instace;

    protected $name = 'user_vip_room';

    public static function getInstance()
    {
        return self::$instace ?: (self::$instace = new self());
    }

    // 会议室赠送时间记录
    public function addVipRoomLog($uid, $suid, $type, $time_long)
    {
        $time_long or $time_long = 1;
        return M("UserVipRoom")->add([
            'suid' => $suid,
            'uid' => $uid,
            'type' => $type,
            'time_long' => $time_long,
            'create_time' => date('Y-m-d H:i:s')
        ]);
    }

}