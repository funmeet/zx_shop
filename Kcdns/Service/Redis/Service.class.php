<?php
// +-------------------------------------------------------------------
// | KCDNS 开创网络科技
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 http://www.kcdns.com All rights reserved.
// +-------------------------------------------------------------------
namespace Kcdns\Service\Redis;

/**
 * O('Redis')->getInstance()->set('name','test');
 * var_dump(O('Redis')->getInstance()->get('name'));
 */
class Service
{
    static $instance;

    public function __call($name, $arguments)
    {
        $this->getInstance()->$name($arguments);
    }

    public function getInstance()
    {
        if (self::$instance) {
            return self::$instance;
        }

        $redis = new \Redis();

        $result = $redis->connect(REDIS_HOST, REDIS_PORT);
        if (!$result) {
            throw new \Exception('redis连接失败');
        }

        $result = $redis->auth(REDIS_PASSWORD);
        if (!$result) {
            throw new \Exception('redis验证失败');
        }

        self::$instance = $redis;
        return self::$instance;
    }
}
