<?php

namespace Think\Pay\Driver;

class Shengquery extends \Think\Pay\Pay {

	protected $gateway='http://tradeexprod.shengpay.com/fexchange-web/rest/merchant/queryExchangeRate';
    protected $config = array(
        'key' => '',
        'partner' => ''
    );

    public function check() {
        if (!$this->config['key'] || !$this->config['partner']) {
            E("盛付通设置有误！");
        }
        return true;
    }

    public function buildRequestForm(\Think\Pay\PayVo $vo) {
		return true;
    }
	
    //以下为汇率查询接口
	public function getExchangeRate($currency){
		$params=array(
			'merchantId' => $this->config['partner'],
			'charset' => 'UTF-8',
			'version' => '1.0',
			'foreignCurrency' => $currency,
			'homeCurrency' => 'CNY',
		);
		
		$params=array_filter($params);
		ksort($params);
		
		$params['signMessage']=$this->createSign($params);
		$url=$this->gateway.'?'.urldecode(http_build_query($params));
		$data=$this->fsockOpen($url,0);
		
		return $data;
	}
	
	public function verifyNotify($json_str){
		//提取服务器端的签名
        if (!isset($GLOBALS['filesockheader'])) {
            return false;
        }
		
		$pos=strpos($GLOBALS['filesockheader'],'signMessage:')+strlen('signMessage:')+1;
		$sign=substr($GLOBALS['filesockheader'],$pos,32);
		$mysign=strtoupper(md5($json_str . '123456'));//$this->config['key']));
		
		if ($sign != $mysign) {
            return false;
        }else{
			return true;
		}
	}
	
	//请求签名
	public function createSign($params){
		$str='';
		foreach($params as $key => $val){
			$str.=$key.'='.$val;
		}
		return strtoupper(md5($str . '123456'));//$this->config['key']));
	}
}
