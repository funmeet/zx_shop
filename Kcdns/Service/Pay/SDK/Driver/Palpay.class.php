<?php

namespace Kcdns\Service\Pay\SDK\Driver;


use Kcdns\Service\Pay\SDK\Pay;

class Palpay extends Pay {

    protected $gateway = 'https://www.paypal.com/cgi-bin/webscr';
    protected $config = array(
        'business' => ''
    );

    public function check() {
        if (!$this->config['business']) {
            E("贝宝设置有误！");
        }
        return true;
    }

    public function buildRequestForm($paymentInfo=array()) {
        $orderInfo = unserialize($paymentInfo['order_info']);
        $param = array(
            'cmd' => '_xclick',
            'charset' => 'utf-8',
            'business' => $this->config['business'],
            'currency_code' => 'USD',
            'notify_url' => $paymentInfo['notify'],
            'return' => $paymentInfo['return'],
            'invoice' => $paymentInfo['order_no'],
            'item_name' => $orderInfo['title']?:'',
            'amount' => $paymentInfo['amount'],
            'no_note' => 1,
            'no_shipping' => 1
        );
        $sHtml = $this->_buildForm($param, $this->gateway);

        return $sHtml;
    }

    public function verifyNotify($notify) {
        if (empty($notify['txn_id']))
            return false;
        $tmpAr = array_merge($notify, array("cmd" => "_notify-validate"));

        $ppResponseAr = $this->fsockOpen($this->gateway, 0, $tmpAr);
        if ((strcmp($ppResponseAr, "VERIFIED") == 0) && $notify['receiver_email'] == $this->config['business']) {
            $info = array();
            //支付状态
            $info['status'] = $notify['payment_status'] == 'Completed' ? true : false;
            $info['money'] = $notify['mc_gross'];
            $info['out_trade_no'] = $notify['invoice'];
            $this->info = $info;
            return true;
        }
        return false;
    }

}
