<?php

namespace Kcdns\Service\Pay\SDK\Driver;

class Wxh5 extends Weixin
{
    /**
     * 微信统一下单
     */
    public function unifiedOrder($paymentInfo = [])
    {
        $orderInfo = unserialize($paymentInfo['order_info']);
        $param['body'] = $orderInfo ? $orderInfo['desc'] : "";
        $param['out_trade_no'] = $paymentInfo['pay_sn'];
        $param['total_fee'] = $paymentInfo['amount'] * 100;
        $param['time_start'] = date('YmdHis');
        $param['time_expire'] = date('YmdHis', time() + 600);
        $param['notify_url'] = $paymentInfo['notify'];
        $param['trade_type'] = 'MWEB';
        $param['appid'] = $this->config['appid'];
        $param['mch_id'] = $this->config['mchid'];
        $param['spbill_create_ip'] = $_SERVER['HTTP_X_FORWARDED_FOR_POUND'] ?: $_SERVER['REMOTE_ADDR'];
        $param['nonce_str'] = md5(rand(100, 999));
        $param['scene_info'] = '{"h5_info": {"type":"Wap","wap_url": "http://' . $_SERVER['HTTP_HOST'] . '","wap_name": "' . $_SERVER['HTTP_HOST'] . '"}}';
        $param['sign'] = $this->makeSign($param);

        try {
            $response = self::getResponse($this->apiurl_unifiedorder, $param);
            //预下单信息存储
            $this->_savePrePayInfo($paymentInfo['payment_id'], $param, $response);
            return $response;
        } catch (\Exception $e) {
            $this->_savePrePayInfo($paymentInfo['payment_id'], $param, $response, $e->getMessage());
            throw new \Exception($e->getMessage());
        }
    }

    /**
     * 建立提交表单（JsAPI）
     */
    public function buildRequestForm($paymentInfo = array())
    {
        //微信支付没有同步通知 此处伪造同步回调数据  注意伪造同步回调的数据中 transaction_id为假数据
        $info['return_code'] = 'SUCCESS';
        $info['return_msg'] = 'SUCCESS';
        $info['result_code'] = 'SUCCESS';
        $info['total_fee'] = $paymentInfo['amount'];
        $info['out_trade_no'] = $paymentInfo['order_no'];
        $info['transaction_id'] = time();
        $info['sign'] = $this->makeSign($info);
        $result = $this->unifiedOrder($paymentInfo);
        return $result['mweb_url'];
    }
}