<?php

namespace Kcdns\Service\Pay\SDK\Driver;


use Kcdns\Service\Pay\SDK\Pay;

class Kuaiqian extends Pay {

    protected $gateway = 'https://www.99bill.com/gateway/recvMerchantInfoAction.htm';
    protected $config = array(
        'key' => '',
        'partner' => ''
    );

    public function check() {
        if (!$this->config['key'] || !$this->config['partner']) {
            E("快钱设置有误！");
        }
        return true;
    }

    public function buildRequestForm($paymentInfo=array()) {
        $orderInfo = unserialize($paymentInfo['order_info']);
        $param = array(
            'inputCharset' => '1',
            'pageUrl' => $paymentInfo['return'],
            'bgUrl' => $paymentInfo['notify'],
            'version' => 'v2.0',
            'language' => 1,
            'signType' => 1,
            'merchantAcctId' => $this->config['partner'],
            'orderId' => $paymentInfo['orderNo'],
            'orderAmount' => $paymentInfo['amount'] * 100,
            'orderTime' => date("Ymdhis"),
            'productName' => $orderInfo['title']?:'',
            'productDesc' => $orderInfo['desc']?:"",
            'payType' => '00'
        );

        $param['signMsg'] = $this->createSign($param);

        $sHtml = $this->_buildForm($param, $this->gateway);

        return $sHtml;
    }

    protected function createSign($params) {
        $arg = '';
        foreach ($params as $key => $value) {
            if ($value != "") {
                $arg .= "{$key}={$value}&";
            }
        }
        return strtoupper(md5($arg . 'key=' . $this->config['key']));
    }

    public function verifyNotify($notify) {
        $param = array(
            'merchantAcctId' => $notify['merchantAcctId'],
            'version' => $notify['version'],
            'language' => $notify['language'],
            'signType' => $notify['signType'],
            'payType' => $notify['payType'],
            'bankId' => $notify['bankId'],
            'orderId' => $notify['orderId'],
            'orderTime' => $notify['orderTime'],
            'orderAmount' => $notify['orderAmount'],
            'dealId' => $notify['dealId'],
            'bankDealId' => $notify['bankDealId'],
            'dealTime' => $notify['dealTime'],
            'payAmount' => $notify['payAmount'],
            'fee' => $notify['fee'],
            'payResult' => $notify['payResult'],
            'errCode' => $notify['errCode']
        );

        if ($notify['signMsg'] == $this->createSign($param)) {
            $info = array();
            //支付状态
            $info['status'] = $notify['payResult'] == '10' ? true : false;
            $info['money'] = $notify['orderAmount'];
            $info['out_trade_no'] = $notify['orderId'];
            $this->info = $info;
            return true;
        } else {
            return false;
        }
    }

    public function notifySuccess() {
        echo "<result>1</result><redirecturl>{$this->config['return_url']}</redirecturl>";
    }

}
