<?php

namespace Kcdns\Service\Pay\SDK\Driver;

use Think\Log;

class Wxjs extends Weixin
{
    public function unifiedOrder($paymentInfo = array())
    {
        $orderInfo = unserialize($paymentInfo['order_info']);
        $param['body'] = $orderInfo ? $orderInfo['desc'] : "";
        $param['out_trade_no'] = $paymentInfo['pay_sn'];
        $param['total_fee'] = $paymentInfo['amount'] * 100;
        $param['time_start'] = date('YmdHis');
        $param['time_expire'] = date('YmdHis', time() + 600);
        $param['notify_url'] = $paymentInfo['notify'];
        $param['trade_type'] = 'JSAPI';
        $param['openid'] = $this->config['extra']['openid'];
        $param['appid'] = $this->config['appid'];
        $param['mch_id'] = $this->config['mchid'];
//        $param['spbill_create_ip'] = $_SERVER['HTTP_X_FORWARDED_FOR_POUND'] ?: $_SERVER['REMOTE_ADDR'];
        $param['spbill_create_ip'] = $_SERVER['REMOTE_ADDR'];
        $param['nonce_str'] = md5(rand(100, 999));
        $param['sign'] = $this->makeSign($param);
        Log::w_log("pram:" .json_encode($param));
        try {
            $response = self::getResponse($this->apiurl_unifiedorder, $param);
            //预下单信息存储
            $this->_savePrePayInfo($paymentInfo['payment_id'], $param, $response);
            return $response;
        } catch (\Exception $e) {
            $this->_savePrePayInfo($paymentInfo['payment_id'], $param, $response, $e->getMessage());
            throw new \Exception($e->getMessage());
        }
    }

    /**
     * 建立提交表单（JsAPI）
     */
    public function buildRequestForm($paymentInfo = array())
    {
        //微信支付没有同步通知 此处伪造同步回调数据  注意伪造同步回调的数据中 transaction_id为假数据
        $info['return_code'] = 'SUCCESS';
        $info['return_msg'] = 'SUCCESS';
        $info['result_code'] = 'SUCCESS';
        $info['total_fee'] = $paymentInfo['amount'];
        $info['out_trade_no'] = $paymentInfo['order_no'];
        $info['transaction_id'] = time();
        $info['sign'] = $this->makeSign($info);
        $href = $paymentInfo['return'];
        $href .= '?' . $this->toUrlParams($info) . '&sign=' . $info['sign'];

        $data = $this->unifiedOrder($paymentInfo);

        $timeStamp = time();
        $jsapi['appId'] = $data['appid'];
        $jsapi['timeStamp'] = "$timeStamp";
        $jsapi['nonceStr'] = md5(rand(100, 999));
        $jsapi['package'] = "prepay_id={$data['prepay_id']}";
        $jsapi['signType'] = 'MD5';
        $jsapi['paySign'] = $this->makeSign($jsapi);
        return $jsapi;

        $json = json_encode($jsapi);
        return "<script type=\"text/javascript\">function jsApiCall() {WeixinJSBridge.invoke('getBrandWCPayRequest',$json, function(res) {if (res.err_msg == 'get_brand_wcpay_request:ok') {window.location.href = '$href';}else if (res.err_msg == 'get_brand_wcpay_request:cancel') {window.history.go(-1);} else {alert(res.err_msg);}});}function callpay() {if (typeof WeixinJSBridge == \"undefined\") {if (document.addEventListener) {document.addEventListener('WeixinJSBridgeReady', jsApiCall,false);} else if (document.attachEvent) {document.attachEvent('WeixinJSBridgeReady', jsApiCall);}} else {jsApiCall();}}callpay();</script>";
    }
}