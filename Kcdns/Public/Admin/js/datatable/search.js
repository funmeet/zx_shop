var _datatable_search_ = (function() {

    var datatableSearch = function(o, dt, box, grids) {
        this.dt = dt;
        this.box = box;
        this.grids = grids;
        this.showSearchBar = false; // 搜索栏状态
        this.tableEle = o._tableEle;
        this.btn = this.box.find('.btn-filter');
        this.init();
    };

    datatableSearch.prototype = {

        init: function(o, dt, box, grids) {
            this._createHtml();
            this._setSearchBar(this.showSearchBar);
            this._bindEvents();
        },

        //生成搜索行
        _createHtml: function() {

            var value, classname, grid, input, html, tvalue, tinput;
            var columns = this.dt.state().columns;

            // 第一列是 checkbox
            html = '<td></td>';

            for (var i = 1; i < columns.length; i++) {
                classname = columns[i].visible ? "" : "cell_hide";
                grid = this.grids[i - 1];
                tinput = [];

                if(grid.searchable){
                    // 取得上次搜索状态
                    value = $.trim(columns[i].search.search);
                    input = $.AdminLTE.formInput.appendInput(grid['filter'], value, grid['enum'], i);
                }else{
                    input = '';
                }
                html += '<td class="' + classname + '" data-index="' + i + '">' + input + '</td>';

                // 列可见并且有搜索条件时,显示搜索栏
                if (grid.searchable && columns[i].visible && value !== '' && value !== undefined) {
                    this.showSearchBar = true;
                }
            }
            this.tableEle.children('thead').append("<tr role='row' class='search_row'>" + html + "</tr>");

            //往底部填充table加搜索行
            this.tableEle.parents(".table-wrap").find(".style-table>table").children('thead').append("<tr role='row' class='search_row'>" + html + "</tr>");
        },

        _bindEvents: function() {

            var context = this;

            //Date picker
            this.box.find('.list_filter_timerange,.list_filter_time').datepicker({
                language: 'zh-CN',
                autoclose: true,
                format: "yyyy-mm-dd",
            });

            // 输入框值发生改变时出发回调，更新数据
            this.tableEle.find('.search_row').find('input,select').change(function() {
                context._changeCallback();
            });

            // 搜索按钮
            this.btn.on("click", function() {
                context._setSearchBar(!context.showSearchBar);
            });
        },

        // 输入框值发生变化时回调
        _changeCallback: function() {

            var columns = this.dt.state().columns;
            var grid, value, v;

            for (var i = 1; i < columns.length; i++) {
                grid = this.grids[i - 1];
                value = [];

                // 普通文本框只有一个值，时间区间有两个值
                this.box.find('.search_row td').eq(i).find('input').each(function() {
                    v = $.trim($(this).val());
                    v!=='' && value.push(v);
                });
                //下拉选框
                this.box.find('.search_row td').eq(i).find('select').each(function() {
                    v = $.trim($(this).val());
                    v!=='' && value.push(v);
                });

                // 设置搜索项，加载数据
                this.dt.columns(i).search(value);
            }

            // 保存当前状态，默认重新加载数据后保存，会导致搜索数据应用延迟
            this.dt.state.save();
            // 重新加载数据
            this.dt.draw();
        },

        // 设置搜索栏状态
        _setSearchBar: function(showSearchBar) {
            var btnclass = 'active';
            var trclass = 'row_hide';
            var doChangeCallback=false;
            var itemSearchVal="";
            this.showSearchBar = showSearchBar;
            if (this.showSearchBar) {
                this.btn.text('取消搜索').addClass(btnclass);
                this.box.find('.search_row').removeClass(trclass);
                this.box.parents(".table-wrap").find(".style-table .search_row").removeClass(trclass);
            } else {
                this.btn.text('搜索').removeClass(btnclass);
                this.box.find('.search_row').addClass(trclass);
                this.box.parents(".table-wrap").find(".style-table .search_row").addClass(trclass);
                // 清除搜索值 如果之前有搜索条件 取消搜索后 需要手动触发搜索框值发生变化 重新发送数据请求
                for(var i=0;i<this.box.find(".search_row").find(".form-control").length;i++){
                    itemSearchVal=this.box.find(".search_row").find(".form-control").eq(i).val();
                    if(itemSearchVal&&itemSearchVal!='__NO_CHOOSE__'){
                        doChangeCallback=true;
                    }
                }
                this.box.find(".search_row").find(".form-control").val("");
                this.box.find(".search_row").val("--请选择--");
                if(doChangeCallback)this._changeCallback();
            }
        }
    };

    return function(o, dt, box, grids) {
        return new datatableSearch(o, dt, box, grids);
    }
})();
