var _datatable_search_box_ = (function () {

    var datatableSearch = function (o, dt, box, grids) {
        this.dt = dt;
        this.box = box;
        this.grids = grids;
        this.showSearchBar = false; // 搜索栏状态
        this.tableEle = o._tableEle;
        this.btn = this.box.find('.btn-filter');
        this.init();
    };

    datatableSearch.prototype = {

        init: function (o, dt, box, grids) {
            this._createHtml();
            this._setSearchBar(this.showSearchBar);
            this._bindEvents();
        },

        //生成搜索行
        _createHtml: function () {

            var value, classname, grid, input, html, tvalue, tinput, cls;
            var columns = this.dt.state().columns;
            var css = "margin-bottom:10px;";
            // 大屏使用 4 列布局, 小屏使用 3 列布局
            var col =  document.body.clientWidth>1440 ? 2 : 3;

            this.box = $('<div class="box box-primary form-inline flat databox-search-box"></div>');

            html = '<div class="box-header with-border"> <h3 class="box-title">高级搜索</h3> </div>';
            html += '<div class="box-body row">';
            // 第一列是 checkbox
            for (var i = 1; i < columns.length; i++) {
                grid = this.grids[i - 1];
                if (!grid.searchable || i == 1) {
                    // continue;
                }

                // 取得上次搜索状态
                value = $.trim(columns[i].search.search);
                cls = 'col-md-' + col + ' input-box input-box-' + i;
                input = $.AdminLTE.formInput.appendInput(grid['filter'], value, grid['enum'], i, { 'boxClass': cls, 'inputClass': 'input-sm' });
                html += '<label class="control-label col-md-1" style="text-align:right;padding:0;">' + grid.title + '</label>' + input + '';

                // 列可见并且有搜索条件时,显示搜索栏
                if (grid.searchable && columns[i].visible && value !== '' && value!='__NO_CHOOSE__' && value !== undefined) {
                    this.showSearchBar = true;
                }
            }
            html += "</div>";

            this.showSearchBar || this.box.hide();
            this.box.html(html);
            this.tableEle.closest('.box').before(this.box);
            this.boxHeight = this.box.height();
        },

        _bindEvents: function () {

            var context = this;

            //Date picker
            this.box.find('.list_filter_timerange,.list_filter_time').datepicker({
                language: 'zh-CN',
                autoclose: true,
                format: "yyyy-mm-dd",
            });

            // 输入框值发生改变时出发回调，更新数据
            this.box.find('input,select').change(function () {
                context._changeCallback();
            });

            // 搜索按钮
            this.btn.on("click", function () {
                context._setSearchBar(!context.showSearchBar);
            });
        },

        // 输入框值发生变化时回调
        _changeCallback: function () {

            var columns = this.dt.state().columns;
            var grid, value, v;

            for (var i = 1; i < columns.length; i++) {
                grid = this.grids[i - 1];
                value = [];

                // 普通文本框只有一个值，时间区间有两个值
                this.box.find('.input-box-' + i).find('input').each(function () {
                    v = $.trim($(this).val());
                    v !== '' && value.push(v);
                });
                //下拉选框
                this.box.find('.input-box-' + i).find('select').each(function () {
                    v = $.trim($(this).val());
                    v !== '' && value.push(v);
                });

                // 设置搜索项，加载数据
                this.dt.columns(i).search(value);
            }

            // 保存当前状态，默认重新加载数据后保存，会导致搜索数据应用延迟
            this.dt.state.save();
            // 重新加载数据
            this.dt.draw();
        },

        // 设置搜索栏状态
        _setSearchBar: function (showSearchBar) {
            var btnclass = 'active';
            var doChangeCallback = false;
            var itemSearchVal = "";
            var that = this;
            this.box.addClass("anim");
            this.showSearchBar = showSearchBar;
            if (this.showSearchBar) {
                this.btn.text('取消搜索').addClass(btnclass);
                //显示
                this.box.show().css("height",this.boxHeight).removeClass("searchHideStyle opaci");
            } else {
                this.btn.text('搜索').removeClass(btnclass);
                //隐藏
                this.box.outerHeight(0).addClass("searchHideStyle");
                setTimeout(function(){
                    that.box.addClass("opaci");
                },200);
                //this.box.slideUp(100);
                // 清除搜索值 如果之前有搜索条件 取消搜索后 需要手动触发搜索框值发生变化 重新发送数据请求 input-box
                var inputs = this.box.find(".form-control");
                for (var i = 0; i < inputs.length; i++) {
                    var ie = inputs.eq(i);
                    itemSearchVal = ie.val();
                    if (itemSearchVal && itemSearchVal != '__NO_CHOOSE__') {
                        doChangeCallback = true;
                    }
                    if (ie.is('select')) {
                        ie.val("--请选择--");
                    } else {
                        ie.val("");
                    }
                }
                if (doChangeCallback) this._changeCallback();
            }
        },

        // 获取搜索值
        getValue: function () {

        }
    };

    return function (o, dt, box, grids) {
        return new datatableSearch(o, dt, box, grids);
    }
})();
