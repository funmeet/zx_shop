var cfg_form = function (url,  title) {

    // jquery el
    var el = this;
    var ajaxLock = false;
    var modalEl = $('#datatable-popup');
    var boxEl = modalEl.find('#datatable-popup-body>.popup-block');
    var buttonEl = modalEl.find('.js-datatable-save');
    var closeEl = modalEl.find('.btn-close');
    var autoClose = false;
    var formEl;
    var popupId;
    
    buttonEl.show();

    // 载入表单
    $.ajax({
        'url': url,
        'data':{'id':title.replace('/','|')},
        'type': 'get',
        'success': function (html) {
            try {
                if (!html.status && html.info) {
                    popup.tip(html.info);
                    return false;
                }
            } catch (e) {

            }

            boxEl.html(html);
            popupId = popup.open({
                content: modalEl,
                area:[document.body.scrollWidth*0.9+'px','auto']
            });

            // 调整弹窗样式, 可能导致其他弹窗显示异常
            boxEl.css("overflow-y","auto");
            boxEl.css("overflow-x","hidden");

            modalEl.find('.popup-title>span').text(title);
            buttonEl.unbind().click(function(){
                boxEl.find('form').submit();
            });
        },
        'error': function () {
            popup.tip('系统异常');
        }
    });
};

var cfg_button = function(ModelCfgKey,url,el,uniq){
    //if(uniq){
        $('#model_cfg').remove();
    //}
	var html = '\
	    <div id="model_cfg" style="width: 300px;position:absolute; bottom:4px; right:300px; font-size:30px; cursor:pointer;z-index:9999992;">\
            <div style="float: left;" class="model_cfg_button">\
                <i class="fa fa-gear"></i> <span style="vertical-align: middle;font-size:14px;color:#000;">'+(ModelCfgKey||'')+'</span>\
            </div>\
            <div style="float: left;margin-left: 10px;" onclick="location.href=\'/kcadmin/model/cfgx\'">        \
                <i class="fa fa-gear"></i> <span style="vertical-align: middle;font-size:14px;color:#000;">all</span>    \
            </div>\
        </div>\
    ';
    var o = $(html);
    o.find('.model_cfg_button').click(function(){
        if(!ModelCfgKey){
            ModelCfgKey = prompt('新建 CFG MODEL');
        }
        if(ModelCfgKey){
            cfg_form(url,ModelCfgKey);
        }
    });
    el.append(o);
}