function CityPicker( config ){
    this.init( config )
}
CityPicker.prototype = {
    init: function( config ){
        this.readOnly = config.readOnly;
        this.defaultId = config.defaultId;
        this.defaultName = config.defaultName;
        this.level = config.level || 3;
        this.data = config.data;
        this.target = $( config.target );
        this.make(this.data);
        this.bind();
        if( this.defaultId ){
            var id = this.defaultId.split(","),
                prov = id[0],
                city = id[1],
                town = id[2];
            $( "select.prov option[value='"+prov+"']" ).attr("selected","true");
            $( "select.prov" ).trigger("change");
            $( "select.city option[value='"+city+"']" ).attr("selected","true");
            $( "select.city" ).trigger("change");
            $( "select.town option[value='"+town+"']" ).attr("selected","true");
            $( "select.town" ).trigger("change");
        }
    },
    //绑定事件
    bind: function(){
        var _this = this;
        $( ".prov" ).on( "change", function(){
            var id = $(this).val(),
                content = "<option value='请选择'>请选择</option>";
            for( var i = 0; i < _this.data.length; i ++ ){
                if( _this.data[i].id == id ){
                    var data = _this.data[i].child;
                    for( var k = 0; k < data.length; k ++ ){
                        content += "<option value="+data[k].id+">"+data[k].name+"</option>";
                    }
                    $( ".city" ).empty().append( $( content ) );
                    if( $( ".town option" ).length > 1 ){
                        $( ".town" ).empty().append( $( "<option value='请选择'>请选择</option>" ) )
                    }
                    break;
                }
            }
            $( "input[name="+_this.defaultName+"]" ).val( "" );
        } )
        $( ".city" ).on( "change", function(){
            var id = $(this).val(),
                pId = $( ".prov" ).find("option:selected").val(),
                content = "<option value='请选择'>请选择</option>";
            for( var i = 0; i < _this.data.length; i ++ ){
                if( _this.data[i].id == pId ){
                    for( var k = 0; k < _this.data[i].child.length; k ++ ){
                        if( _this.data[i].child[k].id == id ){
                            var data = _this.data[i].child[k].child;
                            for( var j = 0; j < data.length; j ++ ){
                                content += "<option value="+data[j].id+">"+data[j].name+"</option>";
                            }
                            $( ".town" ).empty().append( $( content ) );
                            break;
                        }
                    }
                }
            }
            $( "input[name="+_this.defaultName+"]" ).val( "" );
        } )
        $( ".town" ).on( "change", function(){
            var id = $(this).val(),
                pId = $( ".city" ).find("option:selected").val(),
                gId = $( ".prov" ).find("option:selected").val();
            $( "input[name="+_this.defaultName+"]" ).val( gId+","+pId+","+id );
        } )
    },
    //根据数据结构生成view
    make: function( data ){
        var content = "",
            prov;
        for( var i = 0; i < data.length; i ++ ){
            content += "<option value="+data[i].id+">"+data[i].name+"</option>"
        }
        var readonly=this.readOnly ? 'disabled="disabled"' : '';
        prov = "<div class='citypicker' style='position: absolute; z-index: 99999; top: 0; left: 0'><input type='hidden' name='"+this.defaultName+"' value='"+this.defaultId+"'/><select class='prov' "+readonly+"><option value='请选择'>请选择</option>"+content+"</select><select "+readonly+" class='city'><option value='请选择'>请选择</option></select><select "+readonly+" class='town'><option value='请选择'>请选择</option></select></div>"
        this.target.append($(prov))
    },
}