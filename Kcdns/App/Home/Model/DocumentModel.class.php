<?php
// +----------------------------------------------------------------------
// | OneThink [ WE CAN DO IT JUST THINK IT ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013 http://www.onethink.cn All rights reserved.
// +----------------------------------------------------------------------
// | Author: 麦当苗儿 <zuojiazi@vip.qq.com> <http://www.zjzit.cn>
// +----------------------------------------------------------------------

namespace Kcdns\Home\Model;
use Think\Model;
use Think\Page;

/**
 * 文档基础模型
 */
class DocumentModel extends Model{

	/* 自动验证规则 */
    protected $_validate = array(
        array('name', '/^[a-zA-Z]\w{0,39}$/', '文档标识不合法', self::VALUE_VALIDATE, 'regex', self::MODEL_BOTH),
        array('name', 'checkName', '标识已经存在', self::VALUE_VALIDATE, 'callback', self::MODEL_BOTH),
        array('title', 'require', '标题不能为空', self::MUST_VALIDATE, 'regex', self::MODEL_BOTH),
        array('title', '1,80', '标题长度不能超过80个字符', self::MUST_VALIDATE, 'length', self::MODEL_BOTH),
        array('level', '/^[\d]+$/', '优先级只能填正整数', self::VALUE_VALIDATE, 'regex', self::MODEL_BOTH),
        array('description', '1,140', '简介长度不能超过140个字符', self::VALUE_VALIDATE, 'length', self::MODEL_BOTH),
        array('category_id', 'require', '分类不能为空', self::MUST_VALIDATE , 'regex', self::MODEL_INSERT),
        array('category_id', 'require', '分类不能为空', self::EXISTS_VALIDATE , 'regex', self::MODEL_UPDATE),
        array('category_id', 'checkCategory', '该分类不允许发布内容', self::EXISTS_VALIDATE , 'callback', self::MODEL_UPDATE),
        array('category_id,type', 'checkCategory', '内容类型不正确', self::MUST_VALIDATE, 'callback', self::MODEL_INSERT),
		array('category_id', 'category_null', '该分类不存在', self::MUST_VALIDATE, 'callback', self::MODEL_INSERT),
    );

	/* 自动完成规则 */
    protected $_auto = array(
        array('uid','getUID',self::MODEL_BOTH,'callback'),
        array('title', 'htmlspecialchars', self::MODEL_BOTH, 'function'),
		array('description', 'getDesc', self::MODEL_BOTH, 'callback'),
        array('root', 'getRoot', self::MODEL_BOTH, 'callback'),
        array('link_id', 'getLink', self::MODEL_BOTH, 'callback'),
        array('attach', 0, self::MODEL_INSERT),
        array('view', 0, self::MODEL_INSERT),
        array('comment', 0, self::MODEL_INSERT),
        array('extend', 0, self::MODEL_INSERT),
        array('create_time', 'getCreateTime', self::MODEL_INSERT,'callback'),
        array('update_time', NOW_TIME, self::MODEL_BOTH),
        array('status', 2, self::MODEL_INSERT),
        array('position', 'getPosition', self::MODEL_BOTH, 'callback'),
        array('deadline', 'strtotime', self::MODEL_BOTH, 'function'),
		array('uuid', 'getUUID', self::MODEL_INSERT, 'callback'),
        array('type', 2, self::MODEL_BOTH),
    );
	protected $_updateValidate = array(
        'id'=>'',
        'title'=>'required',
        'description'=>'',
        'cover_id'=>'',
        'content'=>'',
        'parse'=>'',
        'pid'=>'',
        'model_id'=>'',
        'type'=>''
	);
	protected $_insertValidate = array(
        'category_id'=>'',
        'title'=>'required',
        'description'=>'',
        'cover_id'=>'',
        'content'=>'',
        'parse'=>'',
        'pid'=>'',
        'model_id'=>'',
        'type'=>''
	);

    public $page = '';

	/**
	 * 获取文档列表
	 * @param  integer  $category 分类ID
	 * @param  string   $order    排序规则
	 * @param  integer  $status   状态
	 * @param  boolean  $count    是否返回总数
	 * @param  string   $field    字段 true-所有字段
	 * @return array              文档列表
	 */
	public function lists($category, $order = '`id` DESC', $status = 1, $field = true){
		$map = $this->listMap($category, $status);
		return $this->field($field)->where($map)->order($order)->select();
	}

	/**
	 * 计算列表总数
	 * @param  number  $category 分类ID
	 * @param  integer $status   状态
	 * @return integer           总数
	 */
	public function listCount($category, $status = 1){
		$map = $this->listMap($category, $status);
		return $this->where($map)->count('id');
	}

	/**
	 * 获取详情页数据
	 * @param  integer $id 文档ID
	 * @return array       详细数据
	 */
	public function detail($id){
		/* 获取基础数据 */
		$info = $this->field(true)->find($id);
		if ( !$info ) {
            $this->error = '文档不存在';
            return false;
        }elseif(!is_array($info) || 1 != $info['status']){
			$this->error = '文章不存在！';
			return false;
		}
		/* 获取模型数据 */
		$logic  = $this->logic($info['model_id']);
		$detail = $logic->detail($id); //获取指定ID的数据
		if(!$detail){
			$this->error = $logic->getError();
			return false;
		}
		return array_merge($info, $detail);
	}

	/**
	 * 获取详情页数据(包括未审核的)
	 */
	public function showpreview($id){
		/* 获取基础数据 */
		$info = $this->field(true)->find($id);
		if ( !$info ) {
            $this->error = '文档不存在';
            return false;
        }
		/* 获取模型数据 */
		$logic  = $this->logic($info['model_id']);
		$detail = $logic->detail($id); //获取指定ID的数据
		if(!$detail){
			$this->error = $logic->getError();
			return false;
		}
		return array_merge($info, $detail);
	}

	/**
	 * 返回前一篇文档信息
	 * @param  array $info 当前文档信息
	 * @return array
	 */

	public function prev($info){
		$condition = array(
			'level'	=> array('lt', $info['level']),
			'_complex'=>array(
				'level'          => $info['level'],
				'_complex'=>array(
					'create_time'	=> array('lt', $info['create_time']),
					'_complex'=>array(
						'create_time'          => $info['create_time'],
						'id'          => array('lt', $info['id']),
					),
					'_logic'=>'or'
				),
				'_logic'=>'and'
			),
			'_logic'=>'or'
		);
		$map = array(
			'id'          => array('neq', $info['id']),
			'pid'		  => 0,
			'category_id' => $info['category_id'],
			'status'      => 1,
			'display'      => 1,
            'create_time' => array('lt', NOW_TIME),
            '_string'     => 'deadline = 0 OR deadline > ' . NOW_TIME,  			
		);
		$map['_complex'] = $condition;
		/* 返回前一条数据 */
		return $this->field(true)->where($map)->order('level desc, create_time desc, id DESC')->find();
	}

	/**
	 * 获取下一篇文档基本信息
	 * @param  array    $info 当前文档信息
	 * @return array
	 */
	public function next($info){
		
		$condition = array(
			'level'	=> array('gt', $info['level']),
			'_complex'=>array(
				'level'          => $info['level'],
				'_complex'=>array(
					'create_time'	=> array('gt', $info['create_time']),
					'_complex'=>array(
						'create_time'          => $info['create_time'],
						'id'          => array('gt', $info['id']),
					),
					'_logic'=>'or'
				),
				'_logic'=>'and'
			),
			'_logic'=>'or'
		);

		$map = array(
			'id'          => array('neq', $info['id']),
			'pid'		  => 0,
			'category_id' => $info['category_id'],
			'status'      => 1,
			'display'      => 1,
            'create_time' => array('lt', NOW_TIME),
            '_string'     => 'deadline = 0 OR deadline > ' . NOW_TIME,  			
		);
		$map['_complex'] = $condition;
		/* 返回下一条数据 */
		return $this->field(true)->where($map)->order('level asc, create_time asc, id asc')->find();
	}

	public function update(){
		/* 检查文档类型是否符合要求 */
		$Model = new \Admin\Model\DocumentModel();
		$res = $Model->checkDocumentType( I('type'), I('pid') );
		if(!$res['status']){
			$this->error = $res['info'];
			return false;
		}

		/* 获取数据对象 */
		$data = $this->field('pos,display', true)->create();
		if(empty($data)){
			return false;
		}

		/* 添加或新增基础内容 */
		if(empty($data['id'])){ //新增数据
			$id = $this->add(); //添加基础内容

			if(!$id){
				$this->error = '添加基础内容出错！';
				return false;
			}
			$data['id'] = $id;
		} else { //更新数据
			$status = $this->save(); //更新基础内容
			if(false === $status){
				$this->error = '更新基础内容出错！';
				return false;
			}
		}

		/* 添加或新增扩展内容 */
		$logic = $this->logic($data['model_id']);
		if(!$logic->update($data['id'])){
			if(isset($id)){ //新增失败，删除基础数据
				$this->delete($data['id']);
			}
			$this->error = $logic->getError();
			return false;
		}

		//内容添加或更新完成
		return $data;

	}

	public function blogadd($data = null){
		/* 检查文档类型是否符合要求 */
        $res = $this->checkDocumentType( I('post.type',2), I('pid') );
        if(!$res['status']){
            $this->error = $res['info'];
            return false;
        }
		$_POST['type']=I('post.type',2);//修复文档类型不填默认值

		/* 更新数据时通过uuid获取文章id */
		$uuid = I('uuid');
		if ($uuid) {
			$res = $this->field('id')->where(array('uuid'=>$uuid))->find();
			if($res!==false){
				$_POST['id'] = $res['id'];
			}
		}
		
		$data = $data ? $data : I('post.');
	
		$thisValidate = empty($data['id']) ? $this->_insertValidate : $this->_updateValidate;

		if (! $data = o('util')->validate( $data, $thisValidate))
        {
			$this->error = '填写有误，请修改后再次尝试提交！';
			return false;
        }

		unset($data['uuid']);

		/* 获取数据对象 */
		$data = $this->create($data);
		if(empty($data)){
            return false;
        }
		
		/* 添加或新增基础内容 */
        if(empty($data['id'])){ //新增数据
            $id = $this->add(); //添加基础内容

            if(!$id){
                $this->error = '新增基础内容出错！';
                return false;
            }
        } else { //更新数据
            $status = $this->save(); //更新基础内容
            if(false === $status){
                $this->error = '更新基础内容出错！';
                return false;
            }
        }

        /* 添加或新增扩展内容 */
        $logic = $this->logic($data['model_id']);
        $logic->checkModelAttr($data['model_id']);
        if(!$logic->update($id)){
            if(isset($id)){ //新增失败，删除基础数据
                $this->delete($id);
            }
            $this->error = $logic->getError();
            return false;
        }

        \Think\Hook::listen('documentSaveComplete', array('model_id'=>$data['model_id']));

        //行为记录
        if($id){
            action_log('add_document', 'document', $id, UID);
        }

        //内容添加或更新完成
        return $data;
		
	}

	/**
	 * 获取段落列表
	 * @param  integer $id    文档ID
	 * @param  integer $page  显示页码
	 * @param  boolean $field 查询字段
	 * @param  boolean $logic 是否查询模型数据
	 * @return array
	 */
	public function part($id, $page = 1, $field = true, $logic = true){
		$map  = array('status' => 1, 'pid' => $id, 'type' => 3);
		$info = $this->field($field)->where($map)->page($page, 10)->order('id')->select();
		if(!$info) {
			$this->error = '该文档没有段落！';
			return false;
		}

		/* 不获取内容详情 */
		if(!$logic){
			return $info;
		}

		/* 获取内容详情 */
		$model = $logic = array();
		foreach ($info as $value) {
			$model[$value['model_id']][] = $value['id'];
		}
		foreach ($model as $model_id => $ids) {
			$data   = $this->logic($model_id)->lists($ids);
			$logic += $data;
		}

		/* 合并数据 */
		foreach ($info as &$value) {
			$value = array_merge($value, $logic[$value['id']]);
		}

		return $info;
	}

	/**
	 * 获取指定文档的段落总数
	 * @param  number $id 段落ID
	 * @return number     总数
	 */
	public function partCount($id){
		$map = array('status' => 1, 'pid' => $id, 'type' => 3);
		return $this->where($map)->count('id');
	}

	/**
	 * 获取推荐位数据列表
	 * @param  number  $pos      推荐位 1-列表推荐，2-频道页推荐，4-首页推荐
	 * @param  number  $category 分类ID
	 * @param  number  $limit    列表行数
	 * @param  boolean $filed    查询字段
	 * @return array             数据列表
	 */
	public function position($pos, $category = null, $limit = null, $field = true){
		$map = $this->listMap($category, 1, $pos);

		/* 设置列表数量 */
		is_numeric($limit) && $this->limit($limit);

		/* 读取数据 */
		return $this->field($field)->where($map)->select();
	}

	/**
     * 获取数据状态
     * @return integer 数据状态
     * @author huajie <banhuajie@163.com>
     */
    protected function getStatus(){
        $cate = I('post.category_id');
        $check = M('Category')->getFieldById($cate, 'check');
        if($check){
            $status = 2;
        }else{
        	$status = 1;
        }
        return $status;
    }

    /**
     * 获取根节点id
     * @return integer 数据id
     * @author huajie <banhuajie@163.com>
     */
    protected function getRoot(){
    	$pid = I('post.pid');
    	if($pid == 0){
    		return 0;
    	}
    	$p_root = $this->getFieldById($pid, 'root');
    	return $p_root == 0 ? $pid : $p_root;
    }

	/**
	 * 验证分类是否允许发布内容
	 * @param  integer $id 分类ID
	 * @return boolean     true-允许发布内容，false-不允许发布内容
	 */
	protected function checkCategory($id){
		if(is_array($id)){
			$type = get_category($id['category_id'], 'type');
			return in_array($id['type'], $type);
		} else {
			$publish = get_category($id, 'allow_publish');
			return $publish ? true : false;
		}
	}

	/**
	 * 验证分类是否存在
	 * @param  integer $id 分类ID
	 */
	protected function category_null($id){
		$Category = D('Category')->where(array('id'=>$id))->find();
		if(!isset($Category)){
			return false;
		}else{
			return true;
		}
	}

	/**
	 * 检测分类是否绑定了指定模型
	 * @param  array $info 模型ID和分类ID数组
	 * @return boolean     true-绑定了模型，false-未绑定模型
	 */
	protected function checkModel($info){
        $cate   =   get_category($info['category_id']);
        $array  =   explode(',', $info['pid'] ? $cate['model_sub'] : $cate['model']);
        return in_array($info['model_id'], $array);
	}

	/**
	 * 获取扩展模型对象
	 * @param  integer $model 模型编号
	 * @return object         模型对象
	 */
	private function logic($model){
        $name  = parse_name(get_document_model($model, 'name'), 1);
        $class = is_file(MODULE_PATH . 'Logic/' . $name . 'Logic' . EXT) ? $name : 'Base';
        $class = MODULE_NAME . '\\Logic\\' . $class . 'Logic';
        return new $class($name);  		
	}

	/**
	 * 设置where查询条件
	 * @param  number  $category 分类ID
	 * @param  number  $pos      推荐位
	 * @param  integer $status   状态
	 * @return array             查询条件
	 */
	private function listMap($category, $status = 1, $pos = null){
		/* 设置状态 */
		$map = array('status' => $status, 'pid' => 0);

		/* 设置分类 */
		if(!is_null($category)){
			if(is_numeric($category)){
				$map['category_id'] = $category;
			} else {
				$map['category_id'] = array('in', str2arr($category));
			}
		}

		$map['create_time'] = array('lt', NOW_TIME);
		$map['_string']     = 'deadline = 0 OR deadline > ' . NOW_TIME;

		/* 设置推荐位 */
		if(is_numeric($pos)){
			$map[] = "position & {$pos} = {$pos}";
		}

		return $map;
	}

	/**
     * 检查指定文档下面子文档的类型
     * @param intger $type 子文档类型
     * @param intger $pid 父文档类型
     * @return array 键值：status=>是否允许（0,1），'info'=>提示信息
     * @author huajie <banhuajie@163.com>
     */
    public function checkDocumentType($type = null, $pid = null){
        $res = array('status'=>1, 'info'=>'');
        if(empty($type)){
            return array('status'=>0, 'info'=>'文档类型不能为空');
        }
        if(empty($pid)){
            return $res;
        }
        //查询父文档的类型
        $ptype = is_numeric($pid) ? $this->getFieldById($pid, 'type') : $this->getFieldByName($pid, 'type');
        //父文档为目录时
        switch($ptype){
            case 1: // 目录
            case 2: // 主题
                break;
            case 3: // 段落
                return array('status'=>0, 'info'=>'段落下面不允许再添加子内容');
            default:
                return array('status'=>0, 'info'=>'父文档类型不正确');                       
        }
        return $res;
    }

	/*自动完成描述**/
    public function getDesc($desc){
    	if($desc){
    		return htmlspecialchars($desc);
        }elseif(I('post.content')){
            //自动截取
            return htmlspecialchars(mb_substr(strip_tags(I('post.content')),0,150,'utf-8'));
        }
        return '';
    }

	/**
     * 生成uuid
     */
    protected function getUUID(){
        $uuid = guid();
		return $uuid;
    }

	/**
     * 生成uuid
     */
    protected function getUID(){
		$userinfo = O('Ucenter')->getUserInfo();
		$uid = $userinfo['uid'];
		return $uid;
    }

	/**
     * 生成推荐位的值
     * @return number 推荐位
     * @author huajie <banhuajie@163.com>
     */
    protected function getPosition(){
        $position = I('post.position');
        if(!is_array($position)){
            return 0;
        }else{
            $pos = 0;
            foreach ($position as $key=>$value){
                $pos += $value;		//将各个推荐位的值相加
            }
            return $pos;
        }
    }

	/**
     * 创建时间不写则取当前时间
     * @return int 时间戳
     * @author huajie <banhuajie@163.com>
     */
    protected function getCreateTime(){
        $create_time    =   I('post.create_time');
        return $create_time?strtotime($create_time):NOW_TIME;
    }

	/**
     * 获取链接id
     * @return int 链接对应的id
     * @author huajie <banhuajie@163.com>
     */
    protected function getLink(){
        $link = I('post.link_id');
        if(empty($link)){
            return 0;
        } else if(is_numeric($link)){
            return $link;
        }
        $res = D('Url')->update(array('url'=>$link));
        return $res['id'];
    }

}
