<?php
// +----------------------------------------------------------------------
// | OneThink [ WE CAN DO IT JUST THINK IT ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013 http://www.onethink.cn All rights reserved.
// +----------------------------------------------------------------------
// | Author: 麦当苗儿 <zuojiazi@vip.qq.com> <http://www.zjzit.cn>
// +----------------------------------------------------------------------

namespace Kcdns\Home\Controller;
use Common\Api\CategoryApi;
/**
 * 作家模型控制器
 */
class WriterController extends HomeController {

    /* 作家模型频道页 */
	public function index(){
		$GLOBALS['THINK_PHP_404'] = true;
		throw new \Exception('非开放页面');
		$this->display();
	}

	/* 作家模型个人页 */
	public function manager($uid = 0, $uuid = '', $p = 1){
		$uuid = I('get.uuid');
		if (!$uuid) {
			throw new \Exception('获取信息失败');
		}

		$map = array('uuid' => $uuid , 'status' => 1);
		$writer = D('Member')->where($map)->find();

		if (!$writer) {
			throw new \Exception('获取信息失败');
		}

		$uid = $writer['uid'];
		$list = O('site')->getCache(600)->pageList(I('get.page'),0,array('uid'=>$uid));//getCache()->

		$Api = new CategoryApi();

		//将分类信息加入到文章列表中
		foreach ($list as $k => $v) {
			$category_id = $list[$k]['category_id'];
			$category = $Api->get_category($category_id);
			$list[$k]['cate'] = $category;
		}

		$this->assign('list',$list);

		if(IS_AJAX){
			$return['status'] = 1;
			/* 模板赋值并渲染模板 */
			$return['data'] = $this->fetch('managerajax');
			$this->ajaxReturn($return);
			exit;
		}

		$ArticleNum = $this->getArticleNum($uid);
		$ArticleViewNum = $this->getArticleViewNum($uid);


		/*SEO信息*/    
		$this->SEO['title'] = sprintf(
			"%s_%s",$writer['nickname'] . ($writer['description']?'('.$writer['description'].')':'') ,$this->SEO['title']
		);
		
		$writer['description'] && $this->SEO['description'] = $writer['author_text'];

		/* 模板赋值并渲染模板 */
		$this->assign('SEO', $this->SEO);

		$this->assign('writer',$writer);
		$this->assign('ArticleNum',$ArticleNum);
		$this->assign('ArticleViewNum',$ArticleViewNum);
		$this->display();
	}

	/* 大咖列表页 */
	public function listauthor(){
		$Member = D('Member');
		
		$map['status'] = array('eq',1);
		$DK = $Member->where($map)->select();
		foreach($DK as $k=>$v){
			$an = $this->getArticleNum($v['uid']);
			$vn = $this->getArticleViewNum($v['uid']);
			$DK[$k]['an'] = $an;
			$DK[$k]['vn'] = $vn;
		}

		$list_tjdk = array_slice($this->arr_sort($this->getGroupuid($DK,6),'vn'),0,4);
		$list_dk = array_slice($this->arr_sort($this->getGroupuid($DK,6),'vn'),4);
		
		$this->assign('tj_dk',$list_tjdk);
		$this->assign('list',$list_dk);
		$this->display();
	}

	/* 获取当前作者的文章总数 */
	private function getArticleNum($uid){
		$Document = D('Document');
		$map = array(
			'uid' => $uid,
			'status' => 1,
			'display' => 1
			);
		$ArticleNum = $Document->where($map)->count();
		return $ArticleNum ? $ArticleNum : 0 ;
	}

	/* 获取当前作者的文章浏览总量 */
	private function getArticleViewNum($uid){
		$Document = D('Document');
		$map = array(
			'uid' => $uid,
			'status' => 1,
			'display' => 1
			);
		$ArticleViewNum = $Document->where($map)->sum('view');
		return $ArticleViewNum ? $ArticleViewNum : 0;
	}

	/* 按用户分组过滤查询结果 */
	private function getGroupuid($row,$groupid,$p=''){
		$uids = array_column($row, 'uid'); 

		$iswriter = M('auth_group_access')->where(
				array('uid'=>array('in',implode(',',$uids)),'group_id'=>$groupid )
			)->field('uid')->select();

		$iswriterUids = array_flip(array_column($iswriter, 'uid')); 

		foreach($row as $k=>$v){
		    if(array_key_exists($v['uid'],$iswriterUids)) {
		        $list_row[] = $v;
		    }
			if ($p!='' && count($list_row)==$p) {
			    break;
			}
		}
		return $list_row;
	}

	private function arr_sort($array,$key,$order="desc"){
		$arr_nums=$arr=array();
		foreach($array as $k=>$v){
			$arr_nums[$k]=$v[$key];
		}
		if($order=='asc'){
			asort($arr_nums);
		}else{
			arsort($arr_nums);
		}
		foreach($arr_nums as $k=>$v){
			$arr[$k]=$array[$k];
		}
		return $arr;
	}

	/* 文档分类检测 */
	private function category($id = 0){
		/* 标识正确性检测 */
		$id = $id ? $id : I('get.category', 0);
		if(empty($id)){
			$this->error('没有指定文档分类！');
		}

		/* 获取分类信息 */
		$category = D('Category')->info($id);
		if($category && 1 == $category['status']){
			switch ($category['display']) {
				case 0:
					$this->error('该分类禁止显示！');
					break;
				//TODO: 更多分类显示状态判断
				default:
					return $category;
			}
		} else {
			$this->error('分类不存在或被禁用！');
		}
	}

}
