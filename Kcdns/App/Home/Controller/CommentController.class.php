<?php
// +----------------------------------------------------------------------
// | OneThink [ WE CAN DO IT JUST THINK IT ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013 http://www.onethink.cn All rights reserved.
// +----------------------------------------------------------------------
// | Author: 麦当苗儿 <zuojiazi@vip.qq.com> <http://www.zjzit.cn>
// +----------------------------------------------------------------------

namespace Kcdns\Home\Controller;

/**
 * 评论控制器
 */
class CommentController extends HomeController {

	public function send(){
		$uid = $this->userinfo['uid']; //当前登陆的用户id

		if (!O('Ucenter')->isLogin())
		{
			$this->error('还没登录，请登录！');
		}
		$data['uid'] = $uid;
		$data['nickname'] = get_nickname($uid);
		$uuid = I('post.uuid');
		$aid = D('Document')->field('id')->where(array('uuid'=>$uuid))->find();
		$data['aid'] = $aid['id'];
		$data['create_time'] = time();
		$data['content'] = I('post.content');
		
		$Comment = M("Comment");
		$Comment->add($data);
		$this->success('评论成功');

    }

	public function getlist(){
		$aid = I('post.aid');
		$Comment = M("Comment");
		$map = array('aid'=>$aid , 'status'=>1);
		$list = $Comment->where($map)->limit('5')->order('create_time desc')->select();
		
		$this->assign('list', $list);
		$this->assign('aid', $aid);
		echo $this->fetch('AjaxComment:list');
    }

	public function getmore(){
		$aid = I('post.aid');
		$page = I('post.page');

		$Comment = M("Comment");
		$map = array('aid'=>$aid , 'status'=>1);
		$listmore = $Comment->page($page,5)->where($map)->order('create_time desc')->select();

		$this->assign('listmore', $listmore);
		echo $this->fetch('AjaxComment:more');
    }

	public function getform(){
		$uuid = I('post.uuid');
		
		$this->assign('uuid', $uuid);
		echo $this->fetch('AjaxComment:form');
    }

}