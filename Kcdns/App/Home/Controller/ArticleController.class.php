<?php
// +----------------------------------------------------------------------
// | OneThink [ WE CAN DO IT JUST THINK IT ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013 http://www.onethink.cn All rights reserved.
// +----------------------------------------------------------------------
// | Author: 麦当苗儿 <zuojiazi@vip.qq.com> <http://www.zjzit.cn>
// +----------------------------------------------------------------------

namespace Kcdns\Home\Controller;

/**
 * 文档模型控制器
 * 文档模型列表和详情
 */
class ArticleController extends HomeController {

    /* 文档模型频道页 */
	public function index(){
		$GLOBALS['THINK_PHP_404'] = true;
		throw new \Exception('非开放页面');

		/* 分类信息 */
		$category = $this->category();

		//频道页只显示模板，默认不读取任何内容
		//内容可以通过模板标签自行定制

		/* 模板赋值并渲染模板 */
		$this->assign('category', $category);
		$this->display($category['template_index']);
	}

	/* 文档模型列表页 */
	public function lists($p = 1){
		$GLOBALS['THINK_PHP_404'] = true;
		/* 分类信息 */
		$category = $this->category();

		//单文档分类 直接显示详情模板
		if($category['is_single']){
                        $list = D('Document')->lists($category['id']);
			$this->detail($list[0]['id'],$list[0]['uuid']);
			return;
		}
		$list = OE('site')->getCache(600)->pageList(I('get.page'),$category['id']);//getCache()->
		
		$this->assign('list', $list);

		if(IS_AJAX){
			$return['status'] = 1;
			/* 模板赋值并渲染模板 */
			$return['data'] = $this->fetch('listsajax');
			$this->ajaxReturn($return);
			exit;
		}
		if (!$list) {
		    $this->error('还没有发布内容',"/");
		}
        /*SEO信息*/    
		$this->SEO['title'] = sprintf(
			"%s_%s",$category['title'],$this->SEO['title']
		);
		$info['keywords'] && $this->SEO['keywords'] = $category['keywords'];
		$info['description'] && $this->SEO['description'] = $category['description'];

		/* 模板赋值并渲染模板 */
		$this->assign('SEO', $this->SEO);
		$this->assign('category', $category);
		$this->display($category['template_lists']);
	}

	/* 文档模型详情页 */
	public function detail($id = 0, $uuid = '', $p = 1){
		$GLOBALS['THINK_PHP_404'] = true;
		if(!$uuid){
			throw new \Exception('获取信息失败');
		}
		$Document = D('Document');
		$map = array('uuid' => $uuid);
		$row = $Document->where($map)->find();
		$id = $row['id'];
		
		/* 标识正确性检测 */
		if(!($id && is_numeric($id))){
			throw new \Exception('文档ID错误！');
		}
		//暂时关闭自动跳出
		//if ($row['link']) {
		//    redirect($row['link']);
		//}
		/* 页码检测 */
		$p = intval($p);
		$p = empty($p) ? 1 : $p;

		/* 获取详细信息 */
		$Document = D('Document');
		$info = $Document->detail($id);
		if(!$info){
			$this->error($Document->getError());
		}

		/* 分类信息 */
		$category = $this->category($info['category_id']);

		/* 获取模板 */
		if(!empty($info['template'])){//已定制模板
			$tmpl = $info['template'];
		} elseif (!empty($category['template_detail'])){ //分类已定制模板
			$tmpl = $category['template_detail'];
		} else { //使用默认模板
			$tmpl = 'Article_detail';
		}

		/* 更新浏览数 */
		$map = array('id' => $id);
		$Document->where($map)->setInc('view');

        /*SEO信息*/    
		$this->SEO['title'] = sprintf(
			"%s_%s_%s",$info['title'],$category['title'],$this->SEO['title']
		);
		$info['keywords'] && $this->SEO['keywords'] = $info['keywords'];
		$info['description'] && $this->SEO['description'] = $info['description'];

		/* 模板赋值并渲染模板 */
		$this->assign('SEO', $this->SEO);
		$this->assign('category', $category);
		$this->assign('info', $info);
		$this->assign('page', $p); //页码
		$this->display($tmpl);
	}

	/* 文档分类检测 */
	private function category($id = 0){
		/* 标识正确性检测 */
		$id = $id ? $id : I('get.category', 0);
		if(empty($id)){
			throw new \Exception('没有指定文档分类！');
		}

		/* 获取分类信息 */
		$category = D('Category')->info($id);
		if($category && 1 == $category['status']){
			switch ($category['display']) {
				case 0:
					throw new \Exception('该分类禁止显示！');
					break;
				//TODO: 更多分类显示状态判断
				default:
					return $category;
			}
		} else {
			throw new \Exception('分类不存在或被禁用！');
		}
	}

		/**
     * 文章预览
     */
    public function preview(){


		$GLOBALS['THINK_PHP_404'] = true;
		
		$uuid = I('uuid');

		if(!$uuid){
			throw new \Exception('获取信息失败');
		}

		if (!O('Ucenter')->isLogin() && !session('user_auth_sign'))
		{ // 还没登录 跳转到登录页面
			$this->error('还没登录，请登录！','/');
		}else{
			$ht_user_id = is_login();
			static $Auth = null;
			$Auth = $Auth ?  : $Auth = new \Think\Auth();
			$auth_rule = $Auth->check('Admin/setStatus', $ht_user_id, 1, 'url');
			if(is_administrator(is_login())){
				$auth_rule = true;
			}
			if(!$auth_rule){
				$articleuid = $this->getArticleUID($uuid); //文章所属用户id
				$loginuid = $this->userinfo['uid']; //当前登陆的用户id
				if($articleuid !== $loginuid){ //判断登陆用户是否是文章所属用户
					throw new \Exception('参数错误！');
				}
			}
		}

		$Document = D('Document');
		$map = array('uuid' => $uuid);
		$row = $Document->where($map)->find();
		$id = $row['id'];
		
		/* 标识正确性检测 */
		if(!($id && is_numeric($id))){
			throw new \Exception('文档ID错误！');
		}

		/* 获取详细信息 */
		$info = $Document->showpreview($id);

		if(!$info){
			$this->error($Document->getError());
		}

		/* 分类信息 */
		$category = $this->category($info['category_id']);

        /*SEO信息*/    
		$this->SEO['title'] = sprintf(
			"%s_%s_%s",$info['title'],$category['title'],$this->SEO['title']
		);
		$info['keywords'] && $this->SEO['keywords'] = $info['keywords'];
		$info['description'] && $this->SEO['description'] = $info['description'];

		/* 模板赋值并渲染模板 */
		$this->assign('SEO', $this->SEO);
		$this->assign('category', $category);
		$this->assign('info', $info);
		$this->display();
    }
	
	/* 通过当前文章uuid获取对应的uid */
	private function getArticleUID($uuid){
		$Document = D('Document');
		$map = array('uuid' => $uuid);
		$res = $Document->field('uid')->where($map)->find();
		if(false === $res){
			throw new \Exception('获取uid失败！');
		}else{
			return $res['uid'];
		}
	}

}
