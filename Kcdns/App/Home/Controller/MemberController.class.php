<?php
namespace Kcdns\Home\Controller;
use User\Api\UserApi;
/**
 * 前台首页控制器
 */
class MemberController extends HomeController {

	protected function _initialize(){
		parent::_initialize();

		$uid = $this->userinfo['uid'];
		// 获取当前用户ID
        define('UID', $uid);

		if (!O('Ucenter')->isLogin())
		{ // 还没登录 跳转到登录页面
			$this->error('还没登录，请登录！','/');
		}

		/*文章相关方法，集中检查用户组*/
		if(!in_array(6,$this->userinfo['group'])){
			redirect(U('/'));
		}

        //检查用户是否禁用
        $checkuser=$this->userinfo['status'];

        if($checkuser['status']<=0)
        {
            $this->error('账户已禁用！','/');
        }

		$this->assign('zz_group',6);
		$this->assign('auth_arr',implode(",", $this->userinfo['group']));

	}

	function index() {
		$loginuid = $this->userinfo['uid']; //当前登陆的用户id
		$ArticleViewNum = O('Ucenter')->getArticleViewNum();
		$ArticleNum = O('Ucenter')->getArticleNum();
		$this->assign('ArticleViewNum',$ArticleViewNum);
		$this->assign('ArticleNum',$ArticleNum);
		$this->display();
	}

	function article($p = 1) {

		$loginuid = $this->userinfo['uid']; //当前登陆的用户id
		$Category = D('Category')->get_Cate_auth($loginuid);

		$p = I('get.p');

		$Document = D('Document');
		$map = array('uid' => $loginuid,'status' => array('gt',0));
		$list = $Document->where($map)->page($p,10)->order('create_time desc')->select();
		//if(false === $list){
		//	throw new \Exception('获取列表数据失败！');
		//}
		status_to_string($list);
		$this->assign('Category',$Category);
		$this->assign('list',$list);
		$this->display();
	}

	/**
     * 文章预览
     */
    public function preview(){
		$GLOBALS['THINK_PHP_404'] = true;
		
		$uuid = I('uuid');

		if(!$uuid){
			throw new \Exception('获取信息失败');
		}

		$articleuid = $this->getArticleUID($uuid); //文章所属用户id
		$loginuid = $this->userinfo['uid']; //当前登陆的用户id
		
		if($articleuid !== $loginuid && is_administrator($loginuid) !== true && is_administrator(is_login()) !== true){ //判断登陆用户是否是文章所属用户
			throw new \Exception('参数错误！');
		}

		$Document = D('Document');
		$map = array('uuid' => $uuid);
		$row = $Document->where($map)->find();
		$id = $row['id'];
		
		/* 标识正确性检测 */
		if(!($id && is_numeric($id))){
			throw new \Exception('文档ID错误！');
		}

		/* 获取详细信息 */
		$info = $Document->showpreview($id);

		if(!$info){
			$this->error($Document->getError());
		}

		/* 分类信息 */
		$category = $this->category($info['category_id']);

        /*SEO信息*/    
		$this->SEO['title'] = sprintf(
			"%s_%s_%s",$info['title'],$category['title'],$this->SEO['title']
		);
		$info['keywords'] && $this->SEO['keywords'] = $info['keywords'];
		$info['description'] && $this->SEO['description'] = $info['description'];

		/* 模板赋值并渲染模板 */
		$this->assign('SEO', $this->SEO);
		$this->assign('category', $category);
		$this->assign('info', $info);
		$this->display();
    }

	function add() {
		$GLOBALS['THINK_PHP_404'] = true;
		$loginuid = $this->userinfo['uid']; //当前登陆的用户id
		if(IS_POST){
			//判断分类权限
			if (!in_array($_POST['category_id'], explode(",",$this->userinfo['cate_auth']))) {
				throw new \Exception('参数错误！');
			}
		    $_POST['cover_id'] = pathToCoverid(I('post.cover_id'));
			$_POST['pid'] = '0';
			$_POST['model_id'] = '2';
			$Document = D('Document');
			$res = $Document->blogadd();
			$res!==false?$this->success('新增成功！',U('Member/article')):$this->error('新增失败！',U('Member/article'));
		}else{
			$Category = D('Category')->get_Cate_auth($loginuid);
			$this->assign('Category',$Category);
			$this->display();
		}
	}

	function edit() {
		$GLOBALS['THINK_PHP_404'] = true;
		$uuid = I('get.uuid'); //文章uuid

		if (!$uuid) {
			throw new \Exception('参数错误！');
		}

		$articleuid = $this->getArticleUID($uuid); //文章所属用户id
		$loginuid = $this->userinfo['uid']; //当前登陆的用户id
		
		if($articleuid !== $loginuid){ //判断登陆用户是否是文章所属用户
			throw new \Exception('参数错误！');
		}

		if(IS_POST){
			$_POST['cover_id'] = pathToCoverid(I('post.cover_id'));
			$_POST['pid'] = '0';
			$_POST['model_id'] = '2';
			$_POST['uuid'] = $uuid;
			$Document = D('Document');
			$res = $Document->blogadd();
			$res!==false?$this->success('修改成功！',U('Member/article')):$this->error('修改失败！',U('Member/article'));
		}else{
			$Document = D('Document');
			$res = $Document->field('id')->where(array('uuid'=>$uuid))->find();
			$info = $Document->showpreview($res['id']);
			if(!$info){
				$this->error($Document->getError());
			}
			$Category = D('Category')->get_Cate_auth($loginuid);
			$this->assign('Category',$Category);
			$this->assign('info',$info);
			$this->display();
		}
	}

	function del() {
		$GLOBALS['THINK_PHP_404'] = true;
		$uuid = I('get.uuid'); //文章uuid

		if (!$uuid) {
			throw new \Exception('参数错误！');
		}

		$articleuid = $this->getArticleUID($uuid); //文章所属用户id
		$loginuid = $this->userinfo['uid']; //当前登陆的用户id

		if($articleuid !== $loginuid){ //判断登陆用户是否是文章所属用户
			throw new \Exception('参数错误！');
		}

		$Document = D('Document');
		$map = array('uuid' => $uuid);
		$data['status'] = '-1';
		$res = $Document->where($map)->save($data);
		$res!==false?$this->success('删除成功！'):$this->error('删除失败！');
		
	}

	function info() {
		$this->display();
	}

	function editinfo() {
		if(IS_POST){
			$loginuid = $this->userinfo['uid']; //当前登陆的用户id
            $cover_id = pathToCoverid(I('post.cover_id'));
			$nickname = I('post.nickname');
			$sex = I('post.sex');
			$description = I('post.description');
			$author_text = I('post.author_text');
            $User=new UserApi;
            $data=array('uid'=>$loginuid,'cover_id'=>$cover_id,'nickname'=>$nickname,'sex'=>$sex,'description'=>$description,'author_text'=>$author_text);
            $res=D('Member')->where(array('uid'=>$data['uid']))->save($data);
            $res!==false?$this->success('修改成功！',U('Member/info')):$this->error('修改失败！',U('Member/info'));
        } else {
			$this->display();
        }
	}

	/* 接收上传头像图片 */
	public function upload(){
	    o('Ucenter')->uploadAvatar();
	}
	
	/* 通过当前文章uuid获取对应的uid */
	public function getArticleUID($uuid){
		$Document = D('Document');
		$map = array('uuid' => $uuid);
		$res = $Document->field('uid')->where($map)->find();
		if(false === $res){
			throw new \Exception('获取uid失败！');
		}else{
			return $res['uid'];
		}
	}

	/* 文档分类检测 */
	private function category($id = 0){
		/* 标识正确性检测 */
		$id = $id ? $id : I('get.category', 0);
		if(empty($id)){
			throw new \Exception('没有指定文档分类！');
		}

		/* 获取分类信息 */
		$category = D('Category')->info($id);
		if($category && 1 == $category['status']){
			switch ($category['display']) {
				case 0:
					throw new \Exception('该分类禁止显示！');
					break;
				//TODO: 更多分类显示状态判断
				default:
					return $category;
			}
		} else {
			throw new \Exception('分类不存在或被禁用！');
		}
	}
}
