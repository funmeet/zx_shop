<?php
// +----------------------------------------------------------------------
// | OneThink [ WE CAN DO IT JUST THINK IT ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013 http://www.onethink.cn All rights reserved.
// +----------------------------------------------------------------------
// | Author: 麦当苗儿 <zuojiazi@vip.qq.com> <http://www.zjzit.cn>
// +----------------------------------------------------------------------

namespace Kcdns\Home\Controller;
use Think\Controller;

/**
 * 前台公共控制器
 * 为防止多分组Controller名称冲突，公共Controller名称统一使用分组名称
 */
class HomeController extends Controller {

        // 是否检测到 url 别名
    protected static $urlAliasDetected = false;
    public $SEO = null;
    public $userinfo = null;
    
	/* 空操作，用于输出404页面 */
	public function _empty(){
		$this->redirect('/');
	}

    protected function _initialize(){

        $this->SEO['keyword'] = C('WEB_SITE_KEYWORD');
        $this->SEO['description'] = C('WEB_SITE_DESCRIPTION');
        $this->SEO['title'] = C('WEB_SITE_TITLE');

        $this->assign('SEO',$this->SEO);


        if(!C('WEB_SITE_CLOSE')){
            $this->error('站点已经关闭，请稍后访问~');
        }

        //生成全局token
        OE('Site')->getCache()->createSiteToken();
        
        //访问地址别名检测
        $this->checkUrlAlias();

         //获取广告位信息
         $this->assign('adsInfo',OE('site')->getCache()->getAdsList(array('foot_link','foot_site','top_banner','bottom_page','banner_list','banner_icon','fund_recommend')));

         //获取导航列表
         $this->assign('navList',OE('site')->getCache()->getNavList());
		 
		 //$this->userinfo = O('Ucenter')->getUserInfo();

		 $this->assign('action_name', ACTION_NAME);
		 $this->assign('userinfo', $this->userinfo);
    }
    
    ///检测文档及分类别名访问
    public function checkUrlAlias(){
        
        if(HomeController::$urlAliasDetected)return;
        $uri = addslashes(stripslashes(trim(substr($_SERVER['PHP_SELF'], strlen($_SERVER['SCRIPT_NAME'])))));
        
        if(preg_match('^/index/index', $uri)){
            $this->redirect('/');
        }

        //文章别名检测
        if($articleAlias=OE('site')->getCache()->checkDocumentAlias($uri)){
            HomeController::$urlAliasDetected=true;
            exit(A('Article')->detail($articleAlias['id']));
        }
        //分类别名检测
        if($categoryAlias=O('site')->getCache()->checkCategoryAlias($uri)){
            HomeController::$urlAliasDetected=true;
            redirect('/' . $categoryAlias['name']);
        }
    }
}
