<?php
namespace Kcdns\Home\Widget;
use Think\Controller;

/**
 * 评论
 */

class CommentWidget extends Controller{

	//评论框和列表
    public function commentlist($aid,$uuid){
		
		$this->assign('aid', $aid);
		$this->assign('uuid', $uuid);
		$this->display('Widget:comment');
    }
	
}
