<?php
namespace Kcdns\Home\Widget;
use Think\Controller;

/**
 * 评论
 */

class GoodsbuyWidget extends Controller{

	//评论框和列表
    public function get($url){
		if (!$url) return false;
		if(!preg_match("/^(http\:\/\/)?wap\.kanggou\.com\/product\/view\//u",$url)){
			return false;
		}
		if(preg_match('/view\/(\d+)\./i', $url, $matches)) {
			$goodsId = $matches[1];
		}
		if (!$goodsId) {
			return false;
		}
		//var_dump($goodsId);exit;
		$this->assign('goodsid', $goodsId);
		$this->assign('goodsurl', $url);
		$this->display('Widget:goodsbuyget');
    }
	
}
