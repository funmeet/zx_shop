<?php
namespace Kcdns\Home\Widget;
use Think\Controller;

/**
 * 相关链接
 */

class LinkWidget extends Controller{
	
	
    //底部链接
    public function linkfoot(){

		$linkfoot = OE('site')->getCache()->getAdsList(array('foot_link'));

		$this->assign('linkfoot',$linkfoot['foot_link']);
        $this->display('Widget:linkfoot');
    }

	//友情链接
    public function linkside(){

		$linkside = OE('site')->getCache()->getAdsList(array('foot_site'));

		$this->assign('linkside',$linkside['foot_site']);
        $this->display('Widget:linkside');
    }
	
}