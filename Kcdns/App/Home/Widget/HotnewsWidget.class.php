<?php
namespace Kcdns\Home\Widget;
use Think\Controller;

/**
 * 热门文章、文章排行榜
 */

class HotnewsWidget extends Controller{
	
	
    //热门文章
    public function gethotnews($cate_id){

		if($cate_id){
			$condition = array(
				'addfield'=>'uuid',
				'model'=>'article',
				'order'=>'view desc,create_time desc',
				'limit'=>'10',
				'where'=>array('create_time'=>array('gt',time()-86400*7),'category_id'=>array('eq',$cate_id))
			);
		}else{
			$condition = array(
				'addfield'=>'uuid',
				'model'=>'article',
				'order'=>'view desc,create_time desc',
				'limit'=>'10',
				'where'=>array('create_time'=>array('gt',time()-86400*7))
			);
		}

		$hotlist = O('site')->getCache()->articleList($condition);

        $this->assign('hotlist',$hotlist);
        $this->display('Widget:hotnews');
    }

	//文章排行
    public function getrank(){

		$day = array(
			'addfield'=>'uuid',
			'model'=>'article',
			'order'=>'view desc,create_time desc',
			'limit'=>'10',
			'where'=>array('create_time'=>array('gt',time()-86400*1))
		);

		$week = array(
			'addfield'=>'uuid',
			'model'=>'article',
			'order'=>'view desc,create_time desc',
			'limit'=>'10',
			'where'=>array('create_time'=>array('gt',time()-86400*7))
		);

		$daylist = O('site')->getCache()->articleList($day);
		$weeklist = O('site')->getCache()->articleList($week);

        $this->assign('daylist',$daylist);
		$this->assign('weeklist',$weeklist);
        $this->display('Widget:ranklist');
    }
	
}
