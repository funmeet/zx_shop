<?php
// +-------------------------------------------------------------------
// | 
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
return array(

        'TAGLIB_PRE_LOAD' => 'OT\\TagLib\\Article,OT\\TagLib\\Think',

        // 模板
        'DEFAULT_THEME' => 'default',
        'TMPL_ACTION_SUCCESS' => 'Dispatch:success',
        'TMPL_ACTION_ERROR' => 'Dispatch:error',
        'TMPL_EXCEPTION_FILE' => APP_PATH . 'Home/View/blog/exception.html',
        'TMPL_PARSE_STRING' => array(
                '__STATIC__' => __ROOT__ . '/Public/static',
                '__ADDONS__' => __ROOT__ . '/Public/' . MODULE_NAME . '/Addons',
                '__IMG__' => __ROOT__ . '/Public/' . MODULE_NAME . '/images',
                '__CSS__' => __ROOT__ . '/Public/' . MODULE_NAME . '/css',
                '__JS__' => __ROOT__ . '/Public/' . MODULE_NAME . '/js'
        ),

        /* 编辑器图片上传相关配置 */
    'EDITOR_UPLOAD' => array(
		'mimes'    => '', //允许上传的文件MiMe类型
		'maxSize'  => 2*1024*1024, //上传的文件大小限制 (0-不做限制)
		'exts'     => 'jpg,gif,png,jpeg', //允许上传的文件后缀
		'autoSub'  => true, //自动子目录保存文件
		'subName'  => array('date', 'Y-m-d'), //子目录创建方式，[0]-函数名，[1]-参数，多个参数使用数组
		'rootPath' => './Uploads/Editor/', //保存根路径
		'savePath' => '', //保存路径
		'saveName' => array('uniqid', ''), //上传文件命名规则，[0]-函数名，[1]-参数，多个参数使用数组
		'saveExt'  => '', //文件保存后缀，空则使用原后缀
		'replace'  => false, //存在同名是否覆盖
		'hash'     => true, //是否生成hash编码
		'callback' => false, //检测文件是否存在回调函数，如果存在返回文件信息数组
    ),

        // 会话
        'SESSION_PREFIX' => 'Home',
        'COOKIE_PREFIX' => 'Home',


        'URL_ROUTER_ON' => true,
        'URL_ROUTE_RULES' => array(
                // 首页
                '/^((cn|en)\/)?index(\.html)?$/' => 'Index/index',
				//xywy oauth
                '/^xoauth\/(\w+)(\.html)?$/' => 'Oauth/index?type=xywy&minitype=:1',
                '/^xoauthbind\/(\w+)(\.html)?$/' => 'Oauth/index?type=xywy&minitype=:1&bind=binduser',
				// 分类列表
				'/^((cn|en)\/)?(\w+)(\.html)?$/' =>	'Article/lists?category=:3',
				'/^((cn|en)\/)?(\w+)(\/p\/(\d+))?(\.html)?$/' => 'Article/lists?category=:3&p=:5',
                // 详情
				'/^((cn|en)\/)?article\/((\w|\-)*)(\.html)?$/' =>	'Article/detail?uuid=:3',
				'/^((cn|en)\/)?ajax\/(\w+)(\.html)?$/' => 'Ajax/:3',
				// 作者
				'/^((cn|en)\/)?writer\/listauthor(\.html)?$/' => 'Writer/listauthor',
				'/^((cn|en)\/)?writer\/((\w|\-)*)(\.html)?$/' => 'Writer/manager?uuid=:3',
        )
);
