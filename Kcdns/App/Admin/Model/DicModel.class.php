<?php
// +-------------------------------------------------------------------
// | 
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Kcdns\Admin\Model;

class DicModel extends CommonModel
{
    
    // 列表数据修正, 补全字典值
    protected function _formatListField ($k, $v, $row)
    {
        static $items = null;
        if (! isset($items))
        {
            $r = M('dic_item')->order('sort ASC, id ASC')->select();
            foreach ($r as $rv)
            {
                $items[$rv['name']][] = $rv['value'] . ' = ' . $rv['lable'];
            }
        }
        
        // 将创建时间字段内容替换为字典值列表
        if ($k == 'dic.create_time')
        {
            $v = implode("<br>", $items[$row['dic.name']]);
        }
        
        return parent::_formatListField($k, $v, $row);
    }
    
    // 编辑表单数据修正, 补全字典值
    public function getData ($idOrRow, $where = [])
    {
        $data = parent::getData($idOrRow, $where);
        $data['dic.items'] = [];
        
        $r = M('dic_item')->where([
                'name' => $data['dic.name']
        ])->order('sort ASC, id ASC')->select();
        foreach ($r as $v)
        {
            $data['dic.items'][] = [
                    'value' => $v['value'],
                    'lable' => $v['lable']
            ];
        }
        return $data;
    }
    
    // 保存表单时同步更新字典值
    protected function _update ($formData = [], $where = [])
    {
        $status = parent::_update($formData, $where);
        if ($status)
        {
            $data = $formData[$this->name];
            M('dic_item')->where([
                    'name' => $data['name']
            ])->delete();
            $sort = 0;
            $items = json_decode($data['items'], true) ?  : [];
            foreach ($items as $v)
            {
                $_i = [
                        'name' => $data['name'],
                        'value' => $v['value'],
                        'lable' => $v['lable'],
                        'create_time' => date('Y-m-d H:i:s'),
                        'update_time' => date('Y-m-d H:i:s'),
                        'sort' => $sort ++,
                        'status' => 1
                ];
                M('dic_item')->data($_i)->add();
            }
        }
        return $status;
    }
    
    // 删除字典分类
    public function del ($ids, $where)
    {
        // 取出所有待删除分类
        $ids = is_array($ids) ? $ids : [
                $ids
        ];
        $r = M('dic')->where([
                'id' => [
                        'IN',
                        $ids
                ]
        ])->select();
        
        // 执行删除
        $status = parent::del($ids, $where);
        
        // 删除分类成功后删除字典值
        if ($status)
        {
            $name = [];
            foreach ($r as $v)
            {
                $name[] = $v['name'];
            }
            $name and M('dic_item')->where([
                    'name' => [
                            'IN',
                            $name
                    ]
            ])->delete();
        }
        
        return $status;
    }
}