<?php
// +-------------------------------------------------------------------
// | 
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Kcdns\Admin\Model;
use Kcdns\Admin\Model\AuthGroupModel;

/**
 * 文档基础模型
 */
class DocumentModel extends CommonModel
{

    /**
     * 新增文章
     */
    protected function _insert ($formData)
    {
        $data = $this->_parseData($formData);
        
        // TODO 审核
        $data[$this->name]['status'] = 1;
        $data[$this->name]['uuid'] = guid();
        $data[$this->name]['create_time'] = date('Y-m-d H:i:s');
        
        $id = $this->data($data[$this->name])->add();
        if (! $id)
        {
            throw new \Exception('添加失败');
        }
        $data['extend_data']['id'] = $id;
        return M($data['extend_table'])->data($data['extend_data'])->add();
    }

    /**
     * 更新文章
     */
    protected function _update ($formData = [], $where = [])
    {
        $data = $this->_parseData($formData);
        $status = $this->data($data[$this->name])->where($where)->save() !== false;
        if (! $status)
        {
            throw new \Exception('更新失败');
        }
        return M($data['extend_table'])->data($data['extend_data'])->where($where)->save() !== false;
    }

    /**
     * 查询文章
     */
    protected function _getData ($formOptions)
    {
        return $this->table(C('DB_PREFIX') . $this->name)->alias("`{$this->name}`")->field($formOptions['field'])->join($formOptions['join'])->where($formOptions['where'])->find();
    }

    /**
     * 删除文章
     */
    protected function _del ($delOptions)
    {
        $record = $this->where($delOptions['where'])->find();
        $status = parent::_del($delOptions);
        if ($status && $record)
        {
            $category = M('category')->where([
                    'id' => $record['category_id']
            ])->find();
            $model = $category ? M('model')->where([
                    'id' => $category['model']
            ])->getField('name') : false;
            if ($model)
            {
                M('document_' . $model)->where([
                        'id' => $delOptions['where']['id']
                ])->delete();
            }
        }
        return $status;
    }
    
    // 解析/构造数据
    protected function _parseData ($formData)
    {
        $data = [
                $this->name => $formData[$this->name]
        ];
        // 自动补全描述
        if (isset($data[$this->name]['description']))
        {
            $data[$this->name]['description'] = htmlspecialchars($data[$this->name]['description']);
        }
        elseif (isset($data['extend_data']['content']))
        {
            // 自动截取
            $data[$this->name]['description'] = htmlspecialchars(mb_substr(strip_tags($data['extend_data']['content']), 0, 150, 'utf-8'));
        }
        
        $data[$this->name]['update_time'] = date('Y-m-d H:i:s');
        
        unset($formData[$this->name]);
        $tables = array_keys($formData);
        $data['extend_table'] = current($tables);
        $data['extend_data'] = current($formData);
        
        return $data;
    }
}