<?php
// +-------------------------------------------------------------------
// | 
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Kcdns\Admin\Controller;
use Kcdns\Admin\Common\ModelCfg;

/**
 * 前台导航管理
 */
class ChannelController extends CommonController
{
    protected $editRedirect = 2;
    
    protected function _initialize ()
    {
        parent::_initialize();
        
        $data = M('Channel')->field(true)->order('sort ASC, id DESC')->select();
        foreach ($data as $k => $v)
        {
            $data[$k]['url'] = U('edit', array(
                    ModelCfg::escape('channel.id') => $v['id']
            ));
            $data[$k]['is_single'] = 1;
            $data[$k]['disabled'] = ! $v['status'];
        }
        $this->nodes = list_to_tree($data);
        $this->assign('nodes', $this->nodes);
        
        IS_POST or $this->insertSaveData = [
                'channel.pid' => I('pid'),
                'channel.status' => 1
        ];
    }

    public function index ()
    {
        // 批量排序
        if (IS_POST && I('post.flag') == 'sort')
        {
            $data = I('post.data');
            filter($data, [
                    [
                            'id' => 'required;int',
                            'sort' => 'required;int'
                    ]
            ]);
            foreach ($data as $v)
            {
                M('channel')->data([
                        'sort' => $v['sort']
                ])->where([
                        'id' => $v['id']
                ])->save();
            }
            $this->success();
        }
        
        if ($this->nodes)
        {
            $this->redirect('edit', array(
                   ModelCfg::escape('channel.id') => $this->nodes[0]['id']
            ));
        }
        $this->display();
    }
}