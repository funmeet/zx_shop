<?php
// +-------------------------------------------------------------------
// | 
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Kcdns\Admin\Controller;

/**
 * 配置控制器
 */
class ConfigController extends CommonController
{

    protected function _initialize ()
    {
        parent::_initialize();
        if (ACTION_NAME)
        {}
    }

    public function export ()
    {
        $data = D('config')->export();
        OE('file')->download($data, 'sys_config.txt', 'content');
        exit();
    }

    public function import ()
    {
        $this->modelAction = 'import';
        parent::add();
    }

    public function wechat ()
    {
        $this->modelAction = 'batchSetWechat';
        $this->__call('wechat', []);
    }

    public function __call ($method, $args)
    {
        $this->modelName = CONTROLLER_NAME . '/' . ACTION_NAME;
        $this->modelAction = $this->modelAction ?  : 'batchSet';
        
        $cfg = new \Kcdns\Admin\Common\ModelCfg($this->modelName, $this->modelEnv ?  : 'add');
        if (IS_POST)
        {
            $modelAction = $this->modelAction ?  : 'insert';
            $status = $cfg->model->$modelAction(I('post.'), $this->insertSaveData);
            $status ? $this->success('保存成功') : $this->error('保存失败');
        }
        
        $this->meta_title = '配置';
        $this->assign('data', $cfg->model->batchGet());
        $this->assign('cfg', $cfg);
        $this->assign('env', 'add');
        $this->display($this->_getTpl('Common/form'));
    }
}