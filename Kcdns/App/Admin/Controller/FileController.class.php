<?php
// +-------------------------------------------------------------------
// | 
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Kcdns\Admin\Controller;
use Think\Upload;

/**
 * 文件控制器
 * 主要用于下载模型的文件上传和下载
 */
class FileController extends AdminController
{
    
    // 图片/文件上传
    public function upload ()
    {
        exit(O('file')->upload([
                'dir' => C('UPLOAD_DIR') . 'Picture/',
                'path' => C('UPLOAD_PATH') . 'Picture/'
        ]));
    }
    
    // 编辑器上传图片
    public function uploadEditor ()
    {
        
        /* 返回标准数据 */
        $return = array(
                'error' => 0,
                'info' => '上传成功',
                'data' => ''
        );
        
        /* 编辑器图片上传相关配置 */
        $setting = array(
                'maxSize' => 2 * 1024 * 1024, // 上传的文件大小限制 (0-不做限制)
                'exts' => 'jpg,gif,png,jpeg', // 允许上传的文件后缀
                'autoSub' => true, // 自动子目录保存文件
                'subName' => array(
                        'date',
                        'Y-m-d'
                ), // 子目录创建方式，[0]-函数名，[1]-参数，多个参数使用数组
                'rootPath' => C('UPLOAD_DIR') . 'Editor/', // 保存根路径
                'savePath' => '', // 保存路径
                'saveName' => array(
                        'uniqid',
                        ''
                ), // 上传文件命名规则，[0]-函数名，[1]-参数，多个参数使用数组
                'replace' => false, // 存在同名是否覆盖
                'hash' => true, // 是否生成hash编码
                'callback' => false
        );
        
        $this->uploader = new Upload($setting, 'Local');
        $info = $this->uploader->upload($_FILES);
        if ($info)
        {
            $url = C('EDITOR_UPLOAD.rootPath') . $info['imgFile']['savepath'] . $info['imgFile']['savename'];
            $url = str_replace('./', '/', $url);
            $info['fullpath'] = __ROOT__ . $url;
        }
        
        /* 记录附件信息 */
        if ($info)
        {
            $return['url'] = $info['fullpath'];
            unset($return['info'], $return['data']);
        }
        else
        {
            $return['error'] = 1;
            $return['message'] = $this->uploader->getError();
        }
        
        /* 返回JSON数据 */
        exit(json_encode($return));
    }
    
    // 编辑器上传剪贴板中的图片
    public function uploadScreenImg ()
    {
        // 接受ajax传过来的值
        $data = I('post.');
        // 对接受的数组进行处理替换处理，获取到图片的base64编码
        $data['editor'] = str_replace('<img src="data:image/png;base64,', '', $data['editor']);
        $data['editor'] = str_replace('" alt="" />', '', $data['editor']);
        // 解析图片文件
        $data['editor'] = base64_decode($data['editor']);
        // 创建缓存目录
        $file = C('UPLOAD_DIR') . 'Editor/' . date('Y-m-d', time());
        $path = C('UPLOAD_PATH') . 'Editor/' . date('Y-m-d', time());
        // 生成目录，给目录权限
        file_exists($file) or mkdir($file, 0777); // 生成新的目录给权限
                                                  // 定义文件名
        $filename = $file . '/screenshot_' . time() . '.png';
        $savename = $path . '/screenshot_' . time() . '.png';
        // 将base64转码写入文件
        file_put_contents($filename, $data['editor']);
        // 返回数据
        $returnfile = '<img src="' . $savename . '" alt="" />';
        $this->ajaxReturn($returnfile);
    }
}
