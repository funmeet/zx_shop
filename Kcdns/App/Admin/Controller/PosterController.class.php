<?php
// +-------------------------------------------------------------------
// | 
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Kcdns\Admin\Controller;

/**
 * 广告
 */
class PosterController extends CommonController
{

    protected function _initialize ()
    {
        parent::_initialize();
        
        $category = I('category') and $this->listWhere = [
                'poster.category' => $category
        ];
    }

    public function index()
    {
        $_POST['order'][0]['column'] = 2;
        $_POST['order'][0]['dir'] = 'asc';
        parent::index();
    }

    public function add()
    {
        if(IS_POST){
            $this->addRedirct=U('index',["category"=>I('post.poster.category')]);
        }
        parent::add();
    }

    public function edit()
    {
        if(IS_POST){
            $this->editRedirect=U('index',["category"=>I('post.poster.category')]);
        }
        parent::edit();
    }
}
