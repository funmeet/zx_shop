<?php
// +-------------------------------------------------------------------
// | 
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Kcdns\Admin\Controller;

/**
 * 定时任务控制器
 */
class CronController extends CommonController
{

    public function run ()
    {
        o('cron')->run(I('get.cron.id'));
        $this->success('Running');
    }
}