<?php
// +-------------------------------------------------------------------
// | 
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Kcdns\Admin\Controller;
use Kcdns\Admin\Model\AuthGroupModel;
use Kcdns\Admin\Common\ModelCfg;

/**
 * 角色管理
 */
class AuthGroupController extends CommonController
{

    protected $listWhere = array(
            'module' => 'admin',
            'type' => AuthGroupModel::TYPE_ADMIN
    );

    protected function _initialize ()
    {
        parent::_initialize();
        $vars = [
                ModelCfg::escape('auth_group.id') => I('get.auth_group.id')
        ];
        $this->assign('accessUrl', U('access', $vars));
        $this->assign('categoryUrl', U('category', $vars));
        $this->assign('memberUrl', U('AuthGroupAccess', $vars));
        $this->assign('groupName', M('AuthGroup')->where([
                'id' => I('get.auth_group.id')
        ])->getfield('title'));
    }
    
    // 访问授权
    public function access ()
    {
        if (IS_GET)
        {
            // 所有主权限节点
            $map = array(
                    'module' => 'admin',
                    'type' => 2,
                    'status' => 1
            );
            $main_rules = M('AuthRule')->where($map)->getField('name,id');
            
            // 所有子权限节点
            $map['type'] = 1;
            $child_rules = M('AuthRule')->where($map)->getField('name,id');
            
            // 所有角色
            $auth_group = M('AuthGroup')->where($where)->getfield('id,id,title,rules');
            
            $this->assign('main_rules', $main_rules);
            $this->assign('auth_rules', $child_rules);
            $this->assign('node_list', getAuthNodes(true, false));
            $this->assign('auth_group', $auth_group);
            $this->assign('this_group', $auth_group[I('get.auth_group.id')]);
            $this->display();
        }
        else
        {
            $this->modelAction = 'updateAccess';
            parent::edit();
        }
    }
    
    // 用户组的分类授权
    public function category ()
    {
        if (IS_GET)
        {
            $auth_group = M('AuthGroup')->where(array(
                    'status' => array(
                            'egt',
                            '0'
                    ),
                    'module' => 'admin',
                    'type' => AuthGroupModel::TYPE_ADMIN
            ))->getfield('id,id,title,rules');
            
            $group_list = D('Category')->getTree();
            $authed_group = AuthGroupModel::getCategoryOfGroup(I('get.auth_group.id'));
            
            $this->assign('authed_group', implode(',', (array) $authed_group));
            $this->assign('group_list', $group_list);
            $this->assign('auth_group', $auth_group);
            $this->assign('this_group', $auth_group[I('get.auth_group.id')]);
            $this->display();
        }
        else
        {
            $this->modelAction = 'updateCategory';
            parent::edit();
        }
    }
    
    // 递归输出分类数, 内部调用
    public function _tree ($tree)
    {
        $this->assign('tree', $tree);
        $this->display('AuthGroup/tree');
    }
}
