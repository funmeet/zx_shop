<?php
// +-------------------------------------------------------------------
// | 
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Kcdns\Admin\Controller;
use Kcdns\Admin\Model\AuthGroupModel;
use Kcdns\Admin\Common\ModelCfg;

/**
 * 文章管理
 */
class DocumentController extends CommonController
{
    
    // 文档分类id
    protected $cateId = null;

    protected $category;
    
    // 当前用户可见分类
    protected $authCateIds = array();

    protected function _initialize ()
    {
        parent::_initialize();
        
        // 获取分类id
        if (I('get.document.id'))
        {
            $this->cateId = M('document')->where([
                    'id' => I('get.document.id')
            ])->getField('category_id');
        }
        elseif (I('get.category_id'))
        {
            $this->cateId = I('get.category_id');
        }
        else
        {
            $this->cateId = 0;
        }
        
        if ($this->cateId)
        {
            $this->category = get_category($this->cateId);
            if (! $this->category)
            {
                throw new \Exception('查询无效');
            }
        }
        
        $this->authCateIds = AuthGroupModel::getAuthCategories(UID);
        
        // 所有可见分类
        $where = array(
                'status' => 1
        );
        is_administrator() or $where['id'] = array(
                'IN',
                $this->authCateIds
        );
        
        // 获取面包屑信息
        $nav = get_parent_category($this->cateId) or $nav = array(
                array(
                        'title' => '全部分类'
                )
        );
        $this->assign('rightNav', $nav);
        
        // 分类树
        $cate = M('Category')->where($where)->field('id,title,pid,allow_publish,is_single')->order('pid,sort')->select();
        $cate = list_to_tree($cate);
        getCateMenuToChild($cate, $hide_cate);
        $this->assign('nodes', $cate);
        $this->assign('cate_id', $this->cateId);
        
        // 列表查询条件
        $this->listWhere['document.category_id'] = $this->cateId ?  : [
                'IN',
                $this->authCateIds
        ];
        $this->listWhere['document.status'] = 1;
    }

    /**
     * 检测需要动态判断的文档类目有关的权限
     */
    protected function checkDynamic ()
    {
        return $this->cateId ? in_array($this->cateId, AuthGroupModel::getAuthCategories(UID)) : true;
    }
    
    // 列表
    public function index ()
    {
        $this->_loadModel();
        $this->category && $this->category['is_single'] ? $this->_single() : parent::index();
    }
    
    // 新增
    public function add ()
    {
        $this->category && $this->category['allow_publish'] or $this->error('该分类不允许发布内容！');
        $this->insertSaveData = [
                'document.category_id' => $this->cateId
        ];
        $this->addRedirct = U('index', [
                'category_id' => $this->cateId
        ]);
        $this->_loadModel();
        $this->category && $this->category['is_single'] ? $this->_single() : parent::add();
    }
    
    // 编辑
    public function edit ()
    {
        $this->category && $this->category['allow_publish'] or $this->error('该分类不允许发布内容！');
        $this->_loadModel();
        $this->editRedirect = U('index', [
                'category_id' => $this->cateId
        ]);
        $this->category && $this->category['is_single'] ? $this->_single() : parent::edit();
    }
    
    // 回收站
    public function recycle ()
    {
        $this->listWhere['document.status'] = - 1;
        parent::index();
    }
    
    // 还原
    public function revert ()
    {
        $this->listWhere['document.status'] = - 1;
        parent::set();
    }
    
    // 删除
    public function del ()
    {
//        $this->listWhere['document.status'] = - 1;
        parent::del();
    }
    
    // 单网页
    protected function _single ()
    {
        $article = M('document')->where([
                'category_id' => $this->category['id']
        ])->find();
        
        $this->template = 'Document/edit';
        
        if (! $article)
        {
            $this->insertSaveData = [
                    'document.category_id' => $this->cateId
            ];
            parent::add();
        }
        else
        {
            $_GET['document.id'] = $article['id'];
            parent::edit();
        }
    }

    protected function _loadModel ()
    {
        // 全部文档列表
        $model = [
                'document'
        ];
        
        if ($this->category)
        {
            $documentModel = M('model')->where([
                    'id' => $this->category['model']
            ])->getField('name') or $this->error("该分类没有绑定文档模型");
            
            // 优先使用分类配置
            array_unshift($model, 'document/category/' . $this->category['name']);
            
            // 使用模型分类配置
            array_unshift($model, 'document/model/' . $documentModel);
        }
        foreach ($model as $v)
        {
            try
            {
                new ModelCfg($v);
                return $this->modelName = $v;
            }
            catch (\Exception $e)
            {}
        }
    }
}