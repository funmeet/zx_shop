<?php
// +-------------------------------------------------------------------
// | 
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Kcdns\Admin\Controller;

/**
 * 空白页
 */
class EmptyController extends AdminController
{

    protected function _empty ()
    {
        parent::_empty();
    }
}