<?php
// +-------------------------------------------------------------------
// | 
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Kcdns\Admin\Controller;

/**
 * 管理员
 */
class MemberController extends CommonController
{

    protected function _initialize ()
    {
        parent::_initialize();
//        $this->listWhere = [
//                'member.uid' => [
//                        'neq',
//                        C('USER_ADMINISTRATOR')
//                ]
//        ];
    }

    public function edit(){
        parent::edit();
    }

    public function del ()
    {
        $this->listWhere = [];
        parent::del();
    }
    
    // 修改当前用户密码
    public function password ()
    {
        IS_POST and $this->modelAction = 'editPassword';
        $this->listWhere = [
                'member.uid' => UID
        ];
        return parent::edit();
    }
    
    // 修改用户所属分组
    public function group ()
    {
        $this->modelAction = IS_POST ? 'updateGroup' : 'getDataGroup';
        return parent::edit();
    }
}