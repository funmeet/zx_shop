<?php
// +-------------------------------------------------------------------
// | 
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Kcdns\Admin\Widget;
use Kcdns\Admin\Controller\AdminController;

class TreeWidget extends AdminController
{

    const NODE_PARENT = 1;

    const NODE_END = 2;

    protected $nodeClass = array(
            self::NODE_PARENT => 'folder',
            self::NODE_END => 'file'
    );
    
    // 每个挂件实例对应的 js id
    protected $runId = null;
    
    // 当前项目 ID
    protected $currentItemId = null;

    protected $options = array(
            
            // 默认展开层级
            'expendLevel' => 2,
            
            // 显示增加按钮, 开启时传入点击回调函数名
            'showAdd' => false,
            
            // 显示编辑按钮
            'showEdit' => false,
            
            // 显示删除按钮
            'showDel' => false,
            
            // 显示排序按钮
            'showSort' => false,
            
            // 显示 checkbox, 开启时传入 input name
            'showCheckbox' => false,
            
            // checkbox 当前选中列表
            'checkedList' => array(),
            
            // 是否根据分类或者单网页区分末端节点图标
            'categoryEndNode' => false,
            
            // 排序回调
            'sort' => false
    );

    /**
     * $rootNode = array('title' => '导航管理');
     */
    public function render ($list, $rootNode = array(), $currentItemId = 0, $options = array())
    {
        $this->runId = uniqid('Treeview_');
        $this->currentItemId = $currentItemId;
        $this->options = array_merge($this->options, $options);
        if ($rootNode)
        {
            $rootNode['id'] = $rootNode['id'] ?  : 0;
            $rootNode['_child'] = $list;
            $list = array(
                    $rootNode
            );
        }
        $html = $this->_createHtml($list, $this->runId);
        $this->assign(
                array(
                        'html' => $html,
                        'runId' => $this->runId,
                        'currentId' => $this->currentItemId,
                        'addCallback' => $this->options['showAdd'],
                        'editCallback' => $this->options['showEdit'],
                        'delCallback' => $this->options['showDel'],
                        'checkboxName' => $this->options['showCheckbox'],
                        'checkboxChecked' => $this->options['checkedList'],
                        'sortCallback' => $this->options['showSort']
                ));
        $this->display('Widget/tree');
    }

    protected function _createHtml ($list, $id, $level = 1)
    {
        if (! is_array($list))
        {
            return "";
        }
        $treeClass = $this->options['showCheckbox'] ? 'treeview-no-icon' : 'filetree';
        $html = $id ? "<ul id='$id' class='$treeClass'>" : "<ul>";
        foreach ($list as $k => $v)
        {
            $childNodes = $v['_child'];
            $type = is_array($childNodes) ? self::NODE_PARENT : self::NODE_END;
            
            // 分类树特殊处理, 末端节点是单网页时显示为文件图标, 是分类时显示目录图标
            $type == self::NODE_END and $this->options['categoryEndNode'] and $type = $v['is_single'] ? self::NODE_END : self::NODE_PARENT;
            
            $disabledClass = $v['disabled'] ? 'disabled' : '';
            $closedClass = $level > $this->options['expendLevel'] || $disabledClass ? 'closed' : '';
            
            $html .= "<li class='{$closedClass} {$disabledClass}'>";
            $html .= "<span class='{$this->nodeClass[$type]} treeview-item-box' data-id='{$v['id']}'>";
            $html .= $v['url'] ? "<a class='pjax' href='{$v['url']}'>{$v['title']}</a>" : "<span>{$v['title']}</span>";

            $this->options['showAdd'] and $html .= "<i class='treeview-item treeview-item-add fa fa-plus'></i>";
            $this->options['showEdit'] and $html .= "<i class='treeview-item treeview-item-edit fa fa-pencil'></i>";
            
            // 根结点不显示删除按钮
            $level > 1 and $this->options['showDel'] and $html .= "<i class='treeview-item treeview-item-del fa fa-trash-o'></i>";
            
            $this->options['showSort'] and $html .= "<i class='treeview-item treeview-item-sort fa fa-reorder'></i>";
            
            $html .= "</span>";
            $html .= $this->_createHtml($childNodes, null, $level + 1);
            $html .= "</li>";
        }
        $html .= "</ul>";
        return $html;
    }
}
