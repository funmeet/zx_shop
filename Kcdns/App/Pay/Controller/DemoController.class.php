<?php
// +----------------------------------------------------------------------
// | OneThink [ WE CAN DO IT JUST THINK IT ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013 http://www.onethink.cn All rights reserved.
// +----------------------------------------------------------------------
// | Author: 麦当苗儿 <zuojiazi@vip.qq.com> <http://www.zjzit.cn>
// +----------------------------------------------------------------------

namespace Kcdns\Pay\Controller;


class DemoController extends BaseController {

    //发起支付测试
    public function pay(){
        $params=array(
            'orderNo'=>uniqid(),
            'amount'=>(int)I('get.money',100),
            'payType'=>I('get.type','alipay'),
            'callback'=>'payCallbackTest',
            'redirectUrl'=>"Pay/Demo/success",
            'orderTitle'=>'支付测试',
            'orderDesc'=>'支付测试描述',
            //'weixin_openid'=>uniqid()
        );
        $payForm=O('Pay')->pay($params);
        $this->assign('payForm',$payForm);
        $this->display();
    }

    //支付成功页面
    public function success(){

        echo '恭喜您 商品支付成功';
    }

    public function test(){
        if(!$userInfo=O('Oauth')->getUserInfo('weixin')){
            echo '异常:'.O('Oauth')->getError();
        }

        dump($userInfo);

        $oauthInfo = O('Oauth')->getByOpenId('592e6e8b63b71', 'weixin');

        dump($oauthInfo);
    }
}
