<?php
// +-------------------------------------------------------------------
// | 
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Kcdns\Wechat\Controller;
use \Think\Controller;

class ServerController extends Controller
{

    public function index ()
    {
        $wechat = O('wechat')->load();
        
        // 验证请求合法性(token)
        $wechat->valid();
        
        // 加载请求数据
        $wechat->getRev();
        
        // 用户 openid
        $from = $wechat->getRevFrom();
        
        // 地址上报请求
        $location = $wechat->getRevEventGeo();
        if ($location)
        {
            $lat = $location['x'];
            $lon = $location['y'];
        }
    }
}
