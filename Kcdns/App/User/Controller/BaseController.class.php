<?php
// +-------------------------------------------------------------------
// | 
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Kcdns\User\Controller;

use Think\Controller;

class BaseController extends Controller
{

    public $SEO;

    public $userinfo;

    protected $forceLogin = true;

    protected function _initialize ()
    {
        C(api('Config/lists'));
        C('WEB_SITE_CLOSE') and $this->error('站点已经关闭，请稍后访问~');
        
        $this->SEO['keyword'] = C('WEB_SITE_KEYWORD');
        $this->SEO['description'] = C('WEB_SITE_DESCRIPTION');
        $this->SEO['title'] = C('WEB_SITE_TITLE');
        $this->assign('SEO', $this->SEO);
        
        // 生成全局token
        OE('Site')->getCache()->createSiteToken();
        
        // 微信环境自动获取 Oauth2 信息
        if (strpos($_SERVER['HTTP_USER_AGENT'], 'MicroMessenger') !== false && ! O('oauth')->current())
        {
            O('oauth')->fetch();
        }
        
        // 尝试使用 Oauth2 自动登录
        $ouathInfo = O('oauth')->current() and O('user')->loginWithOauth($ouathInfo);
        
        // 当前用户
        $this->userinfo = O('user')->current();
        if ($this->forceLogin && ! $this->userinfo)
        {
            $this->redirect('Public/login');
        }
    }

    public function _empty ()
    {
        $this->redirect('/');
    }
}
 