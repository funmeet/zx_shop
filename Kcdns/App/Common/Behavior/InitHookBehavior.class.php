<?php
// +-------------------------------------------------------------------
// | 
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Kcdns\Common\Behavior;
use Think\Behavior;

class InitHookBehavior extends Behavior
{
    
    // 行为扩展的执行入口必须是run
    public function run (&$content)
    {
        // 静态文件配置
        OE('util')->assets([
                'cache' => ! APP_DEBUG,
                'path_prefix' => rtrim(ROOT_PATH, '/')
        ]);
        
        // 开启时从 config 表加载配置项
        if (! LOAD_DB_CONFIG)
        {
            return;
        }
        
        // 加载数据库配置项
        $config = S('DB_CONFIG_CACHE');
        if (! $config)
        {
            $configData = M('config')->where([
                    'status' => 1
            ])->select() ?  : [];
            foreach ($configData as $v)
            {
                $config[$v['name']] = kcone_think_decrypt($v['value']);
            }
            S('DB_CONFIG_CACHE', $config);
        }
        C($config);
    }
}