<?php
// +-------------------------------------------------------------------
// | 
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Api\Controller;

class EmptyController extends IndexController
{
    public function __construct()
    {
        parent::__call(CONTROLLER_NAME,[]);
    }
}
