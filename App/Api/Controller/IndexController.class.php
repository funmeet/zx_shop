<?php
// +-------------------------------------------------------------------
// | 
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Api\Controller;

class IndexController extends \Think\Controller
{

    protected function _initialize()
    {
        C('WEB_SITE_CLOSE') and $this->error('站点已经关闭，请稍后访问~');
    }

    public function index()
    {
        // 制定允许其他域名访问
        header("Access-Control-Allow-Origin:*");
// 允许的响应类型
        header('Access-Control-Allow-Methods: GET, POST, PUT,DELETE,OPTIONS,PATCH');
        // 非调试模式 : 按照接口不存在处理
        API_DOC or $this->_invalid_doc_();

        OE('Api')->doc('/Api/V1/', [], 'V1');
    }

    public function __call($method, $args)
    {
        $option_data_str = file_get_contents("php://input");
        $option_data = json_decode($option_data_str,true);
        $param = I('post.', '', '');
//        $option_data = array_merge($option_data, $param);
        $response = OE('Api')->call($method, $param, 'V1');

        // 清除缓冲区的输出, 防止响应内容受到干扰
        ob_get_clean();

        $cost_time = microtime(true) - $_SERVER['REQUEST_TIME_FLOAT'];
        header("Cost-time:{$cost_time}s");
        exit($response);
    }
}
