<?php

namespace Wap\Controller;

class FixController extends \Think\Controller
{
    //修复总贡献
    public function total_contribution()
    {
        $order_list = M('order')->where(['pay_status' => 1])->select();
        try {
            M('')->startTrans();
            $update = M('user')->where('uid > 0')->save([
                'total_contribution' => 0
            ]);
            if ($update === false) {
                throw new \Exception("total_contribution 清零失败");
            }
            $update = M('team_log')->where('uid > 0')->delete();
            if ($update === false) {
                throw new \Exception("team_log 清空失败");
            }
            foreach ($order_list as $order) {
                $update = M('user')->where(['uid' => $order['uid']])->save([
                    'total_contribution' => ['exp', "total_contribution+{$order['order_contribution']}"]
                ]);
                if ($update === false) {
                    throw new \Exception("total_contribution 累计失败");
                }
                $user_list = OE('user')->getRefererTree($order['uid']);
                foreach ($user_list as $user) {
                    OE('user')->addTeamContribution($user['uid'], $order);
                }
            }
            M('')->commit();
        } catch (\Exception $e) {
            M('')->rollback();
            throw new \Exception($e->getMessage());
        }
    }

    //修复总消费
    public function total_buy()
    {
        $order_list = M('order')->where(['pay_status' => 1])->select();
        try {
            M('')->startTrans();
            $update = M('user')->where('uid > 0')->save([
                'total_buy' => 0
            ]);
            if ($update === false) {
                throw new \Exception("total_buy 清零失败");
            }
            foreach ($order_list as $order) {
                $update = M('user')->where(['uid' => $order['uid']])->save([
                    'total_buy' => ['exp', "total_buy+{$order['pay_money']}"]
                ]);
                if ($update === false) {
                    throw new \Exception("pay_money 累计失败");
                }
            }
            M('')->commit();
        } catch (\Exception $e) {
            M('')->rollback();
            throw new \Exception($e->getMessage());
        }
    }
}
