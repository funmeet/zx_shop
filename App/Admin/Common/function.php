<?php

function getQuota($goods_value)
{
    $multiple = C('GOODS_MULTIPLE');
    return $multiple ? $goods_value * $multiple : $goods_value;
}

function getRefererUsername($referer_id)
{
    return M('ucenter_member')->where(['id' => $referer_id])->getField('username');
}

function getSubstrDate($date)
{
	return substr($date,0,10);
}

function getOrderAddress($order_id)
{
	$order = M('order')->where(['id'=>$order_id])->find();

	return $order['area'].$order['address'];
}