<?php
// +-------------------------------------------------------------------
// | 
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Admin\Controller;

use Kcdns\Admin\Controller\CommonController;
use Think\Log;

class CommissionLogController extends CommonController
{
	
	public function index()
	{
	     $typeArray = [1,2,12,13,14,15,16,17];
	    foreach ($typeArray as $type){
	    	$array[$type] = M('commission_log')->where(['type'=>$type])->sum('money')?:0;
	    } 
	    $this->assign('total',array_sum($array));
	    $this->assign('array',$array);
		parent::index();
	}
	
    public function genMonthBounds()
    {
        try {
            OE('user')->genMonthBounds();
            $this->success('操作成功');
        } catch (\Exception $e) {
            $this->error($e->getMessage());
        }
    }

    public function genYearBounds()
    {
        try {
            OE('user')->genYearBounds();
            $this->success('操作成功');
        } catch (\Exception $e) {
            $this->error($e->getMessage());
        }
    }

    public function teamSettle(){
        //exit('未开放');
        Log::w_log("########start:");
        try {
            $batch_id = OE('user')->teamSettle();
            $this->ajaxReturn([
                'status' => 1,
                'info' => '操作成功',
                'url' => $url = strtolower(U('commission_log/index')),
                'batch' => [
                    $url => $batch_id
                ]
            ]);
        } catch (\Exception $e) {
            $this->error($e->getMessage());
        }
        Log::w_log("########end:");
    }

    public function fanli(){
        //exit('未开放');
        try {
            $batch_id = OE('user')->fanli();
            $this->ajaxReturn([
                'status' => 1,
                'info' => '操作成功',
                'url' => $url = strtolower(U(CONTROLLER_NAME .'/index')),
                'batch' => [
                    $url => $batch_id
                ]
            ]);
        } catch (\Exception $e) {
            $this->error($e->getMessage());
        }
    }

/*    public function fanli(){

        try {
            OE('user')->shellfanli();
            $this->success('操作成功');
        } catch (\Exception $e) {
            $this->error($e->getMessage());
        }

    }*/




}