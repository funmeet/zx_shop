<?php
// +-------------------------------------------------------------------
// | 
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Admin\Controller;

use Kcdns\Admin\Controller\CommonController;
use Think\Log;
use Think\Model;

class UserController extends CommonController
{
    public function genMonthBounds()
    {
        try {
            OE('user')->genMonthBounds();
            $this->success('操作成功');
        } catch (\Exception $e) {
            $this->error($e->getMessage());
        }
    }

    public function genYearBounds()
    {
        try {
            OE('user')->genYearBounds();
            $this->success('操作成功');
        } catch (\Exception $e) {
            $this->error($e->getMessage());
        }
    }

    public function upgradeAllVipLevel()
    {
        try {
            OE('user')->upgradeAllVipLevel();
            $this->success('操作成功');
        } catch (\Exception $e) {
            $this->error($e->getMessage());
        }
    }

    public function resetPayPwd()
    {
        try {
            $password = OE('user')->resetPayPwd(I('get.user.uid'));
            $this->success('重置成功，新支付密码是：' . $password);
        } catch (\Exception $e) {
            $this->error($e->getMessage());
        }
    }

    public function resetPwd()
    {
        try {
            $password = OE('user')->resetPwd(I('get.user.uid'));
            $this->success('重置成功，新登录密码是：' . $password);
        } catch (\Exception $e) {
            $this->error($e->getMessage());
        }
    }
    
    public function edit()
    {
    	if (IS_POST) {
    		try {
    			OE('user')->editMobile(I('get.user.uid'), I('post.user.mobile'));
    		} catch (\Exception $e) {
    			$this->error($e->getMessage());
    		}
    		
    	} 
    	parent::edit();
    	
    }
    
    public function reset()
    {
    	if (IS_POST) {
    		try {
    			OE('user')->reset(I('get.user.uid'), I('post.'));
    		} catch (\Exception $e) {
    			$this->error($e->getMessage());
    		}
    		$this->success('操作成功');
    	} else {
    		parent::add();
    	}
    }

    public function recharge()
    {
        if (IS_POST) {
            try {
                OE('user')->recharge(I('post.user.uid'), I('post.user.money'));
            } catch (\Exception $e) {
                $this->error($e->getMessage());
            }
            $this->success('操作成功');
        } else {
            parent::edit();
        }
    }

    public function dock()
    {
        if (IS_POST) {
            try {
                OE('user')->dock(I('post.user.uid'), I('post.user.money'));
            } catch (\Exception $e) {
                $this->error($e->getMessage());
            }
            $this->success('操作成功');
        } else {
            parent::edit();
        }
    }

    public function tree()
    {
        list($current_level, $list) = O('user')->getTopRefererRelationTree(I('get.user.uid'));
        $this->assign([
            'current_level' => $current_level,
            'list' => json_encode($list, JSON_UNESCAPED_UNICODE)
        ]);
        $this->display();
    }

    public function getUnderRelation()
    {
        $list = O('user')->getUnderRelation(I('post.uid'), I('post.level'));
        exit(json_encode([
            'status' => 1,
            'msg' => 'ok',
            'data' => $list
        ]));
    }

    //导入匹配
    public function user_import()
    {
        if (IS_POST) {
            try {
                OE('user')->user_import('.' . I('post.user.file'));
            } catch (\Exception $e) {
                $this->error($e->getMessage());
            }
            $this->success('操作成功');
        } else {
            parent::edit();
        }
    }

    public function userDataRepair(){
        $Model = new Model();
        $data = $Model->table('__USER__')->select();
        try {
            $Model = new Model();
            $Model->startTrans();
            foreach($data as &$item){
                if($item['referer_id']){
                    $one_data = $Model->table('__USER__')
                        ->where(array('uid' => $item['referer_id']))
                        ->find();
                    if($one_data['referer_id']){
                        $ret1 = $Model->table('__USER__')
                            ->where(array('uid' => $item['uid']))
                            ->save(array('referer2_id' => $one_data['referer_id']));
                        if(!$ret1){
                            throw new \Exception('修复数据失败：'.$item['uid']);
                        }
                    }
                }
            }

            $Model->commit();
        } catch (\Exception $e) {
            $Model->rollback();
            throw new \Exception($e->getMessage());
        }

        $this->success('操作成功');

    }

    public function daoUser(){

        try {
            $batch_id = OE('user')->daoUser();
            $this->ajaxReturn([
                'status' => 1,
                'info' => '操作成功',
                'url' => $url = strtolower(U('User/index')),
                'batch' => [
                    $url => $batch_id
                ]
            ]);
        } catch (\Exception $e) {
            $this->error($e->getMessage());
        }
    }

    public function dao1(){
    	try {
    		$url=OE('user')->daoUser();
    		$this->success('操作成功',$url);
    	} catch (\Exception $e) {
    		$this->error($e->getMessage());
    	}
        
    }

    public function sss(){

        $url=OE('order')->sss();
        echo $url;
        exit;
    }
}