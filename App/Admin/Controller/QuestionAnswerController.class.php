<?php
// +-------------------------------------------------------------------
// | 
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Admin\Controller;

use Kcdns\Admin\Controller\CommonController;

class QuestionAnswerController extends CommonController
{
    public function add()
    {
        if (IS_GET) {
            $result = M('question')->find($question_id = I('get.question.id'));
            $this->insertSaveData = [
                'question.title' => $result['title'],
                'question_answer.question_id' => $question_id,
            ];
            parent::add();
        } else {
            try {
                OE('user')->questionAnswer($question_id = I('post.question_answer.question_id'), is_login(), I('post.question_answer.content'), $is_admin = 1);
                $this->success('回答成功', U('index', array('question__CFG__id' => $question_id)));
            } catch (\Exception $e) {
                $this->error($e->getMessage());
            }
        }
    }
}