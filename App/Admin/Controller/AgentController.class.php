<?php
// +-------------------------------------------------------------------
// | 
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Admin\Controller;

use Kcdns\Admin\Controller\CommonController;

class AgentController extends CommonController
{

	//审核
    public function edit()
    {
    	if (IS_POST) {
    		try {
 
    			OE('agent')->examine(I('post.'),I('get.agent.id'));
    			$this->success('操作成功');
    		} catch (\Exception $e) {
    			$this->error($e->getMessage());
    		}
    		
    	} else{

	    	//$check_status = O('agent')->getCheckStatus(I('get.agent.id'));
	    	parent::edit();
	    	/* if ($check_status == 0 || $check_status == 2){
	    		parent::edit();
	    	}else{
	    		parent::view();
	    	} */
    	}
    }
    
    //结算
    public function settle()
    {
    	try {
    	
    		OE('agent')->settle(I('get.agent.id'));
    		$this->success('操作成功');
    	} catch (\Exception $e) {
    		$this->error($e->getMessage());
    	}
    }
    
}