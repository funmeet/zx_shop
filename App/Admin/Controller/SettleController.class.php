<?php
// +-------------------------------------------------------------------
// | 
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Admin\Controller;

use Kcdns\Admin\Controller\CommonController;

class SettleController extends CommonController
{
    public function settleAll()
    {
        try {
            OE('order')->settleAll();
            $this->success('操作成功');
        } catch (\Exception $e) {
            $this->error($e->getMessage());
        }
    }
}