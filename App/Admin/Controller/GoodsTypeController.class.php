<?php
// +-------------------------------------------------------------------
// |
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Admin\Controller;

use Kcdns\Admin\Controller\CommonController;
use Think\Exception;

class GoodstypeController extends CommonController
{
    public function del()
    {

        try {
            //检查关联数据
            $flag = M('goods')->where(['type' => I('get.goodstype.id')])->find();
            if(isset($flag['id'])) throw new Exception('分类被使用，不允许删除');
            parent::del();

        } catch (\Exception $e) {
            $this->error($e->getMessage());
        }



    }

}