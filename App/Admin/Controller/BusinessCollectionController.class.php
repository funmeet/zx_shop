<?php
// +-------------------------------------------------------------------
// | 
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Admin\Controller;

use Kcdns\Admin\Controller\CommonController;

class BusinessCollectionController extends CommonController
{
    public function add()
    {
	    try {
	       if (IS_POST) {
	       	  list($_POST['business_collection.uid'],
	       	  	   $_POST['business_collection.referer_id'],
	       	  	   $no) = OE('business')->checkData(I('post.'));
	       	  $this->insertSaveData = ['business_collection.no'=>$no];
	       	  OE('business')->isBusiness($_POST['business_collection.uid'],1);
	       }
	       
	       parent::add();
	    } catch (\Exception $e) {
	       $this->error($e->getMessage());
	    }
    	
    }

    public function edit()
    {
    	try {
    		if (IS_POST) {
    			list($_POST['business_collection.uid'],
    				 $_POST['business_collection.referer_id'],
    					) = OE('business')->checkData(I('post.'),'edit',I('get.business_collection.id'));

    			
    		}
    		parent::edit();
    	} catch (\Exception $e) {
    		$this->error($e->getMessage());
    	}
    	 
    }
    
    
    
}