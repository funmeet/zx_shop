<?php
// +-------------------------------------------------------------------
// | 
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Admin\Controller;

use Kcdns\Admin\Controller\CommonController;

class SpecController extends CommonController
{
    public function index1()
    {
        if (I('get.goods.id')) {

//            $this->listWhere['goods_id'] = I('get.goods.id');
            $this->listWhere = ['spec.goods_id' => I('get.goods.id')];
            unset($_GET['goods.id']);
            unset($_GET['spec.id']);
        }
        parent::index();
    }
}