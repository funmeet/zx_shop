<?php
// +-------------------------------------------------------------------
// | 
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Admin\Controller;

use Kcdns\Admin\Controller\CommonController;

class OrderController extends CommonController
{
    public function express()
    {
        if (IS_POST) {
            try {
                OE('order')->express(I('get.order.id'), I('post.order.express_company'), I('post.order.express_no'));
                $this->success('操作成功');
            } catch (\Exception $e) {
                $this->error($e->getMessage());
            }
        } else {
            parent::edit();
        }
    }
}