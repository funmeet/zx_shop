<?php
// +-------------------------------------------------------------------
// | 
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Admin\Controller;

use Kcdns\Admin\Controller\CommonController;

class TeamLogController extends CommonController
{
	
	public function team()
	{

		list($total_performance,$commission) = O('data')->getTeam(I('get.start_time'), I('get.end_time'),I('get.mobile'));
		$this->assign([
				'total_performance' => $total_performance,
				'commission' => $commission,
				]);
		$this->display();
	}
	
    
}