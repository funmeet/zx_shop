<?php
// +-------------------------------------------------------------------
// | 
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Admin\Controller;

use Kcdns\Admin\Controller\CommonController;

class RemitController extends CommonController
{
    public function edit()
    {
        if (IS_POST) {
            try {
                OE('user')->checkRemit([
                    'id' => I('get.remit.id'),
                    'status' => I('post.remit.status'),
                    'status_des' => I('post.remit.status_des'),
                    'receive_money' => I('post.remit.receive_money'),
                ]);
            } catch (\Exception $e) {
                $this->error($e->getMessage());
            }
            $this->success('操作成功');
        } else {
            parent::edit();
        }
    }
}