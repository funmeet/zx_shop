<?php
// +-------------------------------------------------------------------
// | 
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Admin\Controller;

use Kcdns\Admin\Controller\CommonController;
use Think\Exception;

class GoodsController extends CommonController
{
    public function add()
    {
    try {
	       if (IS_POST) {
	       	$no = I('post.goods.no','');
		       	if ($no != '') {
		       	   OE('business')->checkNo($no);
		       }
	       }
	       
	       parent::add();
	    } catch (\Exception $e) {
	       $this->error($e->getMessage());
	    }
    }
    
    public function edit()
    {
    	try {
    		if (IS_POST) {
    		  $no = I('post.goods.no','');
		       	if ($no != '') {
		       	   OE('business')->checkNo($no);
		       }
    			 
    		}
    		parent::edit();
    	} catch (\Exception $e) {
    		$this->error($e->getMessage());
    	}
    
    }


	public function del()
	{

		try {
			//检查关联数据
			$flag = M('order_goods')->where(['id' => I('get.goods.id')])->find();
			if(isset($flag['id'])) throw new Exception('该商品已下单，不允许删除');
			$ret = M('goods_sku')->where(['goods_id' => I('get.goods.id')])->sum('stock');
			if($ret>0) throw new Exception('该商品还有库存，不允许删除');
			parent::del();

		} catch (\Exception $e) {
			$this->error($e->getMessage());
		}



	}
    
    
}