<?php
// +-------------------------------------------------------------------
// | 
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Admin\Controller;

use Kcdns\Admin\Controller\CommonController;
use Think\Exception;

class SkuController extends CommonController
{


	public function del()
	{

		try {
			//检查关联数据
			$list = M('goods_sku')->select();
			foreach($list as $item){
				$sku_ids_arr = explode('-', $item['sku_ids']);
				if(in_array(I('get.sku.id'), $sku_ids_arr)){
					throw new Exception('该规格已被使用，不允许删除');
				}
			}
			parent::del();

		} catch (\Exception $e) {
			$this->error($e->getMessage());
		}



	}
    
    
}